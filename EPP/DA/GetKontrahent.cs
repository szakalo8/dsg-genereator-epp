﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EPP.DA
{
    public class GetKontrahent
    {

        public static vwKlienci GetKtrFromSubiekt(GenerujEPP.Firma firma)
        {
            var firma1 = firma.ToString();


            if(firma == GenerujEPP.Firma.WektorBis)
            {
                firma1 = "WEKTOR-BIS";
            }


            vwKlienci ktr = new vwKlienci();

            using (konektEntities db = new konektEntities(Properties.Settings.Default.connectionStringKM))
            {
                ktr = db.vwKlienci.SingleOrDefault(a => a.kh_Symbol == firma1.ToString());

                return ktr;
            }
        }
    }
}
