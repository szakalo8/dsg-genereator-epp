    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading.Tasks;

    namespace EPP.DA
    {
        public class GetWarehousesListBis : IGetWarehousesListBis
        {
            public async Task<List<sl_Magazyn>> GetSnFromSubiektToListAsync()
            {
                using (konektEntities db = new konektEntities(Properties.Settings.Default.connectionStringKM))
                {
                    return await db.sl_Magazyn
                        .Where(s => s.mag_Nazwa != "G��WNY")
                        .ToListAsync();
                }
            }
        }
    }
    