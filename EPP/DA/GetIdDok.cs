﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EPP.DA
{
    public class GetIdDok
    {
        public static int GetIdDokFromSub(string numerDok)
        {
            int idDok = 0;

            using (konektEntities db = new konektEntities(Properties.Settings.Default.connectionStringKM))
            {
                idDok = db.dok__Dokument.SingleOrDefault(d => d.dok_NrPelny == numerDok).dok_Id;

                return idDok;
            }
        }
    }
}
