    using System.Collections.Generic;
    using System.Threading.Tasks;

    namespace EPP.DA
    {
        public interface IGetWarehousesListBis
        {
            Task<List<sl_Magazyn>> GetSnFromSubiektToListAsync();
        }
    }
    