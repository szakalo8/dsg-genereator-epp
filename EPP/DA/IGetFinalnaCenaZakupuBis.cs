﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EPP.DA
{
    public interface IGetFinalnaCenaZakupuBis
    {
        decimal GetFinalCenaZakupu(string kodDostawy, decimal cenaNetto);
    }
}
