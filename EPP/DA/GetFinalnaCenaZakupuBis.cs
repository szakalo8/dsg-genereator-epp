﻿using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace EPP.DA
{
    public class GetFinalnaCenaZakupuBis : IGetFinalnaCenaZakupuBis
    {
        public decimal GetFinalCenaZakupu(string kodDostawy, decimal cenaNetto)
        {
            decimal  finalnaCena = 0;
            decimal finalnaCenaTemp = 0;
            decimal?[] cenaFinalna;
            // konwertuj cenaNetto na double


            using (konektEntities db = new konektEntities(Properties.Settings.Default.connectionStringKM))
            {
                cenaFinalna =  db.Database.SqlQuery<decimal?>("SELECT top 1 pd.pwd_Kwota01 AS 'Finalna cena zakupu' " +
                                                           "FROM Konekt_magazyn.dbo.dok__Dokument dd " +
                                                           "LEFT JOIN Konekt_magazyn.dbo.pw_Dane pd ON dd.dok_DoDokId = pd.pwd_IdObiektu " +
                                                           "LEFT JOIN dbo.dok_Pozycja dp ON dd.dok_DoDokId = dp.ob_DokMagId " +
                                                           "WHERE dp.ob_NumerSeryjny = @numSer " +
                                                           "AND dd.dok_Typ = 15", new SqlParameter("@numSer", kodDostawy)).ToArray();

            }

            if (cenaFinalna.Any() && cenaFinalna[0] != null)
            {
                finalnaCenaTemp = decimal.Parse(cenaFinalna[0].ToString());
            }


            if (finalnaCenaTemp != 0 && cenaNetto != 0)
            {
                finalnaCena = ((cenaNetto - finalnaCenaTemp) / 2) + finalnaCenaTemp;
            }
            else
            {
                finalnaCena = 0;
            }


            return finalnaCena;
        }
    }
}
