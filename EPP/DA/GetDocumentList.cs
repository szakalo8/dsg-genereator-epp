﻿using EPP.VO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EPP.DA
{
    public class GetDocumentList
    {
        public static List<dok__Dokument> GetWZFromWEKTORSubiektToList(int idMagazyn, DateTime dataOd, DateTime dataDo)
        {
            List<dok__Dokument> lista = new List<dok__Dokument>();

            using (konektEntities db = new konektEntities(Properties.Settings.Default.connectionStringKM))
            {
                lista = db.dok__Dokument.Where(a => a.dok_Typ == 11 && a.dok_MagId == idMagazyn && a.dok_DataWyst >= dataOd && a.dok_DataWyst <= dataDo && a.dok_PlatnikId == 446).ToList();
                foreach (var l in lista)
                {
                    l.ilosc_metrow = db.dok_Pozycja.Where(i=>i.ob_DokMagId == l.dok_Id).AsEnumerable().Sum(o => o.ob_Ilosc);
                }
                return lista;
            }
        }

        public static List<dok__Dokument> GetWZFromSubiektToList(List<int> kolekcja1, int idMagazyn, DateTime dataOd, DateTime dataDo)
        {
            List<dok__Dokument> lista = new List<dok__Dokument>();

            using (konektEntities db = new konektEntities(Properties.Settings.Default.connectionStringKM))
            {
                lista = db.dok__Dokument.Where(a => a.dok_Typ == 11 && a.dok_MagId == idMagazyn && a.dok_DataWyst >= dataOd && a.dok_DataWyst <= dataDo && kolekcja1.Contains(a.dok_Id)).ToList();

                foreach (var l in lista)
                {
                    l.ilosc_metrow = db.dok_Pozycja.Where(i => i.ob_DokMagId == l.dok_Id).AsEnumerable().Sum(o => o.ob_Ilosc);
                }
                return lista;
            }
        }

        public static List<dok__Dokument> GetWZFromKONEKTSubiektToList(DateTime dataOd, DateTime dataDo)
        {
            using (konektEntities db = new konektEntities(Properties.Settings.Default.connectionStringK)) ////tu popraw na K
            {
                return db.dok__Dokument.Where(a => a.dok_Typ == 11 && a.dok_DataWyst >= dataOd && a.dok_DataWyst <= dataDo && a.dok_Uwagi != "").ToList();
            }
        }

        public static List<dok__Dokument> GetWZFromKONEKTBISSubiektToList(DateTime dataOd, DateTime dataDo)
        {
            using (konektEntities db = new konektEntities(Properties.Settings.Default.connectionStringKB))
            {
                return db.dok__Dokument.Where(a => a.dok_Typ == 11 && a.dok_DataWyst >= dataOd && a.dok_DataWyst <= dataDo && a.dok_Uwagi != "").ToList();
            }
        }

        public static List<dok__Dokument> GetWZFromKOENKTTKANINYSubiektToList(DateTime dataOd, DateTime dataDo)
        {
            using (konektEntities db = new konektEntities(Properties.Settings.Default.connectionStringKT))
            {
                return db.dok__Dokument.Where(a => a.dok_Typ == 11 && a.dok_DataWyst >= dataOd && a.dok_DataWyst <= dataDo && a.dok_Uwagi != "").ToList();
            }
        }

        public static List<dok__Dokument> GetWZFromWEKTORBISSubiektToList(int idMagazyn, DateTime dataOd, DateTime dataDo)
        {
            List<dok__Dokument> lista = new List<dok__Dokument>();

            using (konektEntities db = new konektEntities(Properties.Settings.Default.connectionStringKM))
            {
                lista = db.dok__Dokument.Where(a => a.dok_Typ == 11 && a.dok_MagId == idMagazyn && a.dok_DataWyst >= dataOd && a.dok_DataWyst <= dataDo && a.dok_PlatnikId == 2917).ToList();
                foreach (var l in lista)
                {
                    l.ilosc_metrow = db.dok_Pozycja.Where(i => i.ob_DokMagId == l.dok_Id).AsEnumerable().Sum(o => o.ob_Ilosc);
                }
                return lista;
            }
        }

        public static List<TempPositionVO> GetInvoiceList(string odbiorca, string wystawca, string data)
        {
            var document_list = new List<dok__Dokument>();

            var tempPositionList = new List<TempPositionVO>();

            var firstLetters = string.Empty;

            var paternFirstLetters = string.Empty;

            var tempConnectionStr = string.Empty;

            DateTime date = Convert.ToDateTime(data);

            if (odbiorca == "WEKTOR")
            {
                tempConnectionStr = Properties.Settings.Default.connectionStringW;
            }

            if (odbiorca == "WEKTOR-BIS")
            {
                tempConnectionStr = Properties.Settings.Default.connectionStringWB;
            }

            if (wystawca == "KONEKT-BIS")
            {
                paternFirstLetters = "KB";
            }

            if (wystawca == "KONEKT")
            {
                paternFirstLetters = "K";
            }

            if (wystawca == "KOENKT-TKANINY")
            {
                paternFirstLetters = "KT";
            }

            if(wystawca == "ELEX TKANINY-BIS")
            {
                paternFirstLetters = "TB";
            }

            if(wystawca == "ELEX TKANINY")
            {
                paternFirstLetters = "T";
            }


            if(wystawca == "ELEX")
            {
                paternFirstLetters = "E";
            }

            if (tempConnectionStr == string.Empty)
            {
                return tempPositionList;
            }

            using (konektEntities db = new konektEntities(tempConnectionStr))
            {
                var dl = db.dok__Dokument.Where(a => a.dok_Typ == 2 && a.dok_DataWyst == date).ToList();

                foreach (var item in dl)
                {

                    var klient = db.vwKlienci.Where(s => s.kh_Id == item.dok_PlatId).FirstOrDefault();

                    foreach (var i in item.dok_Pozycja)
                    {
                        var towar = db.vwTowar.Where(s => s.tw_Id == i.ob_TowId).FirstOrDefault();
                        firstLetters = ExtractFirstLetters(i.ob_Opis);

                        if (firstLetters == paternFirstLetters)
                        {
                            var kodDostawy = i.ob_Opis.Split(new[] { '-' }, StringSplitOptions.RemoveEmptyEntries).Last().Trim();

                            var calculatedPrice = CalculatePrice(kodDostawy, decimal.Round(i.ob_CenaNetto,2));

                            var newTempPosition = new TempPositionVO
                            {

                                dok_NrPelny = item.dok_NrPelny,
                                dok_DataWyst = item.dok_DataWyst,
                                KodDostawy =  kodDostawy,
                                NazwaTowaru = towar.tw_Nazwa,
                                Ilosc = decimal.Round(i.ob_Ilosc, 2),
                                CenaJednostkowa = decimal.Round(i.ob_CenaNetto,2),
                                WartoscNetto = decimal.Round(i.ob_WartNetto, 2),
                                WartoscBrutto = decimal.Round(i.ob_WartBrutto, 2),
                                NipOdbiorcy = klient != null ? klient.adr_NIP : "",
                                NazwaOdbiorcy = klient != null ? klient.adr_Nazwa : "",
                                CenaDocelowaNetto = calculatedPrice,
                                WartoscDocelowaNetto = decimal.Round(calculatedPrice * i.ob_Ilosc, 2),
                                WartoscDocelowaBrutto = decimal.Round((calculatedPrice * i.ob_Ilosc) * 1.23m, 2)

                            };

                            tempPositionList.Add(newTempPosition);

                        }
                    }
                }
            }


            foreach (var item in document_list)
            {

                var pozycje = item.dok_Pozycja.ToList();

                if (item.dok_Pozycja.Count == 0)
                {
                    Console.WriteLine("Brak pozycji");
                }
                else
                {

                    foreach (var i in item.dok_Pozycja)
                    {
                        Console.WriteLine(i.ob_Opis.Split(new[] { '-' }, StringSplitOptions.RemoveEmptyEntries).Last().Trim());

                    }
                }

            }



            return tempPositionList;
        }

        private static decimal CalculatePrice(string kodDostawy, decimal cenaNetto)
        {
            decimal finalnaCena = 0;
            finalnaCena = new GetFinalnaCenaZakupuBis().GetFinalCenaZakupu(kodDostawy, cenaNetto);

            return finalnaCena;
        }

        public static string ExtractFirstLetters(string input)
        {
            var serial = input.Split(new[] { '-' }, StringSplitOptions.RemoveEmptyEntries).Last().Trim();
            var firstLetters = serial.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).First().Trim();
            return firstLetters;
        }


    }
}
