﻿using System.Linq;
using System.Data.SqlClient;

namespace EPP.DA
{
    public class GetFinalnaCenaZakupu
    {
        public static double GetFinalCenaZakupu(string nrDok, string lpPozz)
        {

                int idDok = 0;
                string kodDostawy = "";
                decimal?[] cenaFinalna;
                int lpPoz = int.Parse(lpPozz);
                using (konektEntities db = new konektEntities(Properties.Settings.Default.connectionStringKM))
                {
                    idDok = db.dok__Dokument.Single(a => a.dok_NrPelny == nrDok).dok_Id;
                    kodDostawy = db.dok_Pozycja.Single(a => a.ob_DokMagId == idDok && a.ob_DokMagLp == lpPoz).ob_NumerSeryjny;
                }

                if (kodDostawy == null)
                    return 0;

                using (konektEntities db = new konektEntities(Properties.Settings.Default.connectionStringKM))
                {
                    // cenaFinalna = db.Database.SqlQuery<string>("SELECT LOWER(column_name) AS column_name FROM information_schema.columns WHERE table_name = @p0", tableName).ToArray();
                    cenaFinalna =  db.Database.SqlQuery<decimal?>("SELECT  pd.pwd_Kwota01 AS 'Finalna cena zakupu' " +
                                                               "FROM Konekt_magazyn.dbo.dok__Dokument dd " +
                                                               "LEFT JOIN Konekt_magazyn.dbo.pw_Dane pd ON dd.dok_DoDokId = pd.pwd_IdObiektu " +
                                                               "LEFT JOIN dbo.dok_Pozycja dp ON dd.dok_DoDokId = dp.ob_DokMagId " +
                                                               "WHERE dp.ob_NumerSeryjny = @numSer " +
                                                               "AND dd.dok_Typ = 15", new SqlParameter("@numSer", kodDostawy)).ToArray();
                if (cenaFinalna[0] == null)
                    return 0;

                }


                return double.Parse(cenaFinalna[0].ToString());

        }
    }
}
