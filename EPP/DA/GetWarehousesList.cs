﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;

namespace EPP.DA
{
    public class GetWarehousesList
    {
        public static List<sl_Magazyn> GetSnFromSubiektToList()
        {
            using (konektEntities db = new konektEntities(Properties.Settings.Default.connectionStringKM))
            {
                return db.sl_Magazyn.Where(s=>s.mag_Nazwa != "GŁÓWNY").ToList();
            }
        }

    }
}
