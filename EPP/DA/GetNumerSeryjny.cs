﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EPP.DA
{
    public class GetNumerSeryjny
    {
        public static string GetNumerSeryjnyFromPoz(int id_poz, int id_dok)
        {
            string nr_ser = null;

            using (konektEntities db = new konektEntities(Properties.Settings.Default.connectionStringKM))
            {
                nr_ser = db.dok_Pozycja.SingleOrDefault(p => p.ob_DokMagLp == id_poz && p.ob_DokMagId == id_dok).ob_NumerSeryjny;

                return nr_ser;
            }
        }
    }
}
