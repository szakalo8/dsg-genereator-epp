﻿using EPP.VO;
using InsERT;
using MetroFramework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using static EPP.GenerujEPP;
using Microsoft.CSharp;
using System.Threading;
using EPP.DA;

namespace EPP
{
    public partial class InvoiceView : MetroFramework.Controls.MetroUserControl
    {
        /// <summary>
        /// Obiekt Subiekt GT
        /// </summary>
        InsERT.Subiekt _sgt;

        Firma firma;

        public InvoiceView()
        {
            InitializeComponent();
        }

        private void metroDateTimeOd_ValueChanged(object sender, EventArgs e)
        {

        }

        private async void InvoiceView_Load(object sender, EventArgs e)
        {

            try
            {
                IGetWarehousesListBis getWarehousesListBis = new GetWarehousesListBis();
                var warehousesList = await getWarehousesListBis.GetSnFromSubiektToListAsync();

                var warehousesList1 = new List<sl_Magazyn>();
                warehousesList1.Add(new
                    sl_Magazyn
                {
                    mag_Symbol = "W",
                    mag_Nazwa = "WEKTOR"
                });
                warehousesList1.Add(new
                    sl_Magazyn
                {
                    mag_Symbol = "WB",
                    mag_Nazwa = "WEKTOR-BIS"
                });
                magazynVOBindingSource.DataSource = warehousesList.OrderBy(s=>s.mag_Nazwa);
                magazynVOBindingSource1.DataSource =  warehousesList1.OrderBy(s => s.mag_Nazwa);
            }
            catch (Exception ex)
            {
                MetroMessageBox.Show(this, ex.Message, "Zamknąć?", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button2);
            }

        }

        private void metroButtonSzukajWZs_Click(object sender, EventArgs e)
        {

            tempPositionVOBindingSource.DataSource = null;

            List<TempPositionVO> documentList = new List<TempPositionVO>();

            documentList = DA.GetDocumentList.GetInvoiceList(metroComboBoxOdbiorca.Text, metroComboBoxWystawca.Text, metroDateTime.Text);

            if(documentList.Count == 0)
            {
                MetroMessageBox.Show(this, "Brak dokumentów do wystawienia faktury VAT", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }


            foreach (var item in documentList)
            {
                if (item.CenaDocelowaNetto == 0)
                {
                    var respond =  MetroMessageBox.Show(this, "Brak ceny zakupu dla towaru: "+ item.NazwaTowaru + " o kodzie dostawy - "+ item.KodDostawy + "\nNiektóre pozycje na utworzonej fakturze mogą mieć cenę zerową. \nNależy uzupełnić cenę zakupu i uruchomić ponownie wyszukiwanie.\nCzy chcesz kontynuować? ", "Błąd", MessageBoxButtons.YesNo, MessageBoxIcon.Error, MessageBoxDefaultButton.Button2);

                    if (respond == DialogResult.No)
                    {
                        return;
                    }
                }
            }


            if (documentList.Count > 0)
            {
                tempPositionVOBindingSource.DataSource = documentList;
            }



        }

        private void metroGridDokumentyWZ_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            // sortowanie po kolumnie 

        }


        void GenerateInsertFV() { 

            string wystawcaText = string.Empty;
            string odbiorcaText = string.Empty;
            string dataWystawienia = string.Empty;


            if (metroComboBoxWystawca.InvokeRequired)
            {
                metroComboBoxWystawca.Invoke(new MethodInvoker(delegate
                {
                    wystawcaText = metroComboBoxWystawca.Text;
                }));
            }
            else
            {
                wystawcaText = metroComboBoxWystawca.Text;
            }

            if (metroComboBoxOdbiorca.InvokeRequired)
            {
                metroComboBoxOdbiorca.Invoke(new MethodInvoker(delegate
                {
                    odbiorcaText = metroComboBoxOdbiorca.Text;
                }));
            }
            else
            {
                odbiorcaText = metroComboBoxOdbiorca.Text;
            }

            if (metroDateTime.InvokeRequired)
            {
                metroDateTime.Invoke(new MethodInvoker(delegate
                {
                    dataWystawienia = metroDateTime.Text;
                }));
            }
            else
            {
                dataWystawienia = metroDateTime.Text;
            }


            switch (wystawcaText)
            {
                case "WEKTOR":
                    firma = Firma.Wektor;
                    break;
                case "KONEKT":
                    firma = Firma.Konekt;
                    break;
                case "KONEKT-BIS":
                    firma = Firma.KonektBis;
                    break;
                case "KONEKT-TKANINY":
                    firma = Firma.KonektTkaniny;
                    break;
                case "WEKTOR-BIS":
                    firma = Firma.WektorBis;
                    break;
                case "ELEX":
                    firma = Firma.Elex;
                    break;
                case "ELEX-BIS":
                    firma = Firma.ElexBis;
                    break;
                case "ELEX TKANINY":
                    firma = Firma.ElexTkaniny;
                    break;
                case "ELEX TKANINY-BIS":
                    firma = Firma.ElexTkaninyBis;
                    break;
                default:
                    MetroMessageBox.Show(this, "Nie wybrano firmy wystawiającej FV", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button2);
                    break;
            }


            // pobierz dokumnety z tabeli 
            List<TempPositionVO> documentList = new List<TempPositionVO>();
            documentList = DA.GetDocumentList.GetInvoiceList(odbiorcaText, wystawcaText, dataWystawienia);

            // documentList sumujemy w obrębie kontrahenta, partii, ceny 
            List<TempPositionVO> documentListSum = new List<TempPositionVO>();
            List<TempPositionVO> tempList = new List<TempPositionVO>();

            foreach (var item in documentList)
            {
                bool found = false;
                foreach (var itemSum in documentListSum)
                {
                    if (itemSum.NipOdbiorcy == item.NipOdbiorcy && itemSum.KodDostawy == item.KodDostawy && itemSum.CenaJednostkowa == item.CenaJednostkowa)
                    {
                        itemSum.Ilosc += item.Ilosc;
                        itemSum.WartoscBrutto += item.WartoscBrutto;
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    tempList.Add(item);
                }
            }

            documentListSum.AddRange(tempList);


            if (documentListSum.Count == 0)
            {
                MetroMessageBox.Show(this, "Brak dokumentów do wystawienia faktury VAT", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            Thread thread = new Thread(() => GenerateInvoice(documentListSum, wystawcaText));
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            thread.Join();

        }

        private void metroButtonGenerujFV_Click(object sender, EventArgs e)
        {


            using (var waitForm = new WaitForm(GenerateInsertFV))
            {
                waitForm.ShowDialog(this);
            }


        }

        private void GenerateInvoice(List<TempPositionVO> documentListSum, string wystawca)
        {

            var docNumber = string.Empty;
            try
            {
              SferaConn();
            }
            catch (Exception ex)
            {
                //MetroMessageBox.Show(this, ex.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button2);
                // close wait form
                MessageBox.Show(ex.Message);

            }

            if (wystawca == "KONEKT-BIS")
                docNumber = GenerateFVKB(documentListSum);

            if (wystawca == "KONEKT")
                docNumber = GenerateFVKB(documentListSum);

            if (wystawca == "ELEX TKANINY")
                docNumber = GenerateFVKB(documentListSum);

            if (wystawca == "ELEX TKANINY-BIS")
                docNumber = GenerateFVKB(documentListSum);

            if (_sgt != null)
                _sgt.Zakoncz();

            if (docNumber != string.Empty)
                MetroMessageBox.Show(this, "Faktura VAT nr: " + docNumber + " w systemie Subiekt "  + wystawca + " została wygenerowana", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else
                MetroMessageBox.Show(this, "Nie udało się wygenerować faktury VAT", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button2);
        }

        private string GenerateFVKB(List<TempPositionVO> documentListSum)
        {
            string nip = string.Empty;
            if(firma == Firma.KonektBis || firma == Firma.ElexTkaninyBis)
            {
                 nip = "6572275847";
                _sgt.MagazynId = 1;

            }
            if(firma == Firma.Konekt || firma == Firma.ElexTkaniny)
            {
                nip = "657-227-58-47";
                _sgt.MagazynId = 1;

            }

            // Ensure the dynamic type is properly cast
            var kontrahentDynamic = _sgt.Kontrahenci.Wczytaj(nip);
            Kontrahent kontrahent = kontrahentDynamic as Kontrahent;

            if (kontrahent == null)
            {
                throw new InvalidOperationException("Failed to load Kontrahent.");
            }
            SuDokument fv = null;

            fv = (SuDokument)_sgt.SuDokumentyManager.DodajFS();
            fv.StatusDokumentu = SubiektDokumentStatusEnum.gtaSubiektDokumentStatusOdlozony;
            fv.KontrahentId = kontrahent.Identyfikator;
            fv.DataWystawienia = metroDateTime.Value;
            fv.DataZakonczeniaDostawy = metroDateTime.Value;

            foreach (var item in documentListSum)
            {
                if (!_sgt.Towary.Istnieje(item.KodDostawy))
                {
                    continue;
                }

                var towar = _sgt.Towary.Wczytaj(item.KodDostawy) as Towar;

                if (towar != null)
                {
                    SuPozycja poz = fv.Pozycje.Dodaj(towar.Identyfikator) as SuPozycja;
                    poz.IloscJm = (decimal)item.Ilosc;
                    poz.CenaNettoPoRabacie = (decimal)item.CenaDocelowaNetto;
                    poz.WartoscBruttoPoRabacie = (decimal)item.WartoscDocelowaBrutto;
                    poz.WartoscNettoPoRabacie = (decimal)item.WartoscDocelowaNetto;
                }
            }

            fv.Przelicz();
            fv.Zapisz();

            string docNumber = fv.NumerPelny;

            fv.Zamknij();

            return docNumber;
        }

        private void SferaConn()
        {
            if (firma == Firma.Wektor)
            {
                var gt = new GT
                {
                    Produkt = ProduktEnum.gtaProduktSubiekt,
                    Autentykacja = AutentykacjaEnum.gtaAutentykacjaMieszana,
                    Serwer = Properties.Settings.Default.serverW.Trim(),
                    Baza = Properties.Settings.Default.databaseW.Trim(),
                    Uzytkownik = Properties.Settings.Default.userW.Trim(),
                    UzytkownikHaslo = Properties.Settings.Default.passwordW.Trim(),
                   // Operator = Properties.Settings.Default.sferaUserW.Trim(),
                   // OperatorHaslo = Properties.Settings.Default.sferaPasswordKM
                };
                _sgt = (InsERT.Subiekt)gt.Uruchom((int)UruchomDopasujEnum.gtaUruchomDopasuj,
                    (int)UruchomEnum.gtaUruchomNowy | (int)UruchomEnum.gtaUruchomWTle);
            }
            else if (firma == Firma.Konekt)
            {
                var gt = new GT
                {
                    Produkt = ProduktEnum.gtaProduktSubiekt,
                    Autentykacja = AutentykacjaEnum.gtaAutentykacjaMieszana,
                    Serwer = Properties.Settings.Default.serverK.Trim(),
                    Baza = Properties.Settings.Default.databaseK.Trim(),
                    Uzytkownik = Properties.Settings.Default.userK.Trim(),
                    UzytkownikHaslo = Properties.Settings.Default.passwordK.Trim(),
                    Operator = Properties.Settings.Default.sferaUserK.Trim(),
                    OperatorHaslo = Properties.Settings.Default.sferaPasswordK
                };
                _sgt = (InsERT.Subiekt)gt.Uruchom((int)UruchomDopasujEnum.gtaUruchomDopasuj,
                    (int)UruchomEnum.gtaUruchomNowy | (int)UruchomEnum.gtaUruchomWTle);
            }
            else if (firma == Firma.KonektBis)
            {
                var gt = new GT
                {
                    Produkt = ProduktEnum.gtaProduktSubiekt,
                    Autentykacja = AutentykacjaEnum.gtaAutentykacjaMieszana,
                    Serwer = Properties.Settings.Default.serverKB.Trim(),
                    Baza = Properties.Settings.Default.databaseKB.Trim(),
                    Uzytkownik = Properties.Settings.Default.userKB.Trim(),
                    UzytkownikHaslo = Properties.Settings.Default.passwordKB.Trim(),
                    Operator = Properties.Settings.Default.sferaUserKB.Trim(),
                    OperatorHaslo = Properties.Settings.Default.sferaPasswordKB
                };
                _sgt = (InsERT.Subiekt)gt.Uruchom((int)UruchomDopasujEnum.gtaUruchomDopasuj,
                    (int)UruchomEnum.gtaUruchomNowy | (int)UruchomEnum.gtaUruchomWTle);
            }
            else if (firma == Firma.KonektTkaniny)
            {
                var gt = new GT
                {
                    Produkt = ProduktEnum.gtaProduktSubiekt,
                    Autentykacja = AutentykacjaEnum.gtaAutentykacjaMieszana,
                    Serwer = Properties.Settings.Default.serverKT.Trim(),
                    Baza = Properties.Settings.Default.databaseKT.Trim(),
                    Uzytkownik = Properties.Settings.Default.userKT.Trim(),
                    UzytkownikHaslo = Properties.Settings.Default.passwordKT.Trim(),
                    Operator = Properties.Settings.Default.sferaUserKT.Trim(),
                    OperatorHaslo = Properties.Settings.Default.sferaPasswordKT
                };
                _sgt = (InsERT.Subiekt)gt.Uruchom((int)UruchomDopasujEnum.gtaUruchomDopasuj,
                    (int)UruchomEnum.gtaUruchomNowy | (int)UruchomEnum.gtaUruchomWTle);
            }
            else if (firma == Firma.WektorBis)
            {
                var gt = new GT
                {
                    Produkt = ProduktEnum.gtaProduktSubiekt,
                    Autentykacja = AutentykacjaEnum.gtaAutentykacjaMieszana,
                    Serwer = Properties.Settings.Default.serverKM.Trim(),
                    Baza = Properties.Settings.Default.databaseKM.Trim(),
                    Uzytkownik = Properties.Settings.Default.userKM.Trim(),
                    UzytkownikHaslo = Properties.Settings.Default.passwordKM.Trim(),
                    Operator = Properties.Settings.Default.sferaUserKM.Trim(),
                    OperatorHaslo = Properties.Settings.Default.sferaPasswordKM
                };
                _sgt = (InsERT.Subiekt)gt.Uruchom((int)UruchomDopasujEnum.gtaUruchomDopasuj,
                    (int)UruchomEnum.gtaUruchomNowy | (int)UruchomEnum.gtaUruchomWTle);
            }
            else if (firma == Firma.ElexTkaniny)
            {
                var gt = new GT
                {
                    Produkt = ProduktEnum.gtaProduktSubiekt,
                    Autentykacja = AutentykacjaEnum.gtaAutentykacjaMieszana,
                    Serwer = Properties.Settings.Default.serverT.Trim(),
                    Baza = Properties.Settings.Default.databaseT.Trim(),
                    Uzytkownik = Properties.Settings.Default.userT.Trim(),
                    UzytkownikHaslo = Properties.Settings.Default.passwordT.Trim(),
                    Operator = Properties.Settings.Default.sferaUserT.Trim(),
                    OperatorHaslo = Properties.Settings.Default.sferaPasswordT
                };
                _sgt = (InsERT.Subiekt)gt.Uruchom((int)UruchomDopasujEnum.gtaUruchomDopasuj,
                    (int)UruchomEnum.gtaUruchomNowy | (int)UruchomEnum.gtaUruchomWTle);
            }
            else if (firma == Firma.ElexTkaninyBis)
            {
                var gt = new GT
                {
                    Produkt = ProduktEnum.gtaProduktSubiekt,
                    Autentykacja = AutentykacjaEnum.gtaAutentykacjaMieszana,
                    Serwer = Properties.Settings.Default.serverTB.Trim(),
                    Baza = Properties.Settings.Default.databaseTB.Trim(),
                    Uzytkownik = Properties.Settings.Default.userTB.Trim(),
                    UzytkownikHaslo = Properties.Settings.Default.passwordTB.Trim(),
                    Operator = Properties.Settings.Default.sferaUserTB.Trim(),
                    OperatorHaslo = Properties.Settings.Default.sferaPasswordTB
                };
                _sgt = (InsERT.Subiekt)gt.Uruchom((int)UruchomDopasujEnum.gtaUruchomDopasuj,
                    (int)UruchomEnum.gtaUruchomNowy | (int)UruchomEnum.gtaUruchomWTle);
            }
        }
    }
}
