﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.Data;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;

namespace EPP
{
    public partial class Konfiguracja : MetroFramework.Controls.MetroUserControl
    {
        public Konfiguracja()
        {
            InitializeComponent();
        }

        private void metroTileZapisz_Click(object sender, EventArgs e)
        {
            SaveConfig();
            MetroMessageBox.Show(this, "Poprawnie zapisano ustawienia", "OK", MessageBoxButtons.OK,
                MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
        }

        private void SaveConfig()
        {
            #region Konekt Magazyn

            string serverKM = metroTextBox_serverKM.Text;
            string userKM = metroTextBox_userKM.Text;
            string passwordKM = metroTextBox_passKM.Text;
            bool authKM = metroCheckBox_authKM.Checked;
            string databaseKM = comboBox_DatabaseKM.Text;

            string sferaUserKM = metroTextBox_sfera_userKM.Text;
            string sferaPasswordKM = metroTextBox_sfera_passwordKM.Text;

            Properties.Settings.Default.serverKM = serverKM;
            Properties.Settings.Default.userKM = userKM;
            Properties.Settings.Default.passwordKM = passwordKM;
            Properties.Settings.Default.authKM = authKM;
            Properties.Settings.Default.databaseKM = databaseKM;
            Properties.Settings.Default.sferaUserKM = sferaUserKM;
            Properties.Settings.Default.sferaPasswordKM = sferaPasswordKM;

            var entityConnectionStringKM = new EntityConnectionStringBuilder
            {
                Provider = "System.Data.SqlClient",
                ProviderConnectionString = $"data source={serverKM};initial catalog={databaseKM};integrated security={authKM};user id={userKM};password={passwordKM};",
                Metadata = "res://*/"
                // "res://*/Model.Model.csdl|res://*/Model.Model.ssdl|res://*/Model.msl"
            };

            Properties.Settings.Default.connectionStringKM = entityConnectionStringKM.ToString();

            #endregion

            #region Konekt

            string serverK = metroTextBox_serverK.Text;
            string userK = metroTextBox_userK.Text;
            string passwordK = metroTextBox_passK.Text;
            bool authK = metroCheckBox_authK.Checked;
            string databaseK = comboBox_DatabaseK.Text;

            string sferaUserK = metroTextBox_sfera_userK.Text;
            string sferaPasswordK = metroTextBox_sfera_passwordK.Text;

            Properties.Settings.Default.serverK = serverK;
            Properties.Settings.Default.userK = userK;
            Properties.Settings.Default.passwordK = passwordK;
            Properties.Settings.Default.authK = authK;
            Properties.Settings.Default.databaseK = databaseK;
            Properties.Settings.Default.sferaUserK = sferaUserK;
            Properties.Settings.Default.sferaPasswordK = sferaPasswordK;

            var entityConnectionStringK = new EntityConnectionStringBuilder
            {
                Provider = "System.Data.SqlClient",
                ProviderConnectionString = $"data source={serverK};initial catalog={databaseK};integrated security={authK};user id={userK};password={passwordK};",
                Metadata = "res://*/"
                // "res://*/Model.Model.csdl|res://*/Model.Model.ssdl|res://*/Model.msl"
            };

            Properties.Settings.Default.connectionStringK = entityConnectionStringK.ToString();

            #endregion

            #region Konekt BIS

            string serverKB = metroTextBox_serverKB.Text;
            string userKB = metroTextBox_userKB.Text;
            string passwordKB = metroTextBox_passKB.Text;
            bool authKB = metroCheckBox_authKB.Checked;
            string databaseKB = comboBox_DatabaseKB.Text;

            string sferaUserKB = metroTextBox_sfera_userKB.Text;
            string sferaPasswordKB = metroTextBox_sfera_passwordKB.Text;

            Properties.Settings.Default.serverKB = serverKB;
            Properties.Settings.Default.userKB = userKB;
            Properties.Settings.Default.passwordKB = passwordKB;
            Properties.Settings.Default.authKB = authKB;
            Properties.Settings.Default.databaseKB = databaseKB;
            Properties.Settings.Default.sferaUserKB = sferaUserKB;
            Properties.Settings.Default.sferaPasswordKB = sferaPasswordKB;

            var entityConnectionStringKB = new EntityConnectionStringBuilder
            {
                Provider = "System.Data.SqlClient",
                ProviderConnectionString = $"data source={serverKB};initial catalog={databaseKB};integrated security={authKB};user id={userKB};password={passwordKB};",
                Metadata = "res://*/"
                // "res://*/Model.Model.csdl|res://*/Model.Model.ssdl|res://*/Model.msl"
            };

            Properties.Settings.Default.connectionStringKB = entityConnectionStringKB.ToString();

            #endregion

            #region Elex Tkaniny

            string serverT = metroTextBox_serverT.Text;
            string userT = metroTextBox_userT.Text;
            string passwordT = metroTextBox_passT.Text;
            bool authT = metroCheckBox_authT.Checked;
            string databaseT = comboBox_DatabaseT.Text;

            string sferaUserT = metroTextBox_sfera_userT.Text;
            string sferaPasswordT = metroTextBox_sfera_passwordT.Text;

            Properties.Settings.Default.serverT = serverT;
            Properties.Settings.Default.userT = userT;
            Properties.Settings.Default.passwordT = passwordT;
            Properties.Settings.Default.authT = authT;
            Properties.Settings.Default.databaseT = databaseT;
            Properties.Settings.Default.sferaUserT = sferaUserT;
            Properties.Settings.Default.sferaPasswordT = sferaPasswordT;

            var entityConnectionStringT = new EntityConnectionStringBuilder
            {
                Provider = "System.Data.SqlClient",
                ProviderConnectionString = $"data source={serverT};initial catalog={databaseT};integrated security={authT};user id={userT};password={passwordT};",
                Metadata = "res://*/"
                // "res://*/Model.Model.csdl|res://*/Model.Model.ssdl|res://*/Model.msl"
            };

            Properties.Settings.Default.connectionStringT = entityConnectionStringT.ToString();

            #endregion

            #region Elex Tkaniny

            string serverTB = metroTextBox_serverTB.Text;
            string userTB = metroTextBox_userTB.Text;
            string passwordTB = metroTextBox_passTB.Text;
            bool authTB = metroCheckBox_authTB.Checked;
            string databaseTB = comboBox_DatabaseTB.Text;

            string sferaUserTB = metroTextBox_sfera_userTB.Text;
            string sferaPasswordTB = metroTextBox_sfera_passwordTB.Text;

            Properties.Settings.Default.serverTB = serverTB;
            Properties.Settings.Default.userTB = userTB;
            Properties.Settings.Default.passwordTB = passwordTB;
            Properties.Settings.Default.authTB = authTB;
            Properties.Settings.Default.databaseTB = databaseTB;
            Properties.Settings.Default.sferaUserTB = sferaUserTB;
            Properties.Settings.Default.sferaPasswordTB = sferaPasswordTB;

            var entityConnectionStringTB = new EntityConnectionStringBuilder
            {
                Provider = "System.Data.SqlClient",
                ProviderConnectionString = $"data source={serverTB};initial catalog={databaseTB};integrated security={authTB};user id={userTB};password={passwordTB};",
                Metadata = "res://*/"
                // "res://*/Model.Model.csdl|res://*/Model.Model.ssdl|res://*/Model.msl"
            };

            Properties.Settings.Default.connectionStringTB = entityConnectionStringTB.ToString();

            #endregion

            #region Konekt Tkaniny

            string serverKT = metroTextBox_serverKT.Text;
            string userKT = metroTextBox_userKT.Text;
            string passwordKT = metroTextBox_passKT.Text;
            bool authKT = metroCheckBox_authKT.Checked;
            string databaseKT = comboBox_DatabaseKT.Text;

            string sferaUserKT = metroTextBox_sfera_userKT.Text;
            string sferaPasswordKT = metroTextBox_sfera_passwordKT.Text;

            Properties.Settings.Default.serverKT = serverKT;
            Properties.Settings.Default.userKT = userKT;
            Properties.Settings.Default.passwordKT = passwordKT;
            Properties.Settings.Default.authKT = authKT;
            Properties.Settings.Default.databaseKT = databaseKT;
            Properties.Settings.Default.sferaUserKT = sferaUserKT;
            Properties.Settings.Default.sferaPasswordKT = sferaPasswordKT;

            var entityConnectionStringKT = new EntityConnectionStringBuilder
            {
                Provider = "System.Data.SqlClient",
                ProviderConnectionString = $"data source={serverKT};initial catalog={databaseKT};integrated security={authKT};user id={userKT};password={passwordKT};",
                Metadata = "res://*/"
                // "res://*/Model.Model.csdl|res://*/Model.Model.ssdl|res://*/Model.msl"
            };

            Properties.Settings.Default.connectionStringKT = entityConnectionStringKT.ToString();

            #endregion

            #region Wektor Bis

            string serverWB = metroTextBox_serverWB.Text;
            string userWB = metroTextBox_userWB.Text;
            string passwordWB = metroTextBox_passWB.Text;
            bool authWB = metroCheckBox_authWB.Checked;
            string databaseWB = comboBox_DatabaseWB.Text;

            string sferaUserWB = metroTextBox_sfera_userWB.Text;
            string sferaPasswordWB = metroTextBox_sfera_passwordWB.Text;

            Properties.Settings.Default.serverWB = serverWB;
            Properties.Settings.Default.userWB = userWB;
            Properties.Settings.Default.passwordWB = passwordWB;
            Properties.Settings.Default.authWB = authWB;
            Properties.Settings.Default.databaseWB = databaseWB;
            Properties.Settings.Default.sferaUserWB = sferaUserWB;
            Properties.Settings.Default.sferaPasswordWB = sferaPasswordWB;

            var entityConnectionStringWB = new EntityConnectionStringBuilder
            {
                Provider = "System.Data.SqlClient",
                ProviderConnectionString = $"data source={serverWB};initial catalog={databaseWB};integrated security={authWB};user id={userWB};password={passwordWB};",
                Metadata = "res://*/"
                // "res://*/Model.Model.csdl|res://*/Model.Model.ssdl|res://*/Model.msl"
            };

            Properties.Settings.Default.connectionStringWB = entityConnectionStringWB.ToString();

            #endregion

            #region Wektor 

            string serverW = metroTextBox_serverW.Text;
            string userW = metroTextBox_userW.Text;
            string passwordW = metroTextBox_passW.Text;
            bool authW = metroCheckBox_authW.Checked;
            string databaseW = comboBox_DatabaseW.Text;


            Properties.Settings.Default.serverW = serverW;
            Properties.Settings.Default.userW = userW;
            Properties.Settings.Default.passwordW = passwordW;
            Properties.Settings.Default.authW = authW;
            Properties.Settings.Default.databaseW = databaseW;

            var entityConnectionStringW = new EntityConnectionStringBuilder
            {
                Provider = "System.Data.SqlClient",
                ProviderConnectionString = $"data source={serverW};initial catalog={databaseW};integrated security={authW};user id={userW};password={passwordW};",
                Metadata = "res://*/"
                // "res://*/Model.Model.csdl|res://*/Model.Model.ssdl|res://*/Model.msl"
            };

            Properties.Settings.Default.connectionStringW = entityConnectionStringW.ToString();

            #endregion

            string folderEpp = metroTextBoxFolderEpp.Text.Trim();
            Properties.Settings.Default.katalogPliku = folderEpp;

            Properties.Settings.Default.Save();
        }

        private void Konfiguracja_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.UpdateSettings)
            {
                Properties.Settings.Default.Upgrade();
                Properties.Settings.Default.UpdateSettings = false;
                Properties.Settings.Default.Save();
            }

            ReadConfig();
        }

        private void ReadConfig()
        {
            #region Konekt Magazyn

            string serverKM = Properties.Settings.Default.serverKM;
            string userKM = Properties.Settings.Default.userKM;
            string passwordKM = Properties.Settings.Default.passwordKM;
            bool authKM = Properties.Settings.Default.authKM;
            string databaseKM = Properties.Settings.Default.databaseKM;

            string sferaUserKM = Properties.Settings.Default.sferaUserKM;
            string sferaPasswordKM = Properties.Settings.Default.sferaPasswordKM;

            metroTextBox_serverKM.Text = serverKM;
            metroTextBox_userKM.Text = userKM;
            metroTextBox_passKM.Text = passwordKM;
            metroCheckBox_authKM.Checked = authKM;
            comboBox_DatabaseKM.Text = databaseKM;

            metroTextBox_sfera_userKM.Text = sferaUserKM;
            metroTextBox_sfera_passwordKM.Text = sferaPasswordKM;

            #endregion

            #region Konekt

            string serverK = Properties.Settings.Default.serverK;
            string userK = Properties.Settings.Default.userK;
            string passwordK = Properties.Settings.Default.passwordK;
            bool authK = Properties.Settings.Default.authK;
            string databaseK = Properties.Settings.Default.databaseK;

            string sferaUserK = Properties.Settings.Default.sferaUserK;
            string sferaPasswordK = Properties.Settings.Default.sferaPasswordK;

            metroTextBox_serverK.Text = serverK;
            metroTextBox_userK.Text = userK;
            metroTextBox_passK.Text = passwordK;
            metroCheckBox_authK.Checked = authK;
            comboBox_DatabaseK.Text = databaseK;

            metroTextBox_sfera_userK.Text = sferaUserK;
            metroTextBox_sfera_passwordK.Text = sferaPasswordK;

            #endregion

            #region Konekt BIS

            string serverKB = Properties.Settings.Default.serverKB;
            string userKB = Properties.Settings.Default.userKB;
            string passwordKB = Properties.Settings.Default.passwordKB;
            bool authKB = Properties.Settings.Default.authKB;
            string databaseKB = Properties.Settings.Default.databaseKB;

            string sferaUserKB = Properties.Settings.Default.sferaUserKB;
            string sferaPasswordKB = Properties.Settings.Default.sferaPasswordKB;

            metroTextBox_serverKB.Text = serverKB;
            metroTextBox_userKB.Text = userKB;
            metroTextBox_passKB.Text = passwordKB;
            metroCheckBox_authKB.Checked = authKB;
            comboBox_DatabaseKB.Text = databaseKB;

            metroTextBox_sfera_userKB.Text = sferaUserKB;
            metroTextBox_sfera_passwordKB.Text = sferaPasswordKB;

            #endregion

            #region Konekt Tkaniny

            string serverKT = Properties.Settings.Default.serverKT;
            string userKT = Properties.Settings.Default.userKT;
            string passwordKT = Properties.Settings.Default.passwordKT;
            bool authKT = Properties.Settings.Default.authKT;
            string databaseKT = Properties.Settings.Default.databaseKT;

            string sferaUserKT = Properties.Settings.Default.sferaUserKT;
            string sferaPasswordKT = Properties.Settings.Default.sferaPasswordKT;

            metroTextBox_serverKT.Text = serverKT;
            metroTextBox_userKT.Text = userKT;
            metroTextBox_passKT.Text = passwordKT;
            metroCheckBox_authKT.Checked = authKT;
            comboBox_DatabaseKT.Text = databaseKT;

            metroTextBox_sfera_userKT.Text = sferaUserKT;
            metroTextBox_sfera_passwordKT.Text = sferaPasswordKT;

            #endregion

            #region Wektor Bis

            string serverWB = Properties.Settings.Default.serverWB;
            string userWB = Properties.Settings.Default.userWB;
            string passwordWB = Properties.Settings.Default.passwordWB;
            bool authWB = Properties.Settings.Default.authWB;
            string databaseWB = Properties.Settings.Default.databaseWB;

            string sferaUserWB = Properties.Settings.Default.sferaUserWB;
            string sferaPasswordWB = Properties.Settings.Default.sferaPasswordWB;

            metroTextBox_serverWB.Text = serverWB;
            metroTextBox_userWB.Text = userWB;
            metroTextBox_passWB.Text = passwordWB;
            metroCheckBox_authWB.Checked = authWB;
            comboBox_DatabaseWB.Text = databaseWB;

            metroTextBox_sfera_userWB.Text = sferaUserWB;
            metroTextBox_sfera_passwordWB.Text = sferaPasswordWB;

            #endregion

            #region Wektor

            string serverW = Properties.Settings.Default.serverW;
            string userW = Properties.Settings.Default.userW;
            string passwordW = Properties.Settings.Default.passwordW;
            bool authW = Properties.Settings.Default.authWB;
            string databaseW = Properties.Settings.Default.databaseW;

            metroTextBox_serverW.Text = serverW;
            metroTextBox_userW.Text = userW;
            metroTextBox_passW.Text = passwordW;
            metroCheckBox_authW.Checked = authW;
            comboBox_DatabaseW.Text = databaseW;

            #endregion

            #region Elex Tkaniny

            string serverT = Properties.Settings.Default.serverT;
            string userT = Properties.Settings.Default.userT;
            string passwordT = Properties.Settings.Default.passwordT;
            bool authT = Properties.Settings.Default.authT;
            string databaseT = Properties.Settings.Default.databaseT;

            string sferaUserT = Properties.Settings.Default.sferaUserT;
            string sferaPasswordT = Properties.Settings.Default.sferaPasswordT;

            metroTextBox_serverT.Text = serverT;
            metroTextBox_userT.Text = userT;
            metroTextBox_passT.Text = passwordT;
            metroCheckBox_authT.Checked = authT;
            comboBox_DatabaseT.Text = databaseT;

            metroTextBox_sfera_userT.Text = sferaUserT;
            metroTextBox_sfera_passwordT.Text = sferaPasswordT;

            #endregion

            #region Elex Tkaniny Bis

            string serverTB = Properties.Settings.Default.serverTB;
            string userTB = Properties.Settings.Default.userTB;
            string passwordTB = Properties.Settings.Default.passwordTB;
            bool authTB = Properties.Settings.Default.authTB;
            string databaseTB = Properties.Settings.Default.databaseTB;

            string sferaUserTB = Properties.Settings.Default.sferaUserTB;
            string sferaPasswordTB = Properties.Settings.Default.sferaPasswordTB;

            metroTextBox_serverTB.Text = serverTB;
            metroTextBox_userTB.Text = userTB;
            metroTextBox_passTB.Text = passwordTB;
            metroCheckBox_authTB.Checked = authTB;
            comboBox_DatabaseTB.Text = databaseTB;

            metroTextBox_sfera_userTB.Text = sferaUserTB;
            metroTextBox_sfera_passwordTB.Text = sferaPasswordTB;

            #endregion

            string folderEpp = Properties.Settings.Default.katalogPliku;
            metroTextBoxFolderEpp.Text = folderEpp;
        }

        private void comboBox_Database_DropDown(object sender, EventArgs e)
        {
            try
            {

                List<string> databasesKM;

                comboBox_DatabaseKM.Items.Clear();

                databasesKM = GetMSSqlDatabasesNames(metroTextBox_serverKM.Text, metroTextBox_userKM.Text,
                metroTextBox_passKM.Text, metroCheckBox_authKM.Checked);

                foreach (var database in databasesKM)
                {
                    comboBox_DatabaseKM.Items.Add(database);
                }


            }
            catch (Exception)
            {
                MetroMessageBox.Show(this, "Błąd podczas pobierania listy baz. Sprawdź poprawność danych", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }

        }

        private List<string> GetMSSqlDatabasesNames(string dataSource, string userName, string password, bool auth)
        {
            var connection = new SqlConnectionStringBuilder
            {
                DataSource = dataSource,
                UserID = userName,
                Password = password,
                IntegratedSecurity = auth
            };

            var strConn = connection.ToString();

            //create connection
            var sqlConn = new SqlConnection(strConn);

            //open connection
            sqlConn.Open();

            var command = new SqlCommand
            {
                Connection = sqlConn,
                CommandType = CommandType.Text,
                CommandText = "SELECT name FROM master.sys.databases where state = 0 and name not in ( 'master', 'tempdb','model','msdb')"
            };

            var adapter = new SqlDataAdapter(command);
            var dataset = new DataSet();
            adapter.Fill(dataset);
            var tblDatabases = dataset.Tables[0];

            sqlConn.Close();

            //add to list

            return (from DataRow row in tblDatabases.Rows select row[0].ToString()).ToList();
        }

        private void metroButtonFolderBrowser_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.Description = "Wybierz folder zapisu plików EPP.";
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                metroTextBoxFolderEpp.Text = fbd.SelectedPath;
            }
        }

        private void comboBox_DatabaseK_DropDown(object sender, EventArgs e)
        {
            List<string> databasesK;

            comboBox_DatabaseK.Items.Clear();

            databasesK = GetMSSqlDatabasesNames(metroTextBox_serverK.Text, metroTextBox_userK.Text,
            metroTextBox_passK.Text, metroCheckBox_authK.Checked);

            foreach (var database in databasesK)
            {
                comboBox_DatabaseK.Items.Add(database);
            }
        }

        private void comboBox_DatabaseKB_DropDown(object sender, EventArgs e)
        {
            List<string> databasesKB;

            comboBox_DatabaseKB.Items.Clear();

            databasesKB = GetMSSqlDatabasesNames(metroTextBox_serverKB.Text, metroTextBox_userKB.Text,
            metroTextBox_passKB.Text, metroCheckBox_authKB.Checked);

            foreach (var database in databasesKB)
            {
                comboBox_DatabaseKB.Items.Add(database);
            }
        }

        private void comboBox_DatabaseKT_DropDown(object sender, EventArgs e)
        {
            List<string> databasesKT;

            comboBox_DatabaseKT.Items.Clear();

            databasesKT = GetMSSqlDatabasesNames(metroTextBox_serverKT.Text, metroTextBox_userKT.Text,
            metroTextBox_passKT.Text, metroCheckBox_authKT.Checked);

            foreach (var database in databasesKT)
            {
                comboBox_DatabaseKT.Items.Add(database);
            }
        }

        private void comboBox_DatabaseWB_DropDown(object sender, EventArgs e)
        {
            List<string> databasesWB;

            comboBox_DatabaseWB.Items.Clear();

            databasesWB = GetMSSqlDatabasesNames(metroTextBox_serverWB.Text, metroTextBox_userWB.Text,
            metroTextBox_passWB.Text, metroCheckBox_authWB.Checked);

            foreach (var database in databasesWB)
            {
                comboBox_DatabaseWB.Items.Add(database);
            }
        }

        private void comboBox_DatabaseW_DropDown(object sender, EventArgs e)
        {
            List<string> databasesW;

            comboBox_DatabaseW.Items.Clear();

            databasesW = GetMSSqlDatabasesNames(metroTextBox_serverW.Text, metroTextBox_userW.Text,
            metroTextBox_passW.Text, metroCheckBox_authW.Checked);

            foreach (var database in databasesW)
            {
                comboBox_DatabaseW.Items.Add(database);
            }

        }

        private void comboBox_DatabaseT_DropDown(object sender, EventArgs e)
        {

              List<string> databasesT;

            comboBox_DatabaseT.Items.Clear();

            databasesT = GetMSSqlDatabasesNames(metroTextBox_serverT.Text, metroTextBox_userT.Text,
            metroTextBox_passT.Text, metroCheckBox_authT.Checked);

            foreach (var database in databasesT)
            {
                comboBox_DatabaseT.Items.Add(database);
            }

        }

        private void comboBox_DatabaseTB_DropDown(object sender, EventArgs e)
        {
              List<string> databasesTB;

            comboBox_DatabaseTB.Items.Clear();

            databasesTB = GetMSSqlDatabasesNames(metroTextBox_serverTB.Text, metroTextBox_userTB.Text,
            metroTextBox_passTB.Text, metroCheckBox_authTB.Checked);

            foreach (var database in databasesTB)
            {
                comboBox_DatabaseTB.Items.Add(database);
            }

        }
    }

}
