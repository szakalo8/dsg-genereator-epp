﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace EPP
{
    public partial class Form1 : MetroFramework.Forms.MetroForm
    {
        private static Form1 _instance;

        public static Form1 Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Form1();
                return _instance;
            }
        }

        public MetroFramework.Controls.MetroPanel MetroContainer
        {
            get { return metroPanel1; }
            set { metroPanel1 = value; }
        }

        public MetroFramework.Controls.MetroLink MetroBack
        {
            get { return metroLinkBack; }
            set { metroLinkBack = value; }
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void metroLinkBack_Click(object sender, EventArgs e)
        {
            metroPanel1.Controls["Menu"].BringToFront();
            metroLinkBack.Visible = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Text = $"      DSG EPP {Assembly.GetEntryAssembly().GetName().Version}";

            metroLinkBack.Visible = false;
            _instance = this;
            Menu menu = new Menu();
            menu.Dock = DockStyle.Fill;
            metroPanel1.Controls.Add(menu);
        }
    }
}
