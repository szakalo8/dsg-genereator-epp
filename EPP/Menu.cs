﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MetroFramework;

namespace EPP
{
    public partial class Menu : MetroFramework.Controls.MetroUserControl
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void metroTileInfo_Click(object sender, EventArgs e)
        {
            if (!Form1.Instance.MetroContainer.Controls.ContainsKey("Info"))
            {
                Info info = new Info();
                info.Dock = DockStyle.Fill;
                Form1.Instance.MetroContainer.Controls.Add(info);
            }
            Form1.Instance.MetroContainer.Controls["Info"].BringToFront();
            Form1.Instance.MetroBack.Visible = true;
        }

        private void metroTileConfig_Click(object sender, EventArgs e)
        {
            if (!Form1.Instance.MetroContainer.Controls.ContainsKey("Konfiguracja"))
            {
                Konfiguracja konfiguracja = new Konfiguracja();
                konfiguracja.Dock = DockStyle.Fill;
                Form1.Instance.MetroContainer.Controls.Add(konfiguracja);
            }
            Form1.Instance.MetroContainer.Controls["Konfiguracja"].BringToFront();
            Form1.Instance.MetroBack.Visible = true;
        }

        private void metroTileExit_Click(object sender, EventArgs e)
        {
            if (MetroMessageBox.Show(this, "Czy chcesz zamknąć aplikację?", "Zamknąć?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                Form1.Instance.Close();
            }
        }

        private void metroTileGenerujEPP_Click(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.UpdateSettings)
            {
                Properties.Settings.Default.Upgrade();
                Properties.Settings.Default.UpdateSettings = false;
                Properties.Settings.Default.Save();
            }

            if (!Form1.Instance.MetroContainer.Controls.ContainsKey("GenerujEPP"))
            {
                GenerujEPP generujEpp = new GenerujEPP();
                generujEpp.Dock = DockStyle.Fill;
                Form1.Instance.MetroContainer.Controls.Add(generujEpp);
            }
            Form1.Instance.MetroContainer.Controls["GenerujEPP"].BringToFront();
            Form1.Instance.MetroBack.Visible = true;
        }

        private void metroTile_fv_Click(object sender, EventArgs e)
        {
            if (!Form1.Instance.MetroContainer.Controls.ContainsKey("InvoiceView"))
            {
                InvoiceView invoiceView = new InvoiceView();
                invoiceView.Dock = DockStyle.Fill;
                Form1.Instance.MetroContainer.Controls.Add(invoiceView);
            }

            var invoiceViewControl = Form1.Instance.MetroContainer.Controls["InvoiceView"];
            if (invoiceViewControl != null)
            {
                invoiceViewControl.BringToFront();
                Form1.Instance.MetroBack.Visible = true;
            }
            else
            {
                Console.WriteLine("InvoiceView control not found in MetroContainer.Controls.");
            }

        }
    }
}
