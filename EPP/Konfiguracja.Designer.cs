﻿namespace EPP
{
    partial class Konfiguracja
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroLabel19 = new MetroFramework.Controls.MetroLabel();
            this.comboBox_DatabaseKM = new System.Windows.Forms.ComboBox();
            this.metroLabel25 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox_sfera_passwordKM = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel17 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel20 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox_sfera_userKM = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox_serverKM = new MetroFramework.Controls.MetroTextBox();
            this.metroCheckBox_authKM = new MetroFramework.Controls.MetroCheckBox();
            this.metroTextBox_userKM = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox_passKM = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroTileZapisz = new MetroFramework.Controls.MetroTile();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxFolderEpp = new MetroFramework.Controls.MetroTextBox();
            this.metroButtonFolderBrowser = new MetroFramework.Controls.MetroButton();
            this.metroTabControl_databaseW = new MetroFramework.Controls.MetroTabControl();
            this.metroTabPage1 = new MetroFramework.Controls.MetroTabPage();
            this.metroTabPage2 = new MetroFramework.Controls.MetroTabPage();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox_passK = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox_userK = new MetroFramework.Controls.MetroTextBox();
            this.comboBox_DatabaseK = new System.Windows.Forms.ComboBox();
            this.metroCheckBox_authK = new MetroFramework.Controls.MetroCheckBox();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox_serverK = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel13 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel14 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox_sfera_userK = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBox_sfera_passwordK = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel15 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel16 = new MetroFramework.Controls.MetroLabel();
            this.metroTabPage3 = new MetroFramework.Controls.MetroTabPage();
            this.metroLabel18 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel21 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox_passKB = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel22 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel23 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox_userKB = new MetroFramework.Controls.MetroTextBox();
            this.comboBox_DatabaseKB = new System.Windows.Forms.ComboBox();
            this.metroCheckBox_authKB = new MetroFramework.Controls.MetroCheckBox();
            this.metroLabel24 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox_serverKB = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel26 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel27 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox_sfera_userKB = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBox_sfera_passwordKB = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel28 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel29 = new MetroFramework.Controls.MetroLabel();
            this.metroTabPage4 = new MetroFramework.Controls.MetroTabPage();
            this.metroLabel30 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel31 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox_passKT = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel32 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel33 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox_userKT = new MetroFramework.Controls.MetroTextBox();
            this.comboBox_DatabaseKT = new System.Windows.Forms.ComboBox();
            this.metroCheckBox_authKT = new MetroFramework.Controls.MetroCheckBox();
            this.metroLabel34 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox_serverKT = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel35 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel36 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox_sfera_userKT = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBox_sfera_passwordKT = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel37 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel38 = new MetroFramework.Controls.MetroLabel();
            this.metroTabPage5 = new MetroFramework.Controls.MetroTabPage();
            this.metroLabel39 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel40 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox_passWB = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel41 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel42 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox_userWB = new MetroFramework.Controls.MetroTextBox();
            this.comboBox_DatabaseWB = new System.Windows.Forms.ComboBox();
            this.metroCheckBox_authWB = new MetroFramework.Controls.MetroCheckBox();
            this.metroLabel43 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox_serverWB = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel44 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel45 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox_sfera_userWB = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBox_sfera_passwordWB = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel46 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel47 = new MetroFramework.Controls.MetroLabel();
            this.metroTabPage6 = new MetroFramework.Controls.MetroTabPage();
            this.metroLabel48 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel49 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox_passW = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel50 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox_userW = new MetroFramework.Controls.MetroTextBox();
            this.comboBox_DatabaseW = new System.Windows.Forms.ComboBox();
            this.metroCheckBox_authW = new MetroFramework.Controls.MetroCheckBox();
            this.metroLabel52 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox_serverW = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel54 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel56 = new MetroFramework.Controls.MetroLabel();
            this.metroTabPage7 = new MetroFramework.Controls.MetroTabPage();
            this.metroLabel51 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel53 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox_passT = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel55 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel57 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox_userT = new MetroFramework.Controls.MetroTextBox();
            this.comboBox_DatabaseT = new System.Windows.Forms.ComboBox();
            this.metroCheckBox_authT = new MetroFramework.Controls.MetroCheckBox();
            this.metroLabel58 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox_serverT = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel59 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel60 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox_sfera_userT = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBox_sfera_passwordT = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel61 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel62 = new MetroFramework.Controls.MetroLabel();
            this.metroTabPage8 = new MetroFramework.Controls.MetroTabPage();
            this.metroLabel63 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel64 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox_passTB = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel65 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel66 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox_userTB = new MetroFramework.Controls.MetroTextBox();
            this.comboBox_DatabaseTB = new System.Windows.Forms.ComboBox();
            this.metroCheckBox_authTB = new MetroFramework.Controls.MetroCheckBox();
            this.metroLabel67 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox_serverTB = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel68 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel69 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox_sfera_userTB = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBox_sfera_passwordTB = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel70 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel71 = new MetroFramework.Controls.MetroLabel();
            this.metroTabControl_databaseW.SuspendLayout();
            this.metroTabPage1.SuspendLayout();
            this.metroTabPage2.SuspendLayout();
            this.metroTabPage3.SuspendLayout();
            this.metroTabPage4.SuspendLayout();
            this.metroTabPage5.SuspendLayout();
            this.metroTabPage6.SuspendLayout();
            this.metroTabPage7.SuspendLayout();
            this.metroTabPage8.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroLabel19
            // 
            this.metroLabel19.AutoSize = true;
            this.metroLabel19.Location = new System.Drawing.Point(103, 228);
            this.metroLabel19.Name = "metroLabel19";
            this.metroLabel19.Size = new System.Drawing.Size(44, 19);
            this.metroLabel19.TabIndex = 76;
            this.metroLabel19.Text = "Hasło:";
            // 
            // comboBox_DatabaseKM
            // 
            this.comboBox_DatabaseKM.FormattingEnabled = true;
            this.comboBox_DatabaseKM.Location = new System.Drawing.Point(163, 137);
            this.comboBox_DatabaseKM.Name = "comboBox_DatabaseKM";
            this.comboBox_DatabaseKM.Size = new System.Drawing.Size(224, 21);
            this.comboBox_DatabaseKM.TabIndex = 71;
            this.comboBox_DatabaseKM.DropDown += new System.EventHandler(this.comboBox_Database_DropDown);
            // 
            // metroLabel25
            // 
            this.metroLabel25.AutoSize = true;
            this.metroLabel25.Location = new System.Drawing.Point(107, 139);
            this.metroLabel25.Name = "metroLabel25";
            this.metroLabel25.Size = new System.Drawing.Size(40, 19);
            this.metroLabel25.TabIndex = 73;
            this.metroLabel25.Text = "Baza:";
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel9.Location = new System.Drawing.Point(163, 176);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(50, 19);
            this.metroLabel9.TabIndex = 65;
            this.metroLabel9.Text = "SFERA";
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(96, 17);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(51, 19);
            this.metroLabel8.TabIndex = 66;
            this.metroLabel8.Text = "Server:";
            // 
            // metroTextBox_sfera_passwordKM
            // 
            // 
            // 
            // 
            this.metroTextBox_sfera_passwordKM.CustomButton.Image = null;
            this.metroTextBox_sfera_passwordKM.CustomButton.Location = new System.Drawing.Point(290, 1);
            this.metroTextBox_sfera_passwordKM.CustomButton.Name = "";
            this.metroTextBox_sfera_passwordKM.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox_sfera_passwordKM.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_sfera_passwordKM.CustomButton.TabIndex = 1;
            this.metroTextBox_sfera_passwordKM.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_sfera_passwordKM.CustomButton.UseSelectable = true;
            this.metroTextBox_sfera_passwordKM.CustomButton.Visible = false;
            this.metroTextBox_sfera_passwordKM.Lines = new string[0];
            this.metroTextBox_sfera_passwordKM.Location = new System.Drawing.Point(163, 228);
            this.metroTextBox_sfera_passwordKM.MaxLength = 32767;
            this.metroTextBox_sfera_passwordKM.Name = "metroTextBox_sfera_passwordKM";
            this.metroTextBox_sfera_passwordKM.PasswordChar = '*';
            this.metroTextBox_sfera_passwordKM.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_sfera_passwordKM.SelectedText = "";
            this.metroTextBox_sfera_passwordKM.SelectionLength = 0;
            this.metroTextBox_sfera_passwordKM.SelectionStart = 0;
            this.metroTextBox_sfera_passwordKM.ShortcutsEnabled = true;
            this.metroTextBox_sfera_passwordKM.Size = new System.Drawing.Size(312, 23);
            this.metroTextBox_sfera_passwordKM.TabIndex = 74;
            this.metroTextBox_sfera_passwordKM.UseSelectable = true;
            this.metroTextBox_sfera_passwordKM.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_sfera_passwordKM.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel17
            // 
            this.metroLabel17.AutoSize = true;
            this.metroLabel17.Location = new System.Drawing.Point(333, 17);
            this.metroLabel17.Name = "metroLabel17";
            this.metroLabel17.Size = new System.Drawing.Size(101, 19);
            this.metroLabel17.Style = MetroFramework.MetroColorStyle.Silver;
            this.metroLabel17.TabIndex = 70;
            this.metroLabel17.Text = "xx.xx.xx.xx,pppp";
            this.metroLabel17.UseStyleColors = true;
            // 
            // metroLabel20
            // 
            this.metroLabel20.AutoSize = true;
            this.metroLabel20.Location = new System.Drawing.Point(71, 198);
            this.metroLabel20.Name = "metroLabel20";
            this.metroLabel20.Size = new System.Drawing.Size(76, 19);
            this.metroLabel20.TabIndex = 75;
            this.metroLabel20.Text = "Użytkownik:";
            // 
            // metroTextBox_sfera_userKM
            // 
            // 
            // 
            // 
            this.metroTextBox_sfera_userKM.CustomButton.Image = null;
            this.metroTextBox_sfera_userKM.CustomButton.Location = new System.Drawing.Point(290, 1);
            this.metroTextBox_sfera_userKM.CustomButton.Name = "";
            this.metroTextBox_sfera_userKM.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox_sfera_userKM.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_sfera_userKM.CustomButton.TabIndex = 1;
            this.metroTextBox_sfera_userKM.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_sfera_userKM.CustomButton.UseSelectable = true;
            this.metroTextBox_sfera_userKM.CustomButton.Visible = false;
            this.metroTextBox_sfera_userKM.Lines = new string[0];
            this.metroTextBox_sfera_userKM.Location = new System.Drawing.Point(163, 198);
            this.metroTextBox_sfera_userKM.MaxLength = 32767;
            this.metroTextBox_sfera_userKM.Name = "metroTextBox_sfera_userKM";
            this.metroTextBox_sfera_userKM.PasswordChar = '\0';
            this.metroTextBox_sfera_userKM.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_sfera_userKM.SelectedText = "";
            this.metroTextBox_sfera_userKM.SelectionLength = 0;
            this.metroTextBox_sfera_userKM.SelectionStart = 0;
            this.metroTextBox_sfera_userKM.ShortcutsEnabled = true;
            this.metroTextBox_sfera_userKM.Size = new System.Drawing.Size(312, 23);
            this.metroTextBox_sfera_userKM.TabIndex = 72;
            this.metroTextBox_sfera_userKM.UseSelectable = true;
            this.metroTextBox_sfera_userKM.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_sfera_userKM.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(5, 102);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(142, 19);
            this.metroLabel5.TabIndex = 69;
            this.metroLabel5.Text = "Autentykacja Windows:";
            // 
            // metroTextBox_serverKM
            // 
            // 
            // 
            // 
            this.metroTextBox_serverKM.CustomButton.Image = null;
            this.metroTextBox_serverKM.CustomButton.Location = new System.Drawing.Point(142, 1);
            this.metroTextBox_serverKM.CustomButton.Name = "";
            this.metroTextBox_serverKM.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox_serverKM.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_serverKM.CustomButton.TabIndex = 1;
            this.metroTextBox_serverKM.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_serverKM.CustomButton.UseSelectable = true;
            this.metroTextBox_serverKM.CustomButton.Visible = false;
            this.metroTextBox_serverKM.Lines = new string[0];
            this.metroTextBox_serverKM.Location = new System.Drawing.Point(163, 13);
            this.metroTextBox_serverKM.MaxLength = 32767;
            this.metroTextBox_serverKM.Name = "metroTextBox_serverKM";
            this.metroTextBox_serverKM.PasswordChar = '\0';
            this.metroTextBox_serverKM.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_serverKM.SelectedText = "";
            this.metroTextBox_serverKM.SelectionLength = 0;
            this.metroTextBox_serverKM.SelectionStart = 0;
            this.metroTextBox_serverKM.ShortcutsEnabled = true;
            this.metroTextBox_serverKM.Size = new System.Drawing.Size(164, 23);
            this.metroTextBox_serverKM.TabIndex = 61;
            this.metroTextBox_serverKM.UseSelectable = true;
            this.metroTextBox_serverKM.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_serverKM.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroCheckBox_authKM
            // 
            this.metroCheckBox_authKM.AutoSize = true;
            this.metroCheckBox_authKM.Location = new System.Drawing.Point(163, 106);
            this.metroCheckBox_authKM.Name = "metroCheckBox_authKM";
            this.metroCheckBox_authKM.Size = new System.Drawing.Size(26, 15);
            this.metroCheckBox_authKM.TabIndex = 64;
            this.metroCheckBox_authKM.Text = " ";
            this.metroCheckBox_authKM.UseSelectable = true;
            // 
            // metroTextBox_userKM
            // 
            // 
            // 
            // 
            this.metroTextBox_userKM.CustomButton.Image = null;
            this.metroTextBox_userKM.CustomButton.Location = new System.Drawing.Point(142, 1);
            this.metroTextBox_userKM.CustomButton.Name = "";
            this.metroTextBox_userKM.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox_userKM.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_userKM.CustomButton.TabIndex = 1;
            this.metroTextBox_userKM.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_userKM.CustomButton.UseSelectable = true;
            this.metroTextBox_userKM.CustomButton.Visible = false;
            this.metroTextBox_userKM.Lines = new string[0];
            this.metroTextBox_userKM.Location = new System.Drawing.Point(163, 42);
            this.metroTextBox_userKM.MaxLength = 32767;
            this.metroTextBox_userKM.Name = "metroTextBox_userKM";
            this.metroTextBox_userKM.PasswordChar = '\0';
            this.metroTextBox_userKM.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_userKM.SelectedText = "";
            this.metroTextBox_userKM.SelectionLength = 0;
            this.metroTextBox_userKM.SelectionStart = 0;
            this.metroTextBox_userKM.ShortcutsEnabled = true;
            this.metroTextBox_userKM.Size = new System.Drawing.Size(164, 23);
            this.metroTextBox_userKM.TabIndex = 62;
            this.metroTextBox_userKM.UseSelectable = true;
            this.metroTextBox_userKM.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_userKM.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(71, 42);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(76, 19);
            this.metroLabel2.TabIndex = 67;
            this.metroLabel2.Text = "Użytkownik:";
            // 
            // metroTextBox_passKM
            // 
            // 
            // 
            // 
            this.metroTextBox_passKM.CustomButton.Image = null;
            this.metroTextBox_passKM.CustomButton.Location = new System.Drawing.Point(142, 1);
            this.metroTextBox_passKM.CustomButton.Name = "";
            this.metroTextBox_passKM.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox_passKM.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_passKM.CustomButton.TabIndex = 1;
            this.metroTextBox_passKM.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_passKM.CustomButton.UseSelectable = true;
            this.metroTextBox_passKM.CustomButton.Visible = false;
            this.metroTextBox_passKM.Lines = new string[0];
            this.metroTextBox_passKM.Location = new System.Drawing.Point(163, 72);
            this.metroTextBox_passKM.MaxLength = 32767;
            this.metroTextBox_passKM.Name = "metroTextBox_passKM";
            this.metroTextBox_passKM.PasswordChar = '*';
            this.metroTextBox_passKM.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_passKM.SelectedText = "";
            this.metroTextBox_passKM.SelectionLength = 0;
            this.metroTextBox_passKM.SelectionStart = 0;
            this.metroTextBox_passKM.ShortcutsEnabled = true;
            this.metroTextBox_passKM.Size = new System.Drawing.Size(164, 23);
            this.metroTextBox_passKM.TabIndex = 63;
            this.metroTextBox_passKM.UseSelectable = true;
            this.metroTextBox_passKM.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_passKM.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(103, 72);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(44, 19);
            this.metroLabel3.TabIndex = 68;
            this.metroLabel3.Text = "Hasło:";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel6.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel6.Location = new System.Drawing.Point(0, 0);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(121, 25);
            this.metroLabel6.TabIndex = 77;
            this.metroLabel6.Text = "Konfiguracja";
            // 
            // metroTileZapisz
            // 
            this.metroTileZapisz.ActiveControl = null;
            this.metroTileZapisz.Location = new System.Drawing.Point(823, 537);
            this.metroTileZapisz.Name = "metroTileZapisz";
            this.metroTileZapisz.Size = new System.Drawing.Size(128, 44);
            this.metroTileZapisz.Style = MetroFramework.MetroColorStyle.Green;
            this.metroTileZapisz.TabIndex = 78;
            this.metroTileZapisz.Text = "Zapisz";
            this.metroTileZapisz.UseSelectable = true;
            this.metroTileZapisz.Click += new System.EventHandler(this.metroTileZapisz_Click);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(33, 474);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(121, 19);
            this.metroLabel1.TabIndex = 80;
            this.metroLabel1.Text = "Katalog plików EPP";
            // 
            // metroTextBoxFolderEpp
            // 
            // 
            // 
            // 
            this.metroTextBoxFolderEpp.CustomButton.Image = null;
            this.metroTextBoxFolderEpp.CustomButton.Location = new System.Drawing.Point(290, 1);
            this.metroTextBoxFolderEpp.CustomButton.Name = "";
            this.metroTextBoxFolderEpp.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBoxFolderEpp.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxFolderEpp.CustomButton.TabIndex = 1;
            this.metroTextBoxFolderEpp.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxFolderEpp.CustomButton.UseSelectable = true;
            this.metroTextBoxFolderEpp.CustomButton.Visible = false;
            this.metroTextBoxFolderEpp.Lines = new string[0];
            this.metroTextBoxFolderEpp.Location = new System.Drawing.Point(170, 470);
            this.metroTextBoxFolderEpp.MaxLength = 32767;
            this.metroTextBoxFolderEpp.Name = "metroTextBoxFolderEpp";
            this.metroTextBoxFolderEpp.PasswordChar = '\0';
            this.metroTextBoxFolderEpp.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxFolderEpp.SelectedText = "";
            this.metroTextBoxFolderEpp.SelectionLength = 0;
            this.metroTextBoxFolderEpp.SelectionStart = 0;
            this.metroTextBoxFolderEpp.ShortcutsEnabled = true;
            this.metroTextBoxFolderEpp.Size = new System.Drawing.Size(312, 23);
            this.metroTextBoxFolderEpp.TabIndex = 79;
            this.metroTextBoxFolderEpp.UseSelectable = true;
            this.metroTextBoxFolderEpp.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxFolderEpp.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroButtonFolderBrowser
            // 
            this.metroButtonFolderBrowser.Location = new System.Drawing.Point(489, 470);
            this.metroButtonFolderBrowser.Name = "metroButtonFolderBrowser";
            this.metroButtonFolderBrowser.Size = new System.Drawing.Size(32, 23);
            this.metroButtonFolderBrowser.TabIndex = 81;
            this.metroButtonFolderBrowser.Text = "...";
            this.metroButtonFolderBrowser.UseSelectable = true;
            this.metroButtonFolderBrowser.Click += new System.EventHandler(this.metroButtonFolderBrowser_Click);
            // 
            // metroTabControl_databaseW
            // 
            this.metroTabControl_databaseW.Controls.Add(this.metroTabPage1);
            this.metroTabControl_databaseW.Controls.Add(this.metroTabPage2);
            this.metroTabControl_databaseW.Controls.Add(this.metroTabPage3);
            this.metroTabControl_databaseW.Controls.Add(this.metroTabPage4);
            this.metroTabControl_databaseW.Controls.Add(this.metroTabPage5);
            this.metroTabControl_databaseW.Controls.Add(this.metroTabPage6);
            this.metroTabControl_databaseW.Controls.Add(this.metroTabPage7);
            this.metroTabControl_databaseW.Controls.Add(this.metroTabPage8);
            this.metroTabControl_databaseW.Location = new System.Drawing.Point(3, 28);
            this.metroTabControl_databaseW.Name = "metroTabControl_databaseW";
            this.metroTabControl_databaseW.SelectedIndex = 7;
            this.metroTabControl_databaseW.Size = new System.Drawing.Size(948, 379);
            this.metroTabControl_databaseW.TabIndex = 82;
            this.metroTabControl_databaseW.UseSelectable = true;
            // 
            // metroTabPage1
            // 
            this.metroTabPage1.Controls.Add(this.metroLabel8);
            this.metroTabPage1.Controls.Add(this.metroLabel3);
            this.metroTabPage1.Controls.Add(this.metroTextBox_passKM);
            this.metroTabPage1.Controls.Add(this.metroLabel2);
            this.metroTabPage1.Controls.Add(this.metroLabel19);
            this.metroTabPage1.Controls.Add(this.metroTextBox_userKM);
            this.metroTabPage1.Controls.Add(this.comboBox_DatabaseKM);
            this.metroTabPage1.Controls.Add(this.metroCheckBox_authKM);
            this.metroTabPage1.Controls.Add(this.metroLabel25);
            this.metroTabPage1.Controls.Add(this.metroTextBox_serverKM);
            this.metroTabPage1.Controls.Add(this.metroLabel9);
            this.metroTabPage1.Controls.Add(this.metroLabel5);
            this.metroTabPage1.Controls.Add(this.metroTextBox_sfera_userKM);
            this.metroTabPage1.Controls.Add(this.metroTextBox_sfera_passwordKM);
            this.metroTabPage1.Controls.Add(this.metroLabel20);
            this.metroTabPage1.Controls.Add(this.metroLabel17);
            this.metroTabPage1.HorizontalScrollbarBarColor = true;
            this.metroTabPage1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.HorizontalScrollbarSize = 10;
            this.metroTabPage1.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage1.Name = "metroTabPage1";
            this.metroTabPage1.Size = new System.Drawing.Size(940, 337);
            this.metroTabPage1.TabIndex = 0;
            this.metroTabPage1.Text = "Konekt Magazyn";
            this.metroTabPage1.VerticalScrollbarBarColor = true;
            this.metroTabPage1.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.VerticalScrollbarSize = 10;
            // 
            // metroTabPage2
            // 
            this.metroTabPage2.Controls.Add(this.metroLabel4);
            this.metroTabPage2.Controls.Add(this.metroLabel7);
            this.metroTabPage2.Controls.Add(this.metroTextBox_passK);
            this.metroTabPage2.Controls.Add(this.metroLabel10);
            this.metroTabPage2.Controls.Add(this.metroLabel11);
            this.metroTabPage2.Controls.Add(this.metroTextBox_userK);
            this.metroTabPage2.Controls.Add(this.comboBox_DatabaseK);
            this.metroTabPage2.Controls.Add(this.metroCheckBox_authK);
            this.metroTabPage2.Controls.Add(this.metroLabel12);
            this.metroTabPage2.Controls.Add(this.metroTextBox_serverK);
            this.metroTabPage2.Controls.Add(this.metroLabel13);
            this.metroTabPage2.Controls.Add(this.metroLabel14);
            this.metroTabPage2.Controls.Add(this.metroTextBox_sfera_userK);
            this.metroTabPage2.Controls.Add(this.metroTextBox_sfera_passwordK);
            this.metroTabPage2.Controls.Add(this.metroLabel15);
            this.metroTabPage2.Controls.Add(this.metroLabel16);
            this.metroTabPage2.HorizontalScrollbarBarColor = true;
            this.metroTabPage2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage2.HorizontalScrollbarSize = 10;
            this.metroTabPage2.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage2.Name = "metroTabPage2";
            this.metroTabPage2.Size = new System.Drawing.Size(940, 337);
            this.metroTabPage2.TabIndex = 1;
            this.metroTabPage2.Text = "Konekt";
            this.metroTabPage2.VerticalScrollbarBarColor = true;
            this.metroTabPage2.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage2.VerticalScrollbarSize = 10;
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(96, 17);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(51, 19);
            this.metroLabel4.TabIndex = 82;
            this.metroLabel4.Text = "Server:";
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(103, 72);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(44, 19);
            this.metroLabel7.TabIndex = 84;
            this.metroLabel7.Text = "Hasło:";
            // 
            // metroTextBox_passK
            // 
            // 
            // 
            // 
            this.metroTextBox_passK.CustomButton.Image = null;
            this.metroTextBox_passK.CustomButton.Location = new System.Drawing.Point(142, 1);
            this.metroTextBox_passK.CustomButton.Name = "";
            this.metroTextBox_passK.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox_passK.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_passK.CustomButton.TabIndex = 1;
            this.metroTextBox_passK.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_passK.CustomButton.UseSelectable = true;
            this.metroTextBox_passK.CustomButton.Visible = false;
            this.metroTextBox_passK.Lines = new string[0];
            this.metroTextBox_passK.Location = new System.Drawing.Point(163, 72);
            this.metroTextBox_passK.MaxLength = 32767;
            this.metroTextBox_passK.Name = "metroTextBox_passK";
            this.metroTextBox_passK.PasswordChar = '*';
            this.metroTextBox_passK.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_passK.SelectedText = "";
            this.metroTextBox_passK.SelectionLength = 0;
            this.metroTextBox_passK.SelectionStart = 0;
            this.metroTextBox_passK.ShortcutsEnabled = true;
            this.metroTextBox_passK.Size = new System.Drawing.Size(164, 23);
            this.metroTextBox_passK.TabIndex = 79;
            this.metroTextBox_passK.UseSelectable = true;
            this.metroTextBox_passK.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_passK.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(71, 42);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(76, 19);
            this.metroLabel10.TabIndex = 83;
            this.metroLabel10.Text = "Użytkownik:";
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.Location = new System.Drawing.Point(103, 228);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(44, 19);
            this.metroLabel11.TabIndex = 92;
            this.metroLabel11.Text = "Hasło:";
            // 
            // metroTextBox_userK
            // 
            // 
            // 
            // 
            this.metroTextBox_userK.CustomButton.Image = null;
            this.metroTextBox_userK.CustomButton.Location = new System.Drawing.Point(142, 1);
            this.metroTextBox_userK.CustomButton.Name = "";
            this.metroTextBox_userK.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox_userK.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_userK.CustomButton.TabIndex = 1;
            this.metroTextBox_userK.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_userK.CustomButton.UseSelectable = true;
            this.metroTextBox_userK.CustomButton.Visible = false;
            this.metroTextBox_userK.Lines = new string[0];
            this.metroTextBox_userK.Location = new System.Drawing.Point(163, 42);
            this.metroTextBox_userK.MaxLength = 32767;
            this.metroTextBox_userK.Name = "metroTextBox_userK";
            this.metroTextBox_userK.PasswordChar = '\0';
            this.metroTextBox_userK.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_userK.SelectedText = "";
            this.metroTextBox_userK.SelectionLength = 0;
            this.metroTextBox_userK.SelectionStart = 0;
            this.metroTextBox_userK.ShortcutsEnabled = true;
            this.metroTextBox_userK.Size = new System.Drawing.Size(164, 23);
            this.metroTextBox_userK.TabIndex = 78;
            this.metroTextBox_userK.UseSelectable = true;
            this.metroTextBox_userK.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_userK.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // comboBox_DatabaseK
            // 
            this.comboBox_DatabaseK.FormattingEnabled = true;
            this.comboBox_DatabaseK.Location = new System.Drawing.Point(163, 137);
            this.comboBox_DatabaseK.Name = "comboBox_DatabaseK";
            this.comboBox_DatabaseK.Size = new System.Drawing.Size(224, 21);
            this.comboBox_DatabaseK.TabIndex = 87;
            this.comboBox_DatabaseK.DropDown += new System.EventHandler(this.comboBox_DatabaseK_DropDown);
            // 
            // metroCheckBox_authK
            // 
            this.metroCheckBox_authK.AutoSize = true;
            this.metroCheckBox_authK.Location = new System.Drawing.Point(163, 106);
            this.metroCheckBox_authK.Name = "metroCheckBox_authK";
            this.metroCheckBox_authK.Size = new System.Drawing.Size(26, 15);
            this.metroCheckBox_authK.TabIndex = 80;
            this.metroCheckBox_authK.Text = " ";
            this.metroCheckBox_authK.UseSelectable = true;
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.Location = new System.Drawing.Point(107, 139);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(40, 19);
            this.metroLabel12.TabIndex = 89;
            this.metroLabel12.Text = "Baza:";
            // 
            // metroTextBox_serverK
            // 
            // 
            // 
            // 
            this.metroTextBox_serverK.CustomButton.Image = null;
            this.metroTextBox_serverK.CustomButton.Location = new System.Drawing.Point(142, 1);
            this.metroTextBox_serverK.CustomButton.Name = "";
            this.metroTextBox_serverK.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox_serverK.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_serverK.CustomButton.TabIndex = 1;
            this.metroTextBox_serverK.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_serverK.CustomButton.UseSelectable = true;
            this.metroTextBox_serverK.CustomButton.Visible = false;
            this.metroTextBox_serverK.Lines = new string[0];
            this.metroTextBox_serverK.Location = new System.Drawing.Point(163, 13);
            this.metroTextBox_serverK.MaxLength = 32767;
            this.metroTextBox_serverK.Name = "metroTextBox_serverK";
            this.metroTextBox_serverK.PasswordChar = '\0';
            this.metroTextBox_serverK.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_serverK.SelectedText = "";
            this.metroTextBox_serverK.SelectionLength = 0;
            this.metroTextBox_serverK.SelectionStart = 0;
            this.metroTextBox_serverK.ShortcutsEnabled = true;
            this.metroTextBox_serverK.Size = new System.Drawing.Size(164, 23);
            this.metroTextBox_serverK.TabIndex = 77;
            this.metroTextBox_serverK.UseSelectable = true;
            this.metroTextBox_serverK.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_serverK.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel13
            // 
            this.metroLabel13.AutoSize = true;
            this.metroLabel13.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel13.Location = new System.Drawing.Point(163, 176);
            this.metroLabel13.Name = "metroLabel13";
            this.metroLabel13.Size = new System.Drawing.Size(50, 19);
            this.metroLabel13.TabIndex = 81;
            this.metroLabel13.Text = "SFERA";
            // 
            // metroLabel14
            // 
            this.metroLabel14.AutoSize = true;
            this.metroLabel14.Location = new System.Drawing.Point(5, 102);
            this.metroLabel14.Name = "metroLabel14";
            this.metroLabel14.Size = new System.Drawing.Size(142, 19);
            this.metroLabel14.TabIndex = 85;
            this.metroLabel14.Text = "Autentykacja Windows:";
            // 
            // metroTextBox_sfera_userK
            // 
            // 
            // 
            // 
            this.metroTextBox_sfera_userK.CustomButton.Image = null;
            this.metroTextBox_sfera_userK.CustomButton.Location = new System.Drawing.Point(290, 1);
            this.metroTextBox_sfera_userK.CustomButton.Name = "";
            this.metroTextBox_sfera_userK.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox_sfera_userK.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_sfera_userK.CustomButton.TabIndex = 1;
            this.metroTextBox_sfera_userK.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_sfera_userK.CustomButton.UseSelectable = true;
            this.metroTextBox_sfera_userK.CustomButton.Visible = false;
            this.metroTextBox_sfera_userK.Lines = new string[0];
            this.metroTextBox_sfera_userK.Location = new System.Drawing.Point(163, 198);
            this.metroTextBox_sfera_userK.MaxLength = 32767;
            this.metroTextBox_sfera_userK.Name = "metroTextBox_sfera_userK";
            this.metroTextBox_sfera_userK.PasswordChar = '\0';
            this.metroTextBox_sfera_userK.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_sfera_userK.SelectedText = "";
            this.metroTextBox_sfera_userK.SelectionLength = 0;
            this.metroTextBox_sfera_userK.SelectionStart = 0;
            this.metroTextBox_sfera_userK.ShortcutsEnabled = true;
            this.metroTextBox_sfera_userK.Size = new System.Drawing.Size(312, 23);
            this.metroTextBox_sfera_userK.TabIndex = 88;
            this.metroTextBox_sfera_userK.UseSelectable = true;
            this.metroTextBox_sfera_userK.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_sfera_userK.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBox_sfera_passwordK
            // 
            // 
            // 
            // 
            this.metroTextBox_sfera_passwordK.CustomButton.Image = null;
            this.metroTextBox_sfera_passwordK.CustomButton.Location = new System.Drawing.Point(290, 1);
            this.metroTextBox_sfera_passwordK.CustomButton.Name = "";
            this.metroTextBox_sfera_passwordK.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox_sfera_passwordK.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_sfera_passwordK.CustomButton.TabIndex = 1;
            this.metroTextBox_sfera_passwordK.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_sfera_passwordK.CustomButton.UseSelectable = true;
            this.metroTextBox_sfera_passwordK.CustomButton.Visible = false;
            this.metroTextBox_sfera_passwordK.Lines = new string[0];
            this.metroTextBox_sfera_passwordK.Location = new System.Drawing.Point(163, 228);
            this.metroTextBox_sfera_passwordK.MaxLength = 32767;
            this.metroTextBox_sfera_passwordK.Name = "metroTextBox_sfera_passwordK";
            this.metroTextBox_sfera_passwordK.PasswordChar = '*';
            this.metroTextBox_sfera_passwordK.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_sfera_passwordK.SelectedText = "";
            this.metroTextBox_sfera_passwordK.SelectionLength = 0;
            this.metroTextBox_sfera_passwordK.SelectionStart = 0;
            this.metroTextBox_sfera_passwordK.ShortcutsEnabled = true;
            this.metroTextBox_sfera_passwordK.Size = new System.Drawing.Size(312, 23);
            this.metroTextBox_sfera_passwordK.TabIndex = 90;
            this.metroTextBox_sfera_passwordK.UseSelectable = true;
            this.metroTextBox_sfera_passwordK.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_sfera_passwordK.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel15
            // 
            this.metroLabel15.AutoSize = true;
            this.metroLabel15.Location = new System.Drawing.Point(71, 198);
            this.metroLabel15.Name = "metroLabel15";
            this.metroLabel15.Size = new System.Drawing.Size(76, 19);
            this.metroLabel15.TabIndex = 91;
            this.metroLabel15.Text = "Użytkownik:";
            // 
            // metroLabel16
            // 
            this.metroLabel16.AutoSize = true;
            this.metroLabel16.Location = new System.Drawing.Point(333, 17);
            this.metroLabel16.Name = "metroLabel16";
            this.metroLabel16.Size = new System.Drawing.Size(101, 19);
            this.metroLabel16.Style = MetroFramework.MetroColorStyle.Silver;
            this.metroLabel16.TabIndex = 86;
            this.metroLabel16.Text = "xx.xx.xx.xx,pppp";
            this.metroLabel16.UseStyleColors = true;
            // 
            // metroTabPage3
            // 
            this.metroTabPage3.Controls.Add(this.metroLabel18);
            this.metroTabPage3.Controls.Add(this.metroLabel21);
            this.metroTabPage3.Controls.Add(this.metroTextBox_passKB);
            this.metroTabPage3.Controls.Add(this.metroLabel22);
            this.metroTabPage3.Controls.Add(this.metroLabel23);
            this.metroTabPage3.Controls.Add(this.metroTextBox_userKB);
            this.metroTabPage3.Controls.Add(this.comboBox_DatabaseKB);
            this.metroTabPage3.Controls.Add(this.metroCheckBox_authKB);
            this.metroTabPage3.Controls.Add(this.metroLabel24);
            this.metroTabPage3.Controls.Add(this.metroTextBox_serverKB);
            this.metroTabPage3.Controls.Add(this.metroLabel26);
            this.metroTabPage3.Controls.Add(this.metroLabel27);
            this.metroTabPage3.Controls.Add(this.metroTextBox_sfera_userKB);
            this.metroTabPage3.Controls.Add(this.metroTextBox_sfera_passwordKB);
            this.metroTabPage3.Controls.Add(this.metroLabel28);
            this.metroTabPage3.Controls.Add(this.metroLabel29);
            this.metroTabPage3.HorizontalScrollbarBarColor = true;
            this.metroTabPage3.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage3.HorizontalScrollbarSize = 10;
            this.metroTabPage3.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage3.Name = "metroTabPage3";
            this.metroTabPage3.Size = new System.Drawing.Size(940, 337);
            this.metroTabPage3.TabIndex = 2;
            this.metroTabPage3.Text = "Konekt Bis";
            this.metroTabPage3.VerticalScrollbarBarColor = true;
            this.metroTabPage3.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage3.VerticalScrollbarSize = 10;
            // 
            // metroLabel18
            // 
            this.metroLabel18.AutoSize = true;
            this.metroLabel18.Location = new System.Drawing.Point(96, 17);
            this.metroLabel18.Name = "metroLabel18";
            this.metroLabel18.Size = new System.Drawing.Size(51, 19);
            this.metroLabel18.TabIndex = 82;
            this.metroLabel18.Text = "Server:";
            // 
            // metroLabel21
            // 
            this.metroLabel21.AutoSize = true;
            this.metroLabel21.Location = new System.Drawing.Point(103, 72);
            this.metroLabel21.Name = "metroLabel21";
            this.metroLabel21.Size = new System.Drawing.Size(44, 19);
            this.metroLabel21.TabIndex = 84;
            this.metroLabel21.Text = "Hasło:";
            // 
            // metroTextBox_passKB
            // 
            // 
            // 
            // 
            this.metroTextBox_passKB.CustomButton.Image = null;
            this.metroTextBox_passKB.CustomButton.Location = new System.Drawing.Point(142, 1);
            this.metroTextBox_passKB.CustomButton.Name = "";
            this.metroTextBox_passKB.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox_passKB.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_passKB.CustomButton.TabIndex = 1;
            this.metroTextBox_passKB.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_passKB.CustomButton.UseSelectable = true;
            this.metroTextBox_passKB.CustomButton.Visible = false;
            this.metroTextBox_passKB.Lines = new string[0];
            this.metroTextBox_passKB.Location = new System.Drawing.Point(163, 72);
            this.metroTextBox_passKB.MaxLength = 32767;
            this.metroTextBox_passKB.Name = "metroTextBox_passKB";
            this.metroTextBox_passKB.PasswordChar = '*';
            this.metroTextBox_passKB.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_passKB.SelectedText = "";
            this.metroTextBox_passKB.SelectionLength = 0;
            this.metroTextBox_passKB.SelectionStart = 0;
            this.metroTextBox_passKB.ShortcutsEnabled = true;
            this.metroTextBox_passKB.Size = new System.Drawing.Size(164, 23);
            this.metroTextBox_passKB.TabIndex = 79;
            this.metroTextBox_passKB.UseSelectable = true;
            this.metroTextBox_passKB.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_passKB.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel22
            // 
            this.metroLabel22.AutoSize = true;
            this.metroLabel22.Location = new System.Drawing.Point(71, 42);
            this.metroLabel22.Name = "metroLabel22";
            this.metroLabel22.Size = new System.Drawing.Size(76, 19);
            this.metroLabel22.TabIndex = 83;
            this.metroLabel22.Text = "Użytkownik:";
            // 
            // metroLabel23
            // 
            this.metroLabel23.AutoSize = true;
            this.metroLabel23.Location = new System.Drawing.Point(103, 228);
            this.metroLabel23.Name = "metroLabel23";
            this.metroLabel23.Size = new System.Drawing.Size(44, 19);
            this.metroLabel23.TabIndex = 92;
            this.metroLabel23.Text = "Hasło:";
            // 
            // metroTextBox_userKB
            // 
            // 
            // 
            // 
            this.metroTextBox_userKB.CustomButton.Image = null;
            this.metroTextBox_userKB.CustomButton.Location = new System.Drawing.Point(142, 1);
            this.metroTextBox_userKB.CustomButton.Name = "";
            this.metroTextBox_userKB.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox_userKB.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_userKB.CustomButton.TabIndex = 1;
            this.metroTextBox_userKB.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_userKB.CustomButton.UseSelectable = true;
            this.metroTextBox_userKB.CustomButton.Visible = false;
            this.metroTextBox_userKB.Lines = new string[0];
            this.metroTextBox_userKB.Location = new System.Drawing.Point(163, 42);
            this.metroTextBox_userKB.MaxLength = 32767;
            this.metroTextBox_userKB.Name = "metroTextBox_userKB";
            this.metroTextBox_userKB.PasswordChar = '\0';
            this.metroTextBox_userKB.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_userKB.SelectedText = "";
            this.metroTextBox_userKB.SelectionLength = 0;
            this.metroTextBox_userKB.SelectionStart = 0;
            this.metroTextBox_userKB.ShortcutsEnabled = true;
            this.metroTextBox_userKB.Size = new System.Drawing.Size(164, 23);
            this.metroTextBox_userKB.TabIndex = 78;
            this.metroTextBox_userKB.UseSelectable = true;
            this.metroTextBox_userKB.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_userKB.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // comboBox_DatabaseKB
            // 
            this.comboBox_DatabaseKB.FormattingEnabled = true;
            this.comboBox_DatabaseKB.Location = new System.Drawing.Point(163, 137);
            this.comboBox_DatabaseKB.Name = "comboBox_DatabaseKB";
            this.comboBox_DatabaseKB.Size = new System.Drawing.Size(224, 21);
            this.comboBox_DatabaseKB.TabIndex = 87;
            this.comboBox_DatabaseKB.DropDown += new System.EventHandler(this.comboBox_DatabaseKB_DropDown);
            // 
            // metroCheckBox_authKB
            // 
            this.metroCheckBox_authKB.AutoSize = true;
            this.metroCheckBox_authKB.Location = new System.Drawing.Point(163, 106);
            this.metroCheckBox_authKB.Name = "metroCheckBox_authKB";
            this.metroCheckBox_authKB.Size = new System.Drawing.Size(26, 15);
            this.metroCheckBox_authKB.TabIndex = 80;
            this.metroCheckBox_authKB.Text = " ";
            this.metroCheckBox_authKB.UseSelectable = true;
            // 
            // metroLabel24
            // 
            this.metroLabel24.AutoSize = true;
            this.metroLabel24.Location = new System.Drawing.Point(107, 139);
            this.metroLabel24.Name = "metroLabel24";
            this.metroLabel24.Size = new System.Drawing.Size(40, 19);
            this.metroLabel24.TabIndex = 89;
            this.metroLabel24.Text = "Baza:";
            // 
            // metroTextBox_serverKB
            // 
            // 
            // 
            // 
            this.metroTextBox_serverKB.CustomButton.Image = null;
            this.metroTextBox_serverKB.CustomButton.Location = new System.Drawing.Point(142, 1);
            this.metroTextBox_serverKB.CustomButton.Name = "";
            this.metroTextBox_serverKB.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox_serverKB.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_serverKB.CustomButton.TabIndex = 1;
            this.metroTextBox_serverKB.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_serverKB.CustomButton.UseSelectable = true;
            this.metroTextBox_serverKB.CustomButton.Visible = false;
            this.metroTextBox_serverKB.Lines = new string[0];
            this.metroTextBox_serverKB.Location = new System.Drawing.Point(163, 13);
            this.metroTextBox_serverKB.MaxLength = 32767;
            this.metroTextBox_serverKB.Name = "metroTextBox_serverKB";
            this.metroTextBox_serverKB.PasswordChar = '\0';
            this.metroTextBox_serverKB.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_serverKB.SelectedText = "";
            this.metroTextBox_serverKB.SelectionLength = 0;
            this.metroTextBox_serverKB.SelectionStart = 0;
            this.metroTextBox_serverKB.ShortcutsEnabled = true;
            this.metroTextBox_serverKB.Size = new System.Drawing.Size(164, 23);
            this.metroTextBox_serverKB.TabIndex = 77;
            this.metroTextBox_serverKB.UseSelectable = true;
            this.metroTextBox_serverKB.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_serverKB.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel26
            // 
            this.metroLabel26.AutoSize = true;
            this.metroLabel26.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel26.Location = new System.Drawing.Point(163, 176);
            this.metroLabel26.Name = "metroLabel26";
            this.metroLabel26.Size = new System.Drawing.Size(50, 19);
            this.metroLabel26.TabIndex = 81;
            this.metroLabel26.Text = "SFERA";
            // 
            // metroLabel27
            // 
            this.metroLabel27.AutoSize = true;
            this.metroLabel27.Location = new System.Drawing.Point(5, 102);
            this.metroLabel27.Name = "metroLabel27";
            this.metroLabel27.Size = new System.Drawing.Size(142, 19);
            this.metroLabel27.TabIndex = 85;
            this.metroLabel27.Text = "Autentykacja Windows:";
            // 
            // metroTextBox_sfera_userKB
            // 
            // 
            // 
            // 
            this.metroTextBox_sfera_userKB.CustomButton.Image = null;
            this.metroTextBox_sfera_userKB.CustomButton.Location = new System.Drawing.Point(290, 1);
            this.metroTextBox_sfera_userKB.CustomButton.Name = "";
            this.metroTextBox_sfera_userKB.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox_sfera_userKB.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_sfera_userKB.CustomButton.TabIndex = 1;
            this.metroTextBox_sfera_userKB.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_sfera_userKB.CustomButton.UseSelectable = true;
            this.metroTextBox_sfera_userKB.CustomButton.Visible = false;
            this.metroTextBox_sfera_userKB.Lines = new string[0];
            this.metroTextBox_sfera_userKB.Location = new System.Drawing.Point(163, 198);
            this.metroTextBox_sfera_userKB.MaxLength = 32767;
            this.metroTextBox_sfera_userKB.Name = "metroTextBox_sfera_userKB";
            this.metroTextBox_sfera_userKB.PasswordChar = '\0';
            this.metroTextBox_sfera_userKB.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_sfera_userKB.SelectedText = "";
            this.metroTextBox_sfera_userKB.SelectionLength = 0;
            this.metroTextBox_sfera_userKB.SelectionStart = 0;
            this.metroTextBox_sfera_userKB.ShortcutsEnabled = true;
            this.metroTextBox_sfera_userKB.Size = new System.Drawing.Size(312, 23);
            this.metroTextBox_sfera_userKB.TabIndex = 88;
            this.metroTextBox_sfera_userKB.UseSelectable = true;
            this.metroTextBox_sfera_userKB.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_sfera_userKB.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBox_sfera_passwordKB
            // 
            // 
            // 
            // 
            this.metroTextBox_sfera_passwordKB.CustomButton.Image = null;
            this.metroTextBox_sfera_passwordKB.CustomButton.Location = new System.Drawing.Point(290, 1);
            this.metroTextBox_sfera_passwordKB.CustomButton.Name = "";
            this.metroTextBox_sfera_passwordKB.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox_sfera_passwordKB.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_sfera_passwordKB.CustomButton.TabIndex = 1;
            this.metroTextBox_sfera_passwordKB.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_sfera_passwordKB.CustomButton.UseSelectable = true;
            this.metroTextBox_sfera_passwordKB.CustomButton.Visible = false;
            this.metroTextBox_sfera_passwordKB.Lines = new string[0];
            this.metroTextBox_sfera_passwordKB.Location = new System.Drawing.Point(163, 228);
            this.metroTextBox_sfera_passwordKB.MaxLength = 32767;
            this.metroTextBox_sfera_passwordKB.Name = "metroTextBox_sfera_passwordKB";
            this.metroTextBox_sfera_passwordKB.PasswordChar = '*';
            this.metroTextBox_sfera_passwordKB.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_sfera_passwordKB.SelectedText = "";
            this.metroTextBox_sfera_passwordKB.SelectionLength = 0;
            this.metroTextBox_sfera_passwordKB.SelectionStart = 0;
            this.metroTextBox_sfera_passwordKB.ShortcutsEnabled = true;
            this.metroTextBox_sfera_passwordKB.Size = new System.Drawing.Size(312, 23);
            this.metroTextBox_sfera_passwordKB.TabIndex = 90;
            this.metroTextBox_sfera_passwordKB.UseSelectable = true;
            this.metroTextBox_sfera_passwordKB.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_sfera_passwordKB.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel28
            // 
            this.metroLabel28.AutoSize = true;
            this.metroLabel28.Location = new System.Drawing.Point(71, 198);
            this.metroLabel28.Name = "metroLabel28";
            this.metroLabel28.Size = new System.Drawing.Size(76, 19);
            this.metroLabel28.TabIndex = 91;
            this.metroLabel28.Text = "Użytkownik:";
            // 
            // metroLabel29
            // 
            this.metroLabel29.AutoSize = true;
            this.metroLabel29.Location = new System.Drawing.Point(333, 17);
            this.metroLabel29.Name = "metroLabel29";
            this.metroLabel29.Size = new System.Drawing.Size(101, 19);
            this.metroLabel29.Style = MetroFramework.MetroColorStyle.Silver;
            this.metroLabel29.TabIndex = 86;
            this.metroLabel29.Text = "xx.xx.xx.xx,pppp";
            this.metroLabel29.UseStyleColors = true;
            // 
            // metroTabPage4
            // 
            this.metroTabPage4.Controls.Add(this.metroLabel30);
            this.metroTabPage4.Controls.Add(this.metroLabel31);
            this.metroTabPage4.Controls.Add(this.metroTextBox_passKT);
            this.metroTabPage4.Controls.Add(this.metroLabel32);
            this.metroTabPage4.Controls.Add(this.metroLabel33);
            this.metroTabPage4.Controls.Add(this.metroTextBox_userKT);
            this.metroTabPage4.Controls.Add(this.comboBox_DatabaseKT);
            this.metroTabPage4.Controls.Add(this.metroCheckBox_authKT);
            this.metroTabPage4.Controls.Add(this.metroLabel34);
            this.metroTabPage4.Controls.Add(this.metroTextBox_serverKT);
            this.metroTabPage4.Controls.Add(this.metroLabel35);
            this.metroTabPage4.Controls.Add(this.metroLabel36);
            this.metroTabPage4.Controls.Add(this.metroTextBox_sfera_userKT);
            this.metroTabPage4.Controls.Add(this.metroTextBox_sfera_passwordKT);
            this.metroTabPage4.Controls.Add(this.metroLabel37);
            this.metroTabPage4.Controls.Add(this.metroLabel38);
            this.metroTabPage4.HorizontalScrollbarBarColor = true;
            this.metroTabPage4.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage4.HorizontalScrollbarSize = 10;
            this.metroTabPage4.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage4.Name = "metroTabPage4";
            this.metroTabPage4.Size = new System.Drawing.Size(940, 337);
            this.metroTabPage4.TabIndex = 3;
            this.metroTabPage4.Text = "Konekt Tkaniny";
            this.metroTabPage4.VerticalScrollbarBarColor = true;
            this.metroTabPage4.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage4.VerticalScrollbarSize = 10;
            // 
            // metroLabel30
            // 
            this.metroLabel30.AutoSize = true;
            this.metroLabel30.Location = new System.Drawing.Point(96, 17);
            this.metroLabel30.Name = "metroLabel30";
            this.metroLabel30.Size = new System.Drawing.Size(51, 19);
            this.metroLabel30.TabIndex = 82;
            this.metroLabel30.Text = "Server:";
            // 
            // metroLabel31
            // 
            this.metroLabel31.AutoSize = true;
            this.metroLabel31.Location = new System.Drawing.Point(103, 72);
            this.metroLabel31.Name = "metroLabel31";
            this.metroLabel31.Size = new System.Drawing.Size(44, 19);
            this.metroLabel31.TabIndex = 84;
            this.metroLabel31.Text = "Hasło:";
            // 
            // metroTextBox_passKT
            // 
            // 
            // 
            // 
            this.metroTextBox_passKT.CustomButton.Image = null;
            this.metroTextBox_passKT.CustomButton.Location = new System.Drawing.Point(142, 1);
            this.metroTextBox_passKT.CustomButton.Name = "";
            this.metroTextBox_passKT.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox_passKT.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_passKT.CustomButton.TabIndex = 1;
            this.metroTextBox_passKT.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_passKT.CustomButton.UseSelectable = true;
            this.metroTextBox_passKT.CustomButton.Visible = false;
            this.metroTextBox_passKT.Lines = new string[0];
            this.metroTextBox_passKT.Location = new System.Drawing.Point(163, 72);
            this.metroTextBox_passKT.MaxLength = 32767;
            this.metroTextBox_passKT.Name = "metroTextBox_passKT";
            this.metroTextBox_passKT.PasswordChar = '*';
            this.metroTextBox_passKT.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_passKT.SelectedText = "";
            this.metroTextBox_passKT.SelectionLength = 0;
            this.metroTextBox_passKT.SelectionStart = 0;
            this.metroTextBox_passKT.ShortcutsEnabled = true;
            this.metroTextBox_passKT.Size = new System.Drawing.Size(164, 23);
            this.metroTextBox_passKT.TabIndex = 79;
            this.metroTextBox_passKT.UseSelectable = true;
            this.metroTextBox_passKT.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_passKT.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel32
            // 
            this.metroLabel32.AutoSize = true;
            this.metroLabel32.Location = new System.Drawing.Point(71, 42);
            this.metroLabel32.Name = "metroLabel32";
            this.metroLabel32.Size = new System.Drawing.Size(76, 19);
            this.metroLabel32.TabIndex = 83;
            this.metroLabel32.Text = "Użytkownik:";
            // 
            // metroLabel33
            // 
            this.metroLabel33.AutoSize = true;
            this.metroLabel33.Location = new System.Drawing.Point(103, 228);
            this.metroLabel33.Name = "metroLabel33";
            this.metroLabel33.Size = new System.Drawing.Size(44, 19);
            this.metroLabel33.TabIndex = 92;
            this.metroLabel33.Text = "Hasło:";
            // 
            // metroTextBox_userKT
            // 
            // 
            // 
            // 
            this.metroTextBox_userKT.CustomButton.Image = null;
            this.metroTextBox_userKT.CustomButton.Location = new System.Drawing.Point(142, 1);
            this.metroTextBox_userKT.CustomButton.Name = "";
            this.metroTextBox_userKT.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox_userKT.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_userKT.CustomButton.TabIndex = 1;
            this.metroTextBox_userKT.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_userKT.CustomButton.UseSelectable = true;
            this.metroTextBox_userKT.CustomButton.Visible = false;
            this.metroTextBox_userKT.Lines = new string[0];
            this.metroTextBox_userKT.Location = new System.Drawing.Point(163, 42);
            this.metroTextBox_userKT.MaxLength = 32767;
            this.metroTextBox_userKT.Name = "metroTextBox_userKT";
            this.metroTextBox_userKT.PasswordChar = '\0';
            this.metroTextBox_userKT.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_userKT.SelectedText = "";
            this.metroTextBox_userKT.SelectionLength = 0;
            this.metroTextBox_userKT.SelectionStart = 0;
            this.metroTextBox_userKT.ShortcutsEnabled = true;
            this.metroTextBox_userKT.Size = new System.Drawing.Size(164, 23);
            this.metroTextBox_userKT.TabIndex = 78;
            this.metroTextBox_userKT.UseSelectable = true;
            this.metroTextBox_userKT.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_userKT.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // comboBox_DatabaseKT
            // 
            this.comboBox_DatabaseKT.FormattingEnabled = true;
            this.comboBox_DatabaseKT.Location = new System.Drawing.Point(163, 137);
            this.comboBox_DatabaseKT.Name = "comboBox_DatabaseKT";
            this.comboBox_DatabaseKT.Size = new System.Drawing.Size(224, 21);
            this.comboBox_DatabaseKT.TabIndex = 87;
            this.comboBox_DatabaseKT.DropDown += new System.EventHandler(this.comboBox_DatabaseKT_DropDown);
            // 
            // metroCheckBox_authKT
            // 
            this.metroCheckBox_authKT.AutoSize = true;
            this.metroCheckBox_authKT.Location = new System.Drawing.Point(163, 106);
            this.metroCheckBox_authKT.Name = "metroCheckBox_authKT";
            this.metroCheckBox_authKT.Size = new System.Drawing.Size(26, 15);
            this.metroCheckBox_authKT.TabIndex = 80;
            this.metroCheckBox_authKT.Text = " ";
            this.metroCheckBox_authKT.UseSelectable = true;
            // 
            // metroLabel34
            // 
            this.metroLabel34.AutoSize = true;
            this.metroLabel34.Location = new System.Drawing.Point(107, 139);
            this.metroLabel34.Name = "metroLabel34";
            this.metroLabel34.Size = new System.Drawing.Size(40, 19);
            this.metroLabel34.TabIndex = 89;
            this.metroLabel34.Text = "Baza:";
            // 
            // metroTextBox_serverKT
            // 
            // 
            // 
            // 
            this.metroTextBox_serverKT.CustomButton.Image = null;
            this.metroTextBox_serverKT.CustomButton.Location = new System.Drawing.Point(142, 1);
            this.metroTextBox_serverKT.CustomButton.Name = "";
            this.metroTextBox_serverKT.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox_serverKT.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_serverKT.CustomButton.TabIndex = 1;
            this.metroTextBox_serverKT.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_serverKT.CustomButton.UseSelectable = true;
            this.metroTextBox_serverKT.CustomButton.Visible = false;
            this.metroTextBox_serverKT.Lines = new string[0];
            this.metroTextBox_serverKT.Location = new System.Drawing.Point(163, 13);
            this.metroTextBox_serverKT.MaxLength = 32767;
            this.metroTextBox_serverKT.Name = "metroTextBox_serverKT";
            this.metroTextBox_serverKT.PasswordChar = '\0';
            this.metroTextBox_serverKT.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_serverKT.SelectedText = "";
            this.metroTextBox_serverKT.SelectionLength = 0;
            this.metroTextBox_serverKT.SelectionStart = 0;
            this.metroTextBox_serverKT.ShortcutsEnabled = true;
            this.metroTextBox_serverKT.Size = new System.Drawing.Size(164, 23);
            this.metroTextBox_serverKT.TabIndex = 77;
            this.metroTextBox_serverKT.UseSelectable = true;
            this.metroTextBox_serverKT.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_serverKT.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel35
            // 
            this.metroLabel35.AutoSize = true;
            this.metroLabel35.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel35.Location = new System.Drawing.Point(163, 176);
            this.metroLabel35.Name = "metroLabel35";
            this.metroLabel35.Size = new System.Drawing.Size(50, 19);
            this.metroLabel35.TabIndex = 81;
            this.metroLabel35.Text = "SFERA";
            // 
            // metroLabel36
            // 
            this.metroLabel36.AutoSize = true;
            this.metroLabel36.Location = new System.Drawing.Point(5, 102);
            this.metroLabel36.Name = "metroLabel36";
            this.metroLabel36.Size = new System.Drawing.Size(142, 19);
            this.metroLabel36.TabIndex = 85;
            this.metroLabel36.Text = "Autentykacja Windows:";
            // 
            // metroTextBox_sfera_userKT
            // 
            // 
            // 
            // 
            this.metroTextBox_sfera_userKT.CustomButton.Image = null;
            this.metroTextBox_sfera_userKT.CustomButton.Location = new System.Drawing.Point(290, 1);
            this.metroTextBox_sfera_userKT.CustomButton.Name = "";
            this.metroTextBox_sfera_userKT.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox_sfera_userKT.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_sfera_userKT.CustomButton.TabIndex = 1;
            this.metroTextBox_sfera_userKT.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_sfera_userKT.CustomButton.UseSelectable = true;
            this.metroTextBox_sfera_userKT.CustomButton.Visible = false;
            this.metroTextBox_sfera_userKT.Lines = new string[0];
            this.metroTextBox_sfera_userKT.Location = new System.Drawing.Point(163, 198);
            this.metroTextBox_sfera_userKT.MaxLength = 32767;
            this.metroTextBox_sfera_userKT.Name = "metroTextBox_sfera_userKT";
            this.metroTextBox_sfera_userKT.PasswordChar = '\0';
            this.metroTextBox_sfera_userKT.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_sfera_userKT.SelectedText = "";
            this.metroTextBox_sfera_userKT.SelectionLength = 0;
            this.metroTextBox_sfera_userKT.SelectionStart = 0;
            this.metroTextBox_sfera_userKT.ShortcutsEnabled = true;
            this.metroTextBox_sfera_userKT.Size = new System.Drawing.Size(312, 23);
            this.metroTextBox_sfera_userKT.TabIndex = 88;
            this.metroTextBox_sfera_userKT.UseSelectable = true;
            this.metroTextBox_sfera_userKT.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_sfera_userKT.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBox_sfera_passwordKT
            // 
            // 
            // 
            // 
            this.metroTextBox_sfera_passwordKT.CustomButton.Image = null;
            this.metroTextBox_sfera_passwordKT.CustomButton.Location = new System.Drawing.Point(290, 1);
            this.metroTextBox_sfera_passwordKT.CustomButton.Name = "";
            this.metroTextBox_sfera_passwordKT.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox_sfera_passwordKT.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_sfera_passwordKT.CustomButton.TabIndex = 1;
            this.metroTextBox_sfera_passwordKT.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_sfera_passwordKT.CustomButton.UseSelectable = true;
            this.metroTextBox_sfera_passwordKT.CustomButton.Visible = false;
            this.metroTextBox_sfera_passwordKT.Lines = new string[0];
            this.metroTextBox_sfera_passwordKT.Location = new System.Drawing.Point(163, 228);
            this.metroTextBox_sfera_passwordKT.MaxLength = 32767;
            this.metroTextBox_sfera_passwordKT.Name = "metroTextBox_sfera_passwordKT";
            this.metroTextBox_sfera_passwordKT.PasswordChar = '*';
            this.metroTextBox_sfera_passwordKT.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_sfera_passwordKT.SelectedText = "";
            this.metroTextBox_sfera_passwordKT.SelectionLength = 0;
            this.metroTextBox_sfera_passwordKT.SelectionStart = 0;
            this.metroTextBox_sfera_passwordKT.ShortcutsEnabled = true;
            this.metroTextBox_sfera_passwordKT.Size = new System.Drawing.Size(312, 23);
            this.metroTextBox_sfera_passwordKT.TabIndex = 90;
            this.metroTextBox_sfera_passwordKT.UseSelectable = true;
            this.metroTextBox_sfera_passwordKT.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_sfera_passwordKT.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel37
            // 
            this.metroLabel37.AutoSize = true;
            this.metroLabel37.Location = new System.Drawing.Point(71, 198);
            this.metroLabel37.Name = "metroLabel37";
            this.metroLabel37.Size = new System.Drawing.Size(76, 19);
            this.metroLabel37.TabIndex = 91;
            this.metroLabel37.Text = "Użytkownik:";
            // 
            // metroLabel38
            // 
            this.metroLabel38.AutoSize = true;
            this.metroLabel38.Location = new System.Drawing.Point(333, 17);
            this.metroLabel38.Name = "metroLabel38";
            this.metroLabel38.Size = new System.Drawing.Size(101, 19);
            this.metroLabel38.Style = MetroFramework.MetroColorStyle.Silver;
            this.metroLabel38.TabIndex = 86;
            this.metroLabel38.Text = "xx.xx.xx.xx,pppp";
            this.metroLabel38.UseStyleColors = true;
            // 
            // metroTabPage5
            // 
            this.metroTabPage5.Controls.Add(this.metroLabel39);
            this.metroTabPage5.Controls.Add(this.metroLabel40);
            this.metroTabPage5.Controls.Add(this.metroTextBox_passWB);
            this.metroTabPage5.Controls.Add(this.metroLabel41);
            this.metroTabPage5.Controls.Add(this.metroLabel42);
            this.metroTabPage5.Controls.Add(this.metroTextBox_userWB);
            this.metroTabPage5.Controls.Add(this.comboBox_DatabaseWB);
            this.metroTabPage5.Controls.Add(this.metroCheckBox_authWB);
            this.metroTabPage5.Controls.Add(this.metroLabel43);
            this.metroTabPage5.Controls.Add(this.metroTextBox_serverWB);
            this.metroTabPage5.Controls.Add(this.metroLabel44);
            this.metroTabPage5.Controls.Add(this.metroLabel45);
            this.metroTabPage5.Controls.Add(this.metroTextBox_sfera_userWB);
            this.metroTabPage5.Controls.Add(this.metroTextBox_sfera_passwordWB);
            this.metroTabPage5.Controls.Add(this.metroLabel46);
            this.metroTabPage5.Controls.Add(this.metroLabel47);
            this.metroTabPage5.HorizontalScrollbarBarColor = true;
            this.metroTabPage5.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage5.HorizontalScrollbarSize = 10;
            this.metroTabPage5.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage5.Name = "metroTabPage5";
            this.metroTabPage5.Size = new System.Drawing.Size(940, 337);
            this.metroTabPage5.TabIndex = 4;
            this.metroTabPage5.Text = "Wektor Bis";
            this.metroTabPage5.VerticalScrollbarBarColor = true;
            this.metroTabPage5.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage5.VerticalScrollbarSize = 10;
            // 
            // metroLabel39
            // 
            this.metroLabel39.AutoSize = true;
            this.metroLabel39.Location = new System.Drawing.Point(96, 17);
            this.metroLabel39.Name = "metroLabel39";
            this.metroLabel39.Size = new System.Drawing.Size(51, 19);
            this.metroLabel39.TabIndex = 98;
            this.metroLabel39.Text = "Server:";
            // 
            // metroLabel40
            // 
            this.metroLabel40.AutoSize = true;
            this.metroLabel40.Location = new System.Drawing.Point(103, 72);
            this.metroLabel40.Name = "metroLabel40";
            this.metroLabel40.Size = new System.Drawing.Size(44, 19);
            this.metroLabel40.TabIndex = 100;
            this.metroLabel40.Text = "Hasło:";
            // 
            // metroTextBox_passWB
            // 
            // 
            // 
            // 
            this.metroTextBox_passWB.CustomButton.Image = null;
            this.metroTextBox_passWB.CustomButton.Location = new System.Drawing.Point(142, 1);
            this.metroTextBox_passWB.CustomButton.Name = "";
            this.metroTextBox_passWB.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox_passWB.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_passWB.CustomButton.TabIndex = 1;
            this.metroTextBox_passWB.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_passWB.CustomButton.UseSelectable = true;
            this.metroTextBox_passWB.CustomButton.Visible = false;
            this.metroTextBox_passWB.Lines = new string[0];
            this.metroTextBox_passWB.Location = new System.Drawing.Point(163, 72);
            this.metroTextBox_passWB.MaxLength = 32767;
            this.metroTextBox_passWB.Name = "metroTextBox_passWB";
            this.metroTextBox_passWB.PasswordChar = '*';
            this.metroTextBox_passWB.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_passWB.SelectedText = "";
            this.metroTextBox_passWB.SelectionLength = 0;
            this.metroTextBox_passWB.SelectionStart = 0;
            this.metroTextBox_passWB.ShortcutsEnabled = true;
            this.metroTextBox_passWB.Size = new System.Drawing.Size(164, 23);
            this.metroTextBox_passWB.TabIndex = 95;
            this.metroTextBox_passWB.UseSelectable = true;
            this.metroTextBox_passWB.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_passWB.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel41
            // 
            this.metroLabel41.AutoSize = true;
            this.metroLabel41.Location = new System.Drawing.Point(71, 42);
            this.metroLabel41.Name = "metroLabel41";
            this.metroLabel41.Size = new System.Drawing.Size(76, 19);
            this.metroLabel41.TabIndex = 99;
            this.metroLabel41.Text = "Użytkownik:";
            // 
            // metroLabel42
            // 
            this.metroLabel42.AutoSize = true;
            this.metroLabel42.Location = new System.Drawing.Point(103, 228);
            this.metroLabel42.Name = "metroLabel42";
            this.metroLabel42.Size = new System.Drawing.Size(44, 19);
            this.metroLabel42.TabIndex = 108;
            this.metroLabel42.Text = "Hasło:";
            // 
            // metroTextBox_userWB
            // 
            // 
            // 
            // 
            this.metroTextBox_userWB.CustomButton.Image = null;
            this.metroTextBox_userWB.CustomButton.Location = new System.Drawing.Point(142, 1);
            this.metroTextBox_userWB.CustomButton.Name = "";
            this.metroTextBox_userWB.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox_userWB.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_userWB.CustomButton.TabIndex = 1;
            this.metroTextBox_userWB.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_userWB.CustomButton.UseSelectable = true;
            this.metroTextBox_userWB.CustomButton.Visible = false;
            this.metroTextBox_userWB.Lines = new string[0];
            this.metroTextBox_userWB.Location = new System.Drawing.Point(163, 42);
            this.metroTextBox_userWB.MaxLength = 32767;
            this.metroTextBox_userWB.Name = "metroTextBox_userWB";
            this.metroTextBox_userWB.PasswordChar = '\0';
            this.metroTextBox_userWB.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_userWB.SelectedText = "";
            this.metroTextBox_userWB.SelectionLength = 0;
            this.metroTextBox_userWB.SelectionStart = 0;
            this.metroTextBox_userWB.ShortcutsEnabled = true;
            this.metroTextBox_userWB.Size = new System.Drawing.Size(164, 23);
            this.metroTextBox_userWB.TabIndex = 94;
            this.metroTextBox_userWB.UseSelectable = true;
            this.metroTextBox_userWB.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_userWB.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // comboBox_DatabaseWB
            // 
            this.comboBox_DatabaseWB.FormattingEnabled = true;
            this.comboBox_DatabaseWB.Location = new System.Drawing.Point(163, 137);
            this.comboBox_DatabaseWB.Name = "comboBox_DatabaseWB";
            this.comboBox_DatabaseWB.Size = new System.Drawing.Size(224, 21);
            this.comboBox_DatabaseWB.TabIndex = 103;
            this.comboBox_DatabaseWB.DropDown += new System.EventHandler(this.comboBox_DatabaseWB_DropDown);
            // 
            // metroCheckBox_authWB
            // 
            this.metroCheckBox_authWB.AutoSize = true;
            this.metroCheckBox_authWB.Location = new System.Drawing.Point(163, 106);
            this.metroCheckBox_authWB.Name = "metroCheckBox_authWB";
            this.metroCheckBox_authWB.Size = new System.Drawing.Size(26, 15);
            this.metroCheckBox_authWB.TabIndex = 96;
            this.metroCheckBox_authWB.Text = " ";
            this.metroCheckBox_authWB.UseSelectable = true;
            // 
            // metroLabel43
            // 
            this.metroLabel43.AutoSize = true;
            this.metroLabel43.Location = new System.Drawing.Point(107, 139);
            this.metroLabel43.Name = "metroLabel43";
            this.metroLabel43.Size = new System.Drawing.Size(40, 19);
            this.metroLabel43.TabIndex = 105;
            this.metroLabel43.Text = "Baza:";
            // 
            // metroTextBox_serverWB
            // 
            // 
            // 
            // 
            this.metroTextBox_serverWB.CustomButton.Image = null;
            this.metroTextBox_serverWB.CustomButton.Location = new System.Drawing.Point(142, 1);
            this.metroTextBox_serverWB.CustomButton.Name = "";
            this.metroTextBox_serverWB.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox_serverWB.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_serverWB.CustomButton.TabIndex = 1;
            this.metroTextBox_serverWB.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_serverWB.CustomButton.UseSelectable = true;
            this.metroTextBox_serverWB.CustomButton.Visible = false;
            this.metroTextBox_serverWB.Lines = new string[0];
            this.metroTextBox_serverWB.Location = new System.Drawing.Point(163, 13);
            this.metroTextBox_serverWB.MaxLength = 32767;
            this.metroTextBox_serverWB.Name = "metroTextBox_serverWB";
            this.metroTextBox_serverWB.PasswordChar = '\0';
            this.metroTextBox_serverWB.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_serverWB.SelectedText = "";
            this.metroTextBox_serverWB.SelectionLength = 0;
            this.metroTextBox_serverWB.SelectionStart = 0;
            this.metroTextBox_serverWB.ShortcutsEnabled = true;
            this.metroTextBox_serverWB.Size = new System.Drawing.Size(164, 23);
            this.metroTextBox_serverWB.TabIndex = 93;
            this.metroTextBox_serverWB.UseSelectable = true;
            this.metroTextBox_serverWB.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_serverWB.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel44
            // 
            this.metroLabel44.AutoSize = true;
            this.metroLabel44.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel44.Location = new System.Drawing.Point(163, 176);
            this.metroLabel44.Name = "metroLabel44";
            this.metroLabel44.Size = new System.Drawing.Size(50, 19);
            this.metroLabel44.TabIndex = 97;
            this.metroLabel44.Text = "SFERA";
            // 
            // metroLabel45
            // 
            this.metroLabel45.AutoSize = true;
            this.metroLabel45.Location = new System.Drawing.Point(5, 102);
            this.metroLabel45.Name = "metroLabel45";
            this.metroLabel45.Size = new System.Drawing.Size(142, 19);
            this.metroLabel45.TabIndex = 101;
            this.metroLabel45.Text = "Autentykacja Windows:";
            // 
            // metroTextBox_sfera_userWB
            // 
            // 
            // 
            // 
            this.metroTextBox_sfera_userWB.CustomButton.Image = null;
            this.metroTextBox_sfera_userWB.CustomButton.Location = new System.Drawing.Point(290, 1);
            this.metroTextBox_sfera_userWB.CustomButton.Name = "";
            this.metroTextBox_sfera_userWB.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox_sfera_userWB.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_sfera_userWB.CustomButton.TabIndex = 1;
            this.metroTextBox_sfera_userWB.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_sfera_userWB.CustomButton.UseSelectable = true;
            this.metroTextBox_sfera_userWB.CustomButton.Visible = false;
            this.metroTextBox_sfera_userWB.Lines = new string[0];
            this.metroTextBox_sfera_userWB.Location = new System.Drawing.Point(163, 198);
            this.metroTextBox_sfera_userWB.MaxLength = 32767;
            this.metroTextBox_sfera_userWB.Name = "metroTextBox_sfera_userWB";
            this.metroTextBox_sfera_userWB.PasswordChar = '\0';
            this.metroTextBox_sfera_userWB.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_sfera_userWB.SelectedText = "";
            this.metroTextBox_sfera_userWB.SelectionLength = 0;
            this.metroTextBox_sfera_userWB.SelectionStart = 0;
            this.metroTextBox_sfera_userWB.ShortcutsEnabled = true;
            this.metroTextBox_sfera_userWB.Size = new System.Drawing.Size(312, 23);
            this.metroTextBox_sfera_userWB.TabIndex = 104;
            this.metroTextBox_sfera_userWB.UseSelectable = true;
            this.metroTextBox_sfera_userWB.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_sfera_userWB.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBox_sfera_passwordWB
            // 
            // 
            // 
            // 
            this.metroTextBox_sfera_passwordWB.CustomButton.Image = null;
            this.metroTextBox_sfera_passwordWB.CustomButton.Location = new System.Drawing.Point(290, 1);
            this.metroTextBox_sfera_passwordWB.CustomButton.Name = "";
            this.metroTextBox_sfera_passwordWB.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox_sfera_passwordWB.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_sfera_passwordWB.CustomButton.TabIndex = 1;
            this.metroTextBox_sfera_passwordWB.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_sfera_passwordWB.CustomButton.UseSelectable = true;
            this.metroTextBox_sfera_passwordWB.CustomButton.Visible = false;
            this.metroTextBox_sfera_passwordWB.Lines = new string[0];
            this.metroTextBox_sfera_passwordWB.Location = new System.Drawing.Point(163, 228);
            this.metroTextBox_sfera_passwordWB.MaxLength = 32767;
            this.metroTextBox_sfera_passwordWB.Name = "metroTextBox_sfera_passwordWB";
            this.metroTextBox_sfera_passwordWB.PasswordChar = '*';
            this.metroTextBox_sfera_passwordWB.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_sfera_passwordWB.SelectedText = "";
            this.metroTextBox_sfera_passwordWB.SelectionLength = 0;
            this.metroTextBox_sfera_passwordWB.SelectionStart = 0;
            this.metroTextBox_sfera_passwordWB.ShortcutsEnabled = true;
            this.metroTextBox_sfera_passwordWB.Size = new System.Drawing.Size(312, 23);
            this.metroTextBox_sfera_passwordWB.TabIndex = 106;
            this.metroTextBox_sfera_passwordWB.UseSelectable = true;
            this.metroTextBox_sfera_passwordWB.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_sfera_passwordWB.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel46
            // 
            this.metroLabel46.AutoSize = true;
            this.metroLabel46.Location = new System.Drawing.Point(71, 198);
            this.metroLabel46.Name = "metroLabel46";
            this.metroLabel46.Size = new System.Drawing.Size(76, 19);
            this.metroLabel46.TabIndex = 107;
            this.metroLabel46.Text = "Użytkownik:";
            // 
            // metroLabel47
            // 
            this.metroLabel47.AutoSize = true;
            this.metroLabel47.Location = new System.Drawing.Point(333, 17);
            this.metroLabel47.Name = "metroLabel47";
            this.metroLabel47.Size = new System.Drawing.Size(101, 19);
            this.metroLabel47.Style = MetroFramework.MetroColorStyle.Silver;
            this.metroLabel47.TabIndex = 102;
            this.metroLabel47.Text = "xx.xx.xx.xx,pppp";
            this.metroLabel47.UseStyleColors = true;
            // 
            // metroTabPage6
            // 
            this.metroTabPage6.Controls.Add(this.metroLabel48);
            this.metroTabPage6.Controls.Add(this.metroLabel49);
            this.metroTabPage6.Controls.Add(this.metroTextBox_passW);
            this.metroTabPage6.Controls.Add(this.metroLabel50);
            this.metroTabPage6.Controls.Add(this.metroTextBox_userW);
            this.metroTabPage6.Controls.Add(this.comboBox_DatabaseW);
            this.metroTabPage6.Controls.Add(this.metroCheckBox_authW);
            this.metroTabPage6.Controls.Add(this.metroLabel52);
            this.metroTabPage6.Controls.Add(this.metroTextBox_serverW);
            this.metroTabPage6.Controls.Add(this.metroLabel54);
            this.metroTabPage6.Controls.Add(this.metroLabel56);
            this.metroTabPage6.HorizontalScrollbarBarColor = true;
            this.metroTabPage6.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage6.HorizontalScrollbarSize = 10;
            this.metroTabPage6.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage6.Name = "metroTabPage6";
            this.metroTabPage6.Size = new System.Drawing.Size(940, 337);
            this.metroTabPage6.TabIndex = 5;
            this.metroTabPage6.Text = "WEKTOR";
            this.metroTabPage6.VerticalScrollbarBarColor = true;
            this.metroTabPage6.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage6.VerticalScrollbarSize = 10;
            // 
            // metroLabel48
            // 
            this.metroLabel48.AutoSize = true;
            this.metroLabel48.Location = new System.Drawing.Point(96, 17);
            this.metroLabel48.Name = "metroLabel48";
            this.metroLabel48.Size = new System.Drawing.Size(51, 19);
            this.metroLabel48.TabIndex = 114;
            this.metroLabel48.Text = "Server:";
            // 
            // metroLabel49
            // 
            this.metroLabel49.AutoSize = true;
            this.metroLabel49.Location = new System.Drawing.Point(103, 72);
            this.metroLabel49.Name = "metroLabel49";
            this.metroLabel49.Size = new System.Drawing.Size(44, 19);
            this.metroLabel49.TabIndex = 116;
            this.metroLabel49.Text = "Hasło:";
            // 
            // metroTextBox_passW
            // 
            // 
            // 
            // 
            this.metroTextBox_passW.CustomButton.Image = null;
            this.metroTextBox_passW.CustomButton.Location = new System.Drawing.Point(142, 1);
            this.metroTextBox_passW.CustomButton.Name = "";
            this.metroTextBox_passW.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox_passW.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_passW.CustomButton.TabIndex = 1;
            this.metroTextBox_passW.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_passW.CustomButton.UseSelectable = true;
            this.metroTextBox_passW.CustomButton.Visible = false;
            this.metroTextBox_passW.Lines = new string[0];
            this.metroTextBox_passW.Location = new System.Drawing.Point(163, 72);
            this.metroTextBox_passW.MaxLength = 32767;
            this.metroTextBox_passW.Name = "metroTextBox_passW";
            this.metroTextBox_passW.PasswordChar = '*';
            this.metroTextBox_passW.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_passW.SelectedText = "";
            this.metroTextBox_passW.SelectionLength = 0;
            this.metroTextBox_passW.SelectionStart = 0;
            this.metroTextBox_passW.ShortcutsEnabled = true;
            this.metroTextBox_passW.Size = new System.Drawing.Size(164, 23);
            this.metroTextBox_passW.TabIndex = 111;
            this.metroTextBox_passW.UseSelectable = true;
            this.metroTextBox_passW.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_passW.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel50
            // 
            this.metroLabel50.AutoSize = true;
            this.metroLabel50.Location = new System.Drawing.Point(71, 42);
            this.metroLabel50.Name = "metroLabel50";
            this.metroLabel50.Size = new System.Drawing.Size(76, 19);
            this.metroLabel50.TabIndex = 115;
            this.metroLabel50.Text = "Użytkownik:";
            // 
            // metroTextBox_userW
            // 
            // 
            // 
            // 
            this.metroTextBox_userW.CustomButton.Image = null;
            this.metroTextBox_userW.CustomButton.Location = new System.Drawing.Point(142, 1);
            this.metroTextBox_userW.CustomButton.Name = "";
            this.metroTextBox_userW.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox_userW.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_userW.CustomButton.TabIndex = 1;
            this.metroTextBox_userW.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_userW.CustomButton.UseSelectable = true;
            this.metroTextBox_userW.CustomButton.Visible = false;
            this.metroTextBox_userW.Lines = new string[0];
            this.metroTextBox_userW.Location = new System.Drawing.Point(163, 42);
            this.metroTextBox_userW.MaxLength = 32767;
            this.metroTextBox_userW.Name = "metroTextBox_userW";
            this.metroTextBox_userW.PasswordChar = '\0';
            this.metroTextBox_userW.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_userW.SelectedText = "";
            this.metroTextBox_userW.SelectionLength = 0;
            this.metroTextBox_userW.SelectionStart = 0;
            this.metroTextBox_userW.ShortcutsEnabled = true;
            this.metroTextBox_userW.Size = new System.Drawing.Size(164, 23);
            this.metroTextBox_userW.TabIndex = 110;
            this.metroTextBox_userW.UseSelectable = true;
            this.metroTextBox_userW.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_userW.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // comboBox_DatabaseW
            // 
            this.comboBox_DatabaseW.FormattingEnabled = true;
            this.comboBox_DatabaseW.Location = new System.Drawing.Point(163, 137);
            this.comboBox_DatabaseW.Name = "comboBox_DatabaseW";
            this.comboBox_DatabaseW.Size = new System.Drawing.Size(224, 21);
            this.comboBox_DatabaseW.TabIndex = 119;
            this.comboBox_DatabaseW.DropDown += new System.EventHandler(this.comboBox_DatabaseW_DropDown);
            // 
            // metroCheckBox_authW
            // 
            this.metroCheckBox_authW.AutoSize = true;
            this.metroCheckBox_authW.Location = new System.Drawing.Point(163, 106);
            this.metroCheckBox_authW.Name = "metroCheckBox_authW";
            this.metroCheckBox_authW.Size = new System.Drawing.Size(26, 15);
            this.metroCheckBox_authW.TabIndex = 112;
            this.metroCheckBox_authW.Text = " ";
            this.metroCheckBox_authW.UseSelectable = true;
            // 
            // metroLabel52
            // 
            this.metroLabel52.AutoSize = true;
            this.metroLabel52.Location = new System.Drawing.Point(107, 139);
            this.metroLabel52.Name = "metroLabel52";
            this.metroLabel52.Size = new System.Drawing.Size(40, 19);
            this.metroLabel52.TabIndex = 121;
            this.metroLabel52.Text = "Baza:";
            // 
            // metroTextBox_serverW
            // 
            // 
            // 
            // 
            this.metroTextBox_serverW.CustomButton.Image = null;
            this.metroTextBox_serverW.CustomButton.Location = new System.Drawing.Point(142, 1);
            this.metroTextBox_serverW.CustomButton.Name = "";
            this.metroTextBox_serverW.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox_serverW.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_serverW.CustomButton.TabIndex = 1;
            this.metroTextBox_serverW.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_serverW.CustomButton.UseSelectable = true;
            this.metroTextBox_serverW.CustomButton.Visible = false;
            this.metroTextBox_serverW.Lines = new string[0];
            this.metroTextBox_serverW.Location = new System.Drawing.Point(163, 13);
            this.metroTextBox_serverW.MaxLength = 32767;
            this.metroTextBox_serverW.Name = "metroTextBox_serverW";
            this.metroTextBox_serverW.PasswordChar = '\0';
            this.metroTextBox_serverW.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_serverW.SelectedText = "";
            this.metroTextBox_serverW.SelectionLength = 0;
            this.metroTextBox_serverW.SelectionStart = 0;
            this.metroTextBox_serverW.ShortcutsEnabled = true;
            this.metroTextBox_serverW.Size = new System.Drawing.Size(164, 23);
            this.metroTextBox_serverW.TabIndex = 109;
            this.metroTextBox_serverW.UseSelectable = true;
            this.metroTextBox_serverW.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_serverW.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel54
            // 
            this.metroLabel54.AutoSize = true;
            this.metroLabel54.Location = new System.Drawing.Point(5, 102);
            this.metroLabel54.Name = "metroLabel54";
            this.metroLabel54.Size = new System.Drawing.Size(142, 19);
            this.metroLabel54.TabIndex = 117;
            this.metroLabel54.Text = "Autentykacja Windows:";
            // 
            // metroLabel56
            // 
            this.metroLabel56.AutoSize = true;
            this.metroLabel56.Location = new System.Drawing.Point(333, 17);
            this.metroLabel56.Name = "metroLabel56";
            this.metroLabel56.Size = new System.Drawing.Size(101, 19);
            this.metroLabel56.Style = MetroFramework.MetroColorStyle.Silver;
            this.metroLabel56.TabIndex = 118;
            this.metroLabel56.Text = "xx.xx.xx.xx,pppp";
            this.metroLabel56.UseStyleColors = true;
            // 
            // metroTabPage7
            // 
            this.metroTabPage7.Controls.Add(this.metroLabel51);
            this.metroTabPage7.Controls.Add(this.metroLabel53);
            this.metroTabPage7.Controls.Add(this.metroTextBox_passT);
            this.metroTabPage7.Controls.Add(this.metroLabel55);
            this.metroTabPage7.Controls.Add(this.metroLabel57);
            this.metroTabPage7.Controls.Add(this.metroTextBox_userT);
            this.metroTabPage7.Controls.Add(this.comboBox_DatabaseT);
            this.metroTabPage7.Controls.Add(this.metroCheckBox_authT);
            this.metroTabPage7.Controls.Add(this.metroLabel58);
            this.metroTabPage7.Controls.Add(this.metroTextBox_serverT);
            this.metroTabPage7.Controls.Add(this.metroLabel59);
            this.metroTabPage7.Controls.Add(this.metroLabel60);
            this.metroTabPage7.Controls.Add(this.metroTextBox_sfera_userT);
            this.metroTabPage7.Controls.Add(this.metroTextBox_sfera_passwordT);
            this.metroTabPage7.Controls.Add(this.metroLabel61);
            this.metroTabPage7.Controls.Add(this.metroLabel62);
            this.metroTabPage7.HorizontalScrollbarBarColor = true;
            this.metroTabPage7.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage7.HorizontalScrollbarSize = 10;
            this.metroTabPage7.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage7.Name = "metroTabPage7";
            this.metroTabPage7.Size = new System.Drawing.Size(940, 337);
            this.metroTabPage7.TabIndex = 6;
            this.metroTabPage7.Text = "Elex Tkaniny";
            this.metroTabPage7.VerticalScrollbarBarColor = true;
            this.metroTabPage7.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage7.VerticalScrollbarSize = 10;
            // 
            // metroLabel51
            // 
            this.metroLabel51.AutoSize = true;
            this.metroLabel51.Location = new System.Drawing.Point(96, 17);
            this.metroLabel51.Name = "metroLabel51";
            this.metroLabel51.Size = new System.Drawing.Size(51, 19);
            this.metroLabel51.TabIndex = 98;
            this.metroLabel51.Text = "Server:";
            // 
            // metroLabel53
            // 
            this.metroLabel53.AutoSize = true;
            this.metroLabel53.Location = new System.Drawing.Point(103, 72);
            this.metroLabel53.Name = "metroLabel53";
            this.metroLabel53.Size = new System.Drawing.Size(44, 19);
            this.metroLabel53.TabIndex = 100;
            this.metroLabel53.Text = "Hasło:";
            // 
            // metroTextBox_passT
            // 
            // 
            // 
            // 
            this.metroTextBox_passT.CustomButton.Image = null;
            this.metroTextBox_passT.CustomButton.Location = new System.Drawing.Point(142, 1);
            this.metroTextBox_passT.CustomButton.Name = "";
            this.metroTextBox_passT.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox_passT.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_passT.CustomButton.TabIndex = 1;
            this.metroTextBox_passT.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_passT.CustomButton.UseSelectable = true;
            this.metroTextBox_passT.CustomButton.Visible = false;
            this.metroTextBox_passT.Lines = new string[0];
            this.metroTextBox_passT.Location = new System.Drawing.Point(163, 72);
            this.metroTextBox_passT.MaxLength = 32767;
            this.metroTextBox_passT.Name = "metroTextBox_passT";
            this.metroTextBox_passT.PasswordChar = '*';
            this.metroTextBox_passT.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_passT.SelectedText = "";
            this.metroTextBox_passT.SelectionLength = 0;
            this.metroTextBox_passT.SelectionStart = 0;
            this.metroTextBox_passT.ShortcutsEnabled = true;
            this.metroTextBox_passT.Size = new System.Drawing.Size(164, 23);
            this.metroTextBox_passT.TabIndex = 95;
            this.metroTextBox_passT.UseSelectable = true;
            this.metroTextBox_passT.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_passT.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel55
            // 
            this.metroLabel55.AutoSize = true;
            this.metroLabel55.Location = new System.Drawing.Point(71, 42);
            this.metroLabel55.Name = "metroLabel55";
            this.metroLabel55.Size = new System.Drawing.Size(76, 19);
            this.metroLabel55.TabIndex = 99;
            this.metroLabel55.Text = "Użytkownik:";
            // 
            // metroLabel57
            // 
            this.metroLabel57.AutoSize = true;
            this.metroLabel57.Location = new System.Drawing.Point(103, 228);
            this.metroLabel57.Name = "metroLabel57";
            this.metroLabel57.Size = new System.Drawing.Size(44, 19);
            this.metroLabel57.TabIndex = 108;
            this.metroLabel57.Text = "Hasło:";
            // 
            // metroTextBox_userT
            // 
            // 
            // 
            // 
            this.metroTextBox_userT.CustomButton.Image = null;
            this.metroTextBox_userT.CustomButton.Location = new System.Drawing.Point(142, 1);
            this.metroTextBox_userT.CustomButton.Name = "";
            this.metroTextBox_userT.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox_userT.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_userT.CustomButton.TabIndex = 1;
            this.metroTextBox_userT.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_userT.CustomButton.UseSelectable = true;
            this.metroTextBox_userT.CustomButton.Visible = false;
            this.metroTextBox_userT.Lines = new string[0];
            this.metroTextBox_userT.Location = new System.Drawing.Point(163, 42);
            this.metroTextBox_userT.MaxLength = 32767;
            this.metroTextBox_userT.Name = "metroTextBox_userT";
            this.metroTextBox_userT.PasswordChar = '\0';
            this.metroTextBox_userT.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_userT.SelectedText = "";
            this.metroTextBox_userT.SelectionLength = 0;
            this.metroTextBox_userT.SelectionStart = 0;
            this.metroTextBox_userT.ShortcutsEnabled = true;
            this.metroTextBox_userT.Size = new System.Drawing.Size(164, 23);
            this.metroTextBox_userT.TabIndex = 94;
            this.metroTextBox_userT.UseSelectable = true;
            this.metroTextBox_userT.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_userT.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // comboBox_DatabaseT
            // 
            this.comboBox_DatabaseT.FormattingEnabled = true;
            this.comboBox_DatabaseT.Location = new System.Drawing.Point(163, 137);
            this.comboBox_DatabaseT.Name = "comboBox_DatabaseT";
            this.comboBox_DatabaseT.Size = new System.Drawing.Size(224, 21);
            this.comboBox_DatabaseT.TabIndex = 103;
            this.comboBox_DatabaseT.DropDown += new System.EventHandler(this.comboBox_DatabaseT_DropDown);
            // 
            // metroCheckBox_authT
            // 
            this.metroCheckBox_authT.AutoSize = true;
            this.metroCheckBox_authT.Location = new System.Drawing.Point(163, 106);
            this.metroCheckBox_authT.Name = "metroCheckBox_authT";
            this.metroCheckBox_authT.Size = new System.Drawing.Size(26, 15);
            this.metroCheckBox_authT.TabIndex = 96;
            this.metroCheckBox_authT.Text = " ";
            this.metroCheckBox_authT.UseSelectable = true;
            // 
            // metroLabel58
            // 
            this.metroLabel58.AutoSize = true;
            this.metroLabel58.Location = new System.Drawing.Point(107, 139);
            this.metroLabel58.Name = "metroLabel58";
            this.metroLabel58.Size = new System.Drawing.Size(40, 19);
            this.metroLabel58.TabIndex = 105;
            this.metroLabel58.Text = "Baza:";
            // 
            // metroTextBox_serverT
            // 
            // 
            // 
            // 
            this.metroTextBox_serverT.CustomButton.Image = null;
            this.metroTextBox_serverT.CustomButton.Location = new System.Drawing.Point(142, 1);
            this.metroTextBox_serverT.CustomButton.Name = "";
            this.metroTextBox_serverT.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox_serverT.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_serverT.CustomButton.TabIndex = 1;
            this.metroTextBox_serverT.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_serverT.CustomButton.UseSelectable = true;
            this.metroTextBox_serverT.CustomButton.Visible = false;
            this.metroTextBox_serverT.Lines = new string[0];
            this.metroTextBox_serverT.Location = new System.Drawing.Point(163, 13);
            this.metroTextBox_serverT.MaxLength = 32767;
            this.metroTextBox_serverT.Name = "metroTextBox_serverT";
            this.metroTextBox_serverT.PasswordChar = '\0';
            this.metroTextBox_serverT.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_serverT.SelectedText = "";
            this.metroTextBox_serverT.SelectionLength = 0;
            this.metroTextBox_serverT.SelectionStart = 0;
            this.metroTextBox_serverT.ShortcutsEnabled = true;
            this.metroTextBox_serverT.Size = new System.Drawing.Size(164, 23);
            this.metroTextBox_serverT.TabIndex = 93;
            this.metroTextBox_serverT.UseSelectable = true;
            this.metroTextBox_serverT.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_serverT.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel59
            // 
            this.metroLabel59.AutoSize = true;
            this.metroLabel59.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel59.Location = new System.Drawing.Point(163, 176);
            this.metroLabel59.Name = "metroLabel59";
            this.metroLabel59.Size = new System.Drawing.Size(50, 19);
            this.metroLabel59.TabIndex = 97;
            this.metroLabel59.Text = "SFERA";
            // 
            // metroLabel60
            // 
            this.metroLabel60.AutoSize = true;
            this.metroLabel60.Location = new System.Drawing.Point(5, 102);
            this.metroLabel60.Name = "metroLabel60";
            this.metroLabel60.Size = new System.Drawing.Size(142, 19);
            this.metroLabel60.TabIndex = 101;
            this.metroLabel60.Text = "Autentykacja Windows:";
            // 
            // metroTextBox_sfera_userT
            // 
            // 
            // 
            // 
            this.metroTextBox_sfera_userT.CustomButton.Image = null;
            this.metroTextBox_sfera_userT.CustomButton.Location = new System.Drawing.Point(290, 1);
            this.metroTextBox_sfera_userT.CustomButton.Name = "";
            this.metroTextBox_sfera_userT.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox_sfera_userT.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_sfera_userT.CustomButton.TabIndex = 1;
            this.metroTextBox_sfera_userT.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_sfera_userT.CustomButton.UseSelectable = true;
            this.metroTextBox_sfera_userT.CustomButton.Visible = false;
            this.metroTextBox_sfera_userT.Lines = new string[0];
            this.metroTextBox_sfera_userT.Location = new System.Drawing.Point(163, 198);
            this.metroTextBox_sfera_userT.MaxLength = 32767;
            this.metroTextBox_sfera_userT.Name = "metroTextBox_sfera_userT";
            this.metroTextBox_sfera_userT.PasswordChar = '\0';
            this.metroTextBox_sfera_userT.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_sfera_userT.SelectedText = "";
            this.metroTextBox_sfera_userT.SelectionLength = 0;
            this.metroTextBox_sfera_userT.SelectionStart = 0;
            this.metroTextBox_sfera_userT.ShortcutsEnabled = true;
            this.metroTextBox_sfera_userT.Size = new System.Drawing.Size(312, 23);
            this.metroTextBox_sfera_userT.TabIndex = 104;
            this.metroTextBox_sfera_userT.UseSelectable = true;
            this.metroTextBox_sfera_userT.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_sfera_userT.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBox_sfera_passwordT
            // 
            // 
            // 
            // 
            this.metroTextBox_sfera_passwordT.CustomButton.Image = null;
            this.metroTextBox_sfera_passwordT.CustomButton.Location = new System.Drawing.Point(290, 1);
            this.metroTextBox_sfera_passwordT.CustomButton.Name = "";
            this.metroTextBox_sfera_passwordT.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox_sfera_passwordT.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_sfera_passwordT.CustomButton.TabIndex = 1;
            this.metroTextBox_sfera_passwordT.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_sfera_passwordT.CustomButton.UseSelectable = true;
            this.metroTextBox_sfera_passwordT.CustomButton.Visible = false;
            this.metroTextBox_sfera_passwordT.Lines = new string[0];
            this.metroTextBox_sfera_passwordT.Location = new System.Drawing.Point(163, 228);
            this.metroTextBox_sfera_passwordT.MaxLength = 32767;
            this.metroTextBox_sfera_passwordT.Name = "metroTextBox_sfera_passwordT";
            this.metroTextBox_sfera_passwordT.PasswordChar = '*';
            this.metroTextBox_sfera_passwordT.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_sfera_passwordT.SelectedText = "";
            this.metroTextBox_sfera_passwordT.SelectionLength = 0;
            this.metroTextBox_sfera_passwordT.SelectionStart = 0;
            this.metroTextBox_sfera_passwordT.ShortcutsEnabled = true;
            this.metroTextBox_sfera_passwordT.Size = new System.Drawing.Size(312, 23);
            this.metroTextBox_sfera_passwordT.TabIndex = 106;
            this.metroTextBox_sfera_passwordT.UseSelectable = true;
            this.metroTextBox_sfera_passwordT.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_sfera_passwordT.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel61
            // 
            this.metroLabel61.AutoSize = true;
            this.metroLabel61.Location = new System.Drawing.Point(71, 198);
            this.metroLabel61.Name = "metroLabel61";
            this.metroLabel61.Size = new System.Drawing.Size(76, 19);
            this.metroLabel61.TabIndex = 107;
            this.metroLabel61.Text = "Użytkownik:";
            // 
            // metroLabel62
            // 
            this.metroLabel62.AutoSize = true;
            this.metroLabel62.Location = new System.Drawing.Point(333, 17);
            this.metroLabel62.Name = "metroLabel62";
            this.metroLabel62.Size = new System.Drawing.Size(101, 19);
            this.metroLabel62.Style = MetroFramework.MetroColorStyle.Silver;
            this.metroLabel62.TabIndex = 102;
            this.metroLabel62.Text = "xx.xx.xx.xx,pppp";
            this.metroLabel62.UseStyleColors = true;
            // 
            // metroTabPage8
            // 
            this.metroTabPage8.Controls.Add(this.metroLabel63);
            this.metroTabPage8.Controls.Add(this.metroLabel64);
            this.metroTabPage8.Controls.Add(this.metroTextBox_passTB);
            this.metroTabPage8.Controls.Add(this.metroLabel65);
            this.metroTabPage8.Controls.Add(this.metroLabel66);
            this.metroTabPage8.Controls.Add(this.metroTextBox_userTB);
            this.metroTabPage8.Controls.Add(this.comboBox_DatabaseTB);
            this.metroTabPage8.Controls.Add(this.metroCheckBox_authTB);
            this.metroTabPage8.Controls.Add(this.metroLabel67);
            this.metroTabPage8.Controls.Add(this.metroTextBox_serverTB);
            this.metroTabPage8.Controls.Add(this.metroLabel68);
            this.metroTabPage8.Controls.Add(this.metroLabel69);
            this.metroTabPage8.Controls.Add(this.metroTextBox_sfera_userTB);
            this.metroTabPage8.Controls.Add(this.metroTextBox_sfera_passwordTB);
            this.metroTabPage8.Controls.Add(this.metroLabel70);
            this.metroTabPage8.Controls.Add(this.metroLabel71);
            this.metroTabPage8.HorizontalScrollbarBarColor = true;
            this.metroTabPage8.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage8.HorizontalScrollbarSize = 10;
            this.metroTabPage8.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage8.Name = "metroTabPage8";
            this.metroTabPage8.Size = new System.Drawing.Size(940, 337);
            this.metroTabPage8.TabIndex = 7;
            this.metroTabPage8.Text = "Elex Tkaniny Bis";
            this.metroTabPage8.VerticalScrollbarBarColor = true;
            this.metroTabPage8.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage8.VerticalScrollbarSize = 10;
            // 
            // metroLabel63
            // 
            this.metroLabel63.AutoSize = true;
            this.metroLabel63.Location = new System.Drawing.Point(96, 17);
            this.metroLabel63.Name = "metroLabel63";
            this.metroLabel63.Size = new System.Drawing.Size(51, 19);
            this.metroLabel63.TabIndex = 98;
            this.metroLabel63.Text = "Server:";
            // 
            // metroLabel64
            // 
            this.metroLabel64.AutoSize = true;
            this.metroLabel64.Location = new System.Drawing.Point(103, 72);
            this.metroLabel64.Name = "metroLabel64";
            this.metroLabel64.Size = new System.Drawing.Size(44, 19);
            this.metroLabel64.TabIndex = 100;
            this.metroLabel64.Text = "Hasło:";
            // 
            // metroTextBox_passTB
            // 
            // 
            // 
            // 
            this.metroTextBox_passTB.CustomButton.Image = null;
            this.metroTextBox_passTB.CustomButton.Location = new System.Drawing.Point(142, 1);
            this.metroTextBox_passTB.CustomButton.Name = "";
            this.metroTextBox_passTB.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox_passTB.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_passTB.CustomButton.TabIndex = 1;
            this.metroTextBox_passTB.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_passTB.CustomButton.UseSelectable = true;
            this.metroTextBox_passTB.CustomButton.Visible = false;
            this.metroTextBox_passTB.Lines = new string[0];
            this.metroTextBox_passTB.Location = new System.Drawing.Point(163, 72);
            this.metroTextBox_passTB.MaxLength = 32767;
            this.metroTextBox_passTB.Name = "metroTextBox_passTB";
            this.metroTextBox_passTB.PasswordChar = '*';
            this.metroTextBox_passTB.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_passTB.SelectedText = "";
            this.metroTextBox_passTB.SelectionLength = 0;
            this.metroTextBox_passTB.SelectionStart = 0;
            this.metroTextBox_passTB.ShortcutsEnabled = true;
            this.metroTextBox_passTB.Size = new System.Drawing.Size(164, 23);
            this.metroTextBox_passTB.TabIndex = 95;
            this.metroTextBox_passTB.UseSelectable = true;
            this.metroTextBox_passTB.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_passTB.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel65
            // 
            this.metroLabel65.AutoSize = true;
            this.metroLabel65.Location = new System.Drawing.Point(71, 42);
            this.metroLabel65.Name = "metroLabel65";
            this.metroLabel65.Size = new System.Drawing.Size(76, 19);
            this.metroLabel65.TabIndex = 99;
            this.metroLabel65.Text = "Użytkownik:";
            // 
            // metroLabel66
            // 
            this.metroLabel66.AutoSize = true;
            this.metroLabel66.Location = new System.Drawing.Point(103, 228);
            this.metroLabel66.Name = "metroLabel66";
            this.metroLabel66.Size = new System.Drawing.Size(44, 19);
            this.metroLabel66.TabIndex = 108;
            this.metroLabel66.Text = "Hasło:";
            // 
            // metroTextBox_userTB
            // 
            // 
            // 
            // 
            this.metroTextBox_userTB.CustomButton.Image = null;
            this.metroTextBox_userTB.CustomButton.Location = new System.Drawing.Point(142, 1);
            this.metroTextBox_userTB.CustomButton.Name = "";
            this.metroTextBox_userTB.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox_userTB.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_userTB.CustomButton.TabIndex = 1;
            this.metroTextBox_userTB.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_userTB.CustomButton.UseSelectable = true;
            this.metroTextBox_userTB.CustomButton.Visible = false;
            this.metroTextBox_userTB.Lines = new string[0];
            this.metroTextBox_userTB.Location = new System.Drawing.Point(163, 42);
            this.metroTextBox_userTB.MaxLength = 32767;
            this.metroTextBox_userTB.Name = "metroTextBox_userTB";
            this.metroTextBox_userTB.PasswordChar = '\0';
            this.metroTextBox_userTB.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_userTB.SelectedText = "";
            this.metroTextBox_userTB.SelectionLength = 0;
            this.metroTextBox_userTB.SelectionStart = 0;
            this.metroTextBox_userTB.ShortcutsEnabled = true;
            this.metroTextBox_userTB.Size = new System.Drawing.Size(164, 23);
            this.metroTextBox_userTB.TabIndex = 94;
            this.metroTextBox_userTB.UseSelectable = true;
            this.metroTextBox_userTB.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_userTB.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // comboBox_DatabaseTB
            // 
            this.comboBox_DatabaseTB.FormattingEnabled = true;
            this.comboBox_DatabaseTB.Location = new System.Drawing.Point(163, 137);
            this.comboBox_DatabaseTB.Name = "comboBox_DatabaseTB";
            this.comboBox_DatabaseTB.Size = new System.Drawing.Size(224, 21);
            this.comboBox_DatabaseTB.TabIndex = 103;
            this.comboBox_DatabaseTB.DropDown += new System.EventHandler(this.comboBox_DatabaseTB_DropDown);
            // 
            // metroCheckBox_authTB
            // 
            this.metroCheckBox_authTB.AutoSize = true;
            this.metroCheckBox_authTB.Location = new System.Drawing.Point(163, 106);
            this.metroCheckBox_authTB.Name = "metroCheckBox_authTB";
            this.metroCheckBox_authTB.Size = new System.Drawing.Size(26, 15);
            this.metroCheckBox_authTB.TabIndex = 96;
            this.metroCheckBox_authTB.Text = " ";
            this.metroCheckBox_authTB.UseSelectable = true;
            // 
            // metroLabel67
            // 
            this.metroLabel67.AutoSize = true;
            this.metroLabel67.Location = new System.Drawing.Point(107, 139);
            this.metroLabel67.Name = "metroLabel67";
            this.metroLabel67.Size = new System.Drawing.Size(40, 19);
            this.metroLabel67.TabIndex = 105;
            this.metroLabel67.Text = "Baza:";
            // 
            // metroTextBox_serverTB
            // 
            // 
            // 
            // 
            this.metroTextBox_serverTB.CustomButton.Image = null;
            this.metroTextBox_serverTB.CustomButton.Location = new System.Drawing.Point(142, 1);
            this.metroTextBox_serverTB.CustomButton.Name = "";
            this.metroTextBox_serverTB.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox_serverTB.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_serverTB.CustomButton.TabIndex = 1;
            this.metroTextBox_serverTB.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_serverTB.CustomButton.UseSelectable = true;
            this.metroTextBox_serverTB.CustomButton.Visible = false;
            this.metroTextBox_serverTB.Lines = new string[0];
            this.metroTextBox_serverTB.Location = new System.Drawing.Point(163, 13);
            this.metroTextBox_serverTB.MaxLength = 32767;
            this.metroTextBox_serverTB.Name = "metroTextBox_serverTB";
            this.metroTextBox_serverTB.PasswordChar = '\0';
            this.metroTextBox_serverTB.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_serverTB.SelectedText = "";
            this.metroTextBox_serverTB.SelectionLength = 0;
            this.metroTextBox_serverTB.SelectionStart = 0;
            this.metroTextBox_serverTB.ShortcutsEnabled = true;
            this.metroTextBox_serverTB.Size = new System.Drawing.Size(164, 23);
            this.metroTextBox_serverTB.TabIndex = 93;
            this.metroTextBox_serverTB.UseSelectable = true;
            this.metroTextBox_serverTB.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_serverTB.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel68
            // 
            this.metroLabel68.AutoSize = true;
            this.metroLabel68.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel68.Location = new System.Drawing.Point(163, 176);
            this.metroLabel68.Name = "metroLabel68";
            this.metroLabel68.Size = new System.Drawing.Size(50, 19);
            this.metroLabel68.TabIndex = 97;
            this.metroLabel68.Text = "SFERA";
            // 
            // metroLabel69
            // 
            this.metroLabel69.AutoSize = true;
            this.metroLabel69.Location = new System.Drawing.Point(5, 102);
            this.metroLabel69.Name = "metroLabel69";
            this.metroLabel69.Size = new System.Drawing.Size(142, 19);
            this.metroLabel69.TabIndex = 101;
            this.metroLabel69.Text = "Autentykacja Windows:";
            // 
            // metroTextBox_sfera_userTB
            // 
            // 
            // 
            // 
            this.metroTextBox_sfera_userTB.CustomButton.Image = null;
            this.metroTextBox_sfera_userTB.CustomButton.Location = new System.Drawing.Point(290, 1);
            this.metroTextBox_sfera_userTB.CustomButton.Name = "";
            this.metroTextBox_sfera_userTB.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox_sfera_userTB.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_sfera_userTB.CustomButton.TabIndex = 1;
            this.metroTextBox_sfera_userTB.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_sfera_userTB.CustomButton.UseSelectable = true;
            this.metroTextBox_sfera_userTB.CustomButton.Visible = false;
            this.metroTextBox_sfera_userTB.Lines = new string[0];
            this.metroTextBox_sfera_userTB.Location = new System.Drawing.Point(163, 198);
            this.metroTextBox_sfera_userTB.MaxLength = 32767;
            this.metroTextBox_sfera_userTB.Name = "metroTextBox_sfera_userTB";
            this.metroTextBox_sfera_userTB.PasswordChar = '\0';
            this.metroTextBox_sfera_userTB.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_sfera_userTB.SelectedText = "";
            this.metroTextBox_sfera_userTB.SelectionLength = 0;
            this.metroTextBox_sfera_userTB.SelectionStart = 0;
            this.metroTextBox_sfera_userTB.ShortcutsEnabled = true;
            this.metroTextBox_sfera_userTB.Size = new System.Drawing.Size(312, 23);
            this.metroTextBox_sfera_userTB.TabIndex = 104;
            this.metroTextBox_sfera_userTB.UseSelectable = true;
            this.metroTextBox_sfera_userTB.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_sfera_userTB.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBox_sfera_passwordTB
            // 
            // 
            // 
            // 
            this.metroTextBox_sfera_passwordTB.CustomButton.Image = null;
            this.metroTextBox_sfera_passwordTB.CustomButton.Location = new System.Drawing.Point(290, 1);
            this.metroTextBox_sfera_passwordTB.CustomButton.Name = "";
            this.metroTextBox_sfera_passwordTB.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox_sfera_passwordTB.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_sfera_passwordTB.CustomButton.TabIndex = 1;
            this.metroTextBox_sfera_passwordTB.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_sfera_passwordTB.CustomButton.UseSelectable = true;
            this.metroTextBox_sfera_passwordTB.CustomButton.Visible = false;
            this.metroTextBox_sfera_passwordTB.Lines = new string[0];
            this.metroTextBox_sfera_passwordTB.Location = new System.Drawing.Point(163, 228);
            this.metroTextBox_sfera_passwordTB.MaxLength = 32767;
            this.metroTextBox_sfera_passwordTB.Name = "metroTextBox_sfera_passwordTB";
            this.metroTextBox_sfera_passwordTB.PasswordChar = '*';
            this.metroTextBox_sfera_passwordTB.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_sfera_passwordTB.SelectedText = "";
            this.metroTextBox_sfera_passwordTB.SelectionLength = 0;
            this.metroTextBox_sfera_passwordTB.SelectionStart = 0;
            this.metroTextBox_sfera_passwordTB.ShortcutsEnabled = true;
            this.metroTextBox_sfera_passwordTB.Size = new System.Drawing.Size(312, 23);
            this.metroTextBox_sfera_passwordTB.TabIndex = 106;
            this.metroTextBox_sfera_passwordTB.UseSelectable = true;
            this.metroTextBox_sfera_passwordTB.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_sfera_passwordTB.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel70
            // 
            this.metroLabel70.AutoSize = true;
            this.metroLabel70.Location = new System.Drawing.Point(71, 198);
            this.metroLabel70.Name = "metroLabel70";
            this.metroLabel70.Size = new System.Drawing.Size(76, 19);
            this.metroLabel70.TabIndex = 107;
            this.metroLabel70.Text = "Użytkownik:";
            // 
            // metroLabel71
            // 
            this.metroLabel71.AutoSize = true;
            this.metroLabel71.Location = new System.Drawing.Point(333, 17);
            this.metroLabel71.Name = "metroLabel71";
            this.metroLabel71.Size = new System.Drawing.Size(101, 19);
            this.metroLabel71.Style = MetroFramework.MetroColorStyle.Silver;
            this.metroLabel71.TabIndex = 102;
            this.metroLabel71.Text = "xx.xx.xx.xx,pppp";
            this.metroLabel71.UseStyleColors = true;
            // 
            // Konfiguracja
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.metroTabControl_databaseW);
            this.Controls.Add(this.metroButtonFolderBrowser);
            this.Controls.Add(this.metroTileZapisz);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.metroTextBoxFolderEpp);
            this.Name = "Konfiguracja";
            this.Size = new System.Drawing.Size(954, 584);
            this.Load += new System.EventHandler(this.Konfiguracja_Load);
            this.metroTabControl_databaseW.ResumeLayout(false);
            this.metroTabPage1.ResumeLayout(false);
            this.metroTabPage1.PerformLayout();
            this.metroTabPage2.ResumeLayout(false);
            this.metroTabPage2.PerformLayout();
            this.metroTabPage3.ResumeLayout(false);
            this.metroTabPage3.PerformLayout();
            this.metroTabPage4.ResumeLayout(false);
            this.metroTabPage4.PerformLayout();
            this.metroTabPage5.ResumeLayout(false);
            this.metroTabPage5.PerformLayout();
            this.metroTabPage6.ResumeLayout(false);
            this.metroTabPage6.PerformLayout();
            this.metroTabPage7.ResumeLayout(false);
            this.metroTabPage7.PerformLayout();
            this.metroTabPage8.ResumeLayout(false);
            this.metroTabPage8.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel metroLabel19;
        private System.Windows.Forms.ComboBox comboBox_DatabaseKM;
        private MetroFramework.Controls.MetroLabel metroLabel25;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroTextBox metroTextBox_sfera_passwordKM;
        private MetroFramework.Controls.MetroLabel metroLabel17;
        private MetroFramework.Controls.MetroLabel metroLabel20;
        private MetroFramework.Controls.MetroTextBox metroTextBox_sfera_userKM;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroTextBox metroTextBox_serverKM;
        private MetroFramework.Controls.MetroCheckBox metroCheckBox_authKM;
        private MetroFramework.Controls.MetroTextBox metroTextBox_userKM;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox metroTextBox_passKM;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroTile metroTileZapisz;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox metroTextBoxFolderEpp;
        private MetroFramework.Controls.MetroButton metroButtonFolderBrowser;
        private MetroFramework.Controls.MetroTabControl metroTabControl_databaseW;
        private MetroFramework.Controls.MetroTabPage metroTabPage1;
        private MetroFramework.Controls.MetroTabPage metroTabPage2;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroTextBox metroTextBox_passK;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroTextBox metroTextBox_userK;
        private System.Windows.Forms.ComboBox comboBox_DatabaseK;
        private MetroFramework.Controls.MetroCheckBox metroCheckBox_authK;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private MetroFramework.Controls.MetroTextBox metroTextBox_serverK;
        private MetroFramework.Controls.MetroLabel metroLabel13;
        private MetroFramework.Controls.MetroLabel metroLabel14;
        private MetroFramework.Controls.MetroTextBox metroTextBox_sfera_userK;
        private MetroFramework.Controls.MetroTextBox metroTextBox_sfera_passwordK;
        private MetroFramework.Controls.MetroLabel metroLabel15;
        private MetroFramework.Controls.MetroLabel metroLabel16;
        private MetroFramework.Controls.MetroTabPage metroTabPage3;
        private MetroFramework.Controls.MetroLabel metroLabel18;
        private MetroFramework.Controls.MetroLabel metroLabel21;
        private MetroFramework.Controls.MetroTextBox metroTextBox_passKB;
        private MetroFramework.Controls.MetroLabel metroLabel22;
        private MetroFramework.Controls.MetroLabel metroLabel23;
        private MetroFramework.Controls.MetroTextBox metroTextBox_userKB;
        private System.Windows.Forms.ComboBox comboBox_DatabaseKB;
        private MetroFramework.Controls.MetroCheckBox metroCheckBox_authKB;
        private MetroFramework.Controls.MetroLabel metroLabel24;
        private MetroFramework.Controls.MetroTextBox metroTextBox_serverKB;
        private MetroFramework.Controls.MetroLabel metroLabel26;
        private MetroFramework.Controls.MetroLabel metroLabel27;
        private MetroFramework.Controls.MetroTextBox metroTextBox_sfera_userKB;
        private MetroFramework.Controls.MetroTextBox metroTextBox_sfera_passwordKB;
        private MetroFramework.Controls.MetroLabel metroLabel28;
        private MetroFramework.Controls.MetroLabel metroLabel29;
        private MetroFramework.Controls.MetroTabPage metroTabPage4;
        private MetroFramework.Controls.MetroLabel metroLabel30;
        private MetroFramework.Controls.MetroLabel metroLabel31;
        private MetroFramework.Controls.MetroTextBox metroTextBox_passKT;
        private MetroFramework.Controls.MetroLabel metroLabel32;
        private MetroFramework.Controls.MetroLabel metroLabel33;
        private MetroFramework.Controls.MetroTextBox metroTextBox_userKT;
        private System.Windows.Forms.ComboBox comboBox_DatabaseKT;
        private MetroFramework.Controls.MetroCheckBox metroCheckBox_authKT;
        private MetroFramework.Controls.MetroLabel metroLabel34;
        private MetroFramework.Controls.MetroTextBox metroTextBox_serverKT;
        private MetroFramework.Controls.MetroLabel metroLabel35;
        private MetroFramework.Controls.MetroLabel metroLabel36;
        private MetroFramework.Controls.MetroTextBox metroTextBox_sfera_userKT;
        private MetroFramework.Controls.MetroTextBox metroTextBox_sfera_passwordKT;
        private MetroFramework.Controls.MetroLabel metroLabel37;
        private MetroFramework.Controls.MetroLabel metroLabel38;
        private MetroFramework.Controls.MetroTabPage metroTabPage5;
        private MetroFramework.Controls.MetroLabel metroLabel39;
        private MetroFramework.Controls.MetroLabel metroLabel40;
        private MetroFramework.Controls.MetroTextBox metroTextBox_passWB;
        private MetroFramework.Controls.MetroLabel metroLabel41;
        private MetroFramework.Controls.MetroLabel metroLabel42;
        private MetroFramework.Controls.MetroTextBox metroTextBox_userWB;
        private System.Windows.Forms.ComboBox comboBox_DatabaseWB;
        private MetroFramework.Controls.MetroCheckBox metroCheckBox_authWB;
        private MetroFramework.Controls.MetroLabel metroLabel43;
        private MetroFramework.Controls.MetroTextBox metroTextBox_serverWB;
        private MetroFramework.Controls.MetroLabel metroLabel44;
        private MetroFramework.Controls.MetroLabel metroLabel45;
        private MetroFramework.Controls.MetroTextBox metroTextBox_sfera_userWB;
        private MetroFramework.Controls.MetroTextBox metroTextBox_sfera_passwordWB;
        private MetroFramework.Controls.MetroLabel metroLabel46;
        private MetroFramework.Controls.MetroLabel metroLabel47;
        private MetroFramework.Controls.MetroTabPage metroTabPage6;
        private MetroFramework.Controls.MetroLabel metroLabel48;
        private MetroFramework.Controls.MetroLabel metroLabel49;
        private MetroFramework.Controls.MetroTextBox metroTextBox_passW;
        private MetroFramework.Controls.MetroLabel metroLabel50;
        private MetroFramework.Controls.MetroTextBox metroTextBox_userW;
        private System.Windows.Forms.ComboBox comboBox_DatabaseW;
        private MetroFramework.Controls.MetroCheckBox metroCheckBox_authW;
        private MetroFramework.Controls.MetroLabel metroLabel52;
        private MetroFramework.Controls.MetroTextBox metroTextBox_serverW;
        private MetroFramework.Controls.MetroLabel metroLabel54;
        private MetroFramework.Controls.MetroLabel metroLabel56;
        private MetroFramework.Controls.MetroTabPage metroTabPage7;
        private MetroFramework.Controls.MetroTabPage metroTabPage8;
        private MetroFramework.Controls.MetroLabel metroLabel51;
        private MetroFramework.Controls.MetroLabel metroLabel53;
        private MetroFramework.Controls.MetroTextBox metroTextBox_passT;
        private MetroFramework.Controls.MetroLabel metroLabel55;
        private MetroFramework.Controls.MetroLabel metroLabel57;
        private MetroFramework.Controls.MetroTextBox metroTextBox_userT;
        private System.Windows.Forms.ComboBox comboBox_DatabaseT;
        private MetroFramework.Controls.MetroCheckBox metroCheckBox_authT;
        private MetroFramework.Controls.MetroLabel metroLabel58;
        private MetroFramework.Controls.MetroTextBox metroTextBox_serverT;
        private MetroFramework.Controls.MetroLabel metroLabel59;
        private MetroFramework.Controls.MetroLabel metroLabel60;
        private MetroFramework.Controls.MetroTextBox metroTextBox_sfera_userT;
        private MetroFramework.Controls.MetroTextBox metroTextBox_sfera_passwordT;
        private MetroFramework.Controls.MetroLabel metroLabel61;
        private MetroFramework.Controls.MetroLabel metroLabel62;
        private MetroFramework.Controls.MetroLabel metroLabel63;
        private MetroFramework.Controls.MetroLabel metroLabel64;
        private MetroFramework.Controls.MetroTextBox metroTextBox_passTB;
        private MetroFramework.Controls.MetroLabel metroLabel65;
        private MetroFramework.Controls.MetroLabel metroLabel66;
        private MetroFramework.Controls.MetroTextBox metroTextBox_userTB;
        private System.Windows.Forms.ComboBox comboBox_DatabaseTB;
        private MetroFramework.Controls.MetroCheckBox metroCheckBox_authTB;
        private MetroFramework.Controls.MetroLabel metroLabel67;
        private MetroFramework.Controls.MetroTextBox metroTextBox_serverTB;
        private MetroFramework.Controls.MetroLabel metroLabel68;
        private MetroFramework.Controls.MetroLabel metroLabel69;
        private MetroFramework.Controls.MetroTextBox metroTextBox_sfera_userTB;
        private MetroFramework.Controls.MetroTextBox metroTextBox_sfera_passwordTB;
        private MetroFramework.Controls.MetroLabel metroLabel70;
        private MetroFramework.Controls.MetroLabel metroLabel71;
    }
}
