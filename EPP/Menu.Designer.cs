﻿namespace EPP
{
    partial class Menu
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroTileExit = new MetroFramework.Controls.MetroTile();
            this.metroTileGenerujEPP = new MetroFramework.Controls.MetroTile();
            this.metroTileInfo = new MetroFramework.Controls.MetroTile();
            this.metroTileConfig = new MetroFramework.Controls.MetroTile();
            this.metroTile_fv = new MetroFramework.Controls.MetroTile();
            this.SuspendLayout();
            // 
            // metroTileExit
            // 
            this.metroTileExit.ActiveControl = null;
            this.metroTileExit.Location = new System.Drawing.Point(841, 491);
            this.metroTileExit.Name = "metroTileExit";
            this.metroTileExit.Size = new System.Drawing.Size(110, 90);
            this.metroTileExit.Style = MetroFramework.MetroColorStyle.Red;
            this.metroTileExit.TabIndex = 1;
            this.metroTileExit.Text = "Zamknij";
            this.metroTileExit.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold;
            this.metroTileExit.UseSelectable = true;
            this.metroTileExit.Click += new System.EventHandler(this.metroTileExit_Click);
            // 
            // metroTileGenerujEPP
            // 
            this.metroTileGenerujEPP.ActiveControl = null;
            this.metroTileGenerujEPP.Location = new System.Drawing.Point(3, 3);
            this.metroTileGenerujEPP.Name = "metroTileGenerujEPP";
            this.metroTileGenerujEPP.Size = new System.Drawing.Size(226, 90);
            this.metroTileGenerujEPP.TabIndex = 3;
            this.metroTileGenerujEPP.Text = "Generuj plik EPP";
            this.metroTileGenerujEPP.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold;
            this.metroTileGenerujEPP.UseSelectable = true;
            this.metroTileGenerujEPP.Click += new System.EventHandler(this.metroTileGenerujEPP_Click);
            // 
            // metroTileInfo
            // 
            this.metroTileInfo.ActiveControl = null;
            this.metroTileInfo.Location = new System.Drawing.Point(840, 3);
            this.metroTileInfo.Name = "metroTileInfo";
            this.metroTileInfo.Size = new System.Drawing.Size(110, 90);
            this.metroTileInfo.TabIndex = 4;
            this.metroTileInfo.Text = "Info";
            this.metroTileInfo.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold;
            this.metroTileInfo.UseSelectable = true;
            this.metroTileInfo.Click += new System.EventHandler(this.metroTileInfo_Click);
            // 
            // metroTileConfig
            // 
            this.metroTileConfig.ActiveControl = null;
            this.metroTileConfig.Location = new System.Drawing.Point(724, 3);
            this.metroTileConfig.Name = "metroTileConfig";
            this.metroTileConfig.Size = new System.Drawing.Size(110, 90);
            this.metroTileConfig.TabIndex = 5;
            this.metroTileConfig.Text = "Konfiguracja";
            this.metroTileConfig.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold;
            this.metroTileConfig.UseSelectable = true;
            this.metroTileConfig.Click += new System.EventHandler(this.metroTileConfig_Click);
            // 
            // metroTile_fv
            // 
            this.metroTile_fv.ActiveControl = null;
            this.metroTile_fv.Location = new System.Drawing.Point(235, 3);
            this.metroTile_fv.Name = "metroTile_fv";
            this.metroTile_fv.Size = new System.Drawing.Size(226, 90);
            this.metroTile_fv.TabIndex = 6;
            this.metroTile_fv.Text = "Generuj Fakturę";
            this.metroTile_fv.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold;
            this.metroTile_fv.UseSelectable = true;
            this.metroTile_fv.Click += new System.EventHandler(this.metroTile_fv_Click);
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.metroTile_fv);
            this.Controls.Add(this.metroTileGenerujEPP);
            this.Controls.Add(this.metroTileInfo);
            this.Controls.Add(this.metroTileConfig);
            this.Controls.Add(this.metroTileExit);
            this.Name = "Menu";
            this.Size = new System.Drawing.Size(954, 584);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTile metroTileExit;
        private MetroFramework.Controls.MetroTile metroTileGenerujEPP;
        private MetroFramework.Controls.MetroTile metroTileInfo;
        private MetroFramework.Controls.MetroTile metroTileConfig;
        private MetroFramework.Controls.MetroTile metroTile_fv;
    }
}
