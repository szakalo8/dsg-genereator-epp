﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EPP.VO
{
    public class DokumentVO
    {
        public DokumentVO(int dok_Id, string dok_NrPelny)
        {
            this.dok_Id = dok_Id;
            this.dok_NrPelny = dok_NrPelny;
        }

        private int dok_Id { get; set; }
        private string dok_NrPelny { get; set; }
    }
}
