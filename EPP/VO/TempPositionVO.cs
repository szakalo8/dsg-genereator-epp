﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EPP.VO
{
    public class TempPositionVO
    {
        //numer dokumentu 
        public string dok_NrPelny { get; set; }
        //data wystawienia dokumentu
        public DateTime dok_DataWyst { get; set; }
        // kod dostawy
        public string KodDostawy { get; set; }
        // nazwa towaru
        public string NazwaTowaru { get; set; }
        // ilość towaru
        public decimal Ilosc { get; set; }
        // cena jednostkowa

        public decimal CenaJednostkowa { get; set; }
        // wartość netto
        public decimal WartoscNetto { get; set; }
        // wartość brutto
        public decimal WartoscBrutto { get; set; }
        // nip odbiorcy
        public string NipOdbiorcy { get; set; }
        // nazwa odbiorcy
        public string NazwaOdbiorcy { get; set; }
        public decimal CenaDocelowaNetto { get; internal set; }
        public decimal WartoscDocelowaNetto { get; internal set; }
        public decimal WartoscDocelowaBrutto { get; internal set; }
    }
}
