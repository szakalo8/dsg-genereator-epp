﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EPP.VO
{
    public class MagazynVO
    {
        public int mag_Id { get; set; }
        public string mag_Symbol { get; set; }
        public string mag_Nazwa { get; set; }

        public MagazynVO(int mag_Id, string mag_Symbol, string mag_Nazwa)
        {
            this.mag_Id = mag_Id;
            this.mag_Symbol = mag_Symbol;
            this.mag_Nazwa = mag_Nazwa;
        }


    }
}
