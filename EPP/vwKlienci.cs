//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EPP
{
    using System;
    using System.Collections.Generic;
    
    public partial class vwKlienci
    {
        public int adr_Id { get; set; }
        public int kh_Id { get; set; }
        public bool kh_Zablokowany { get; set; }
        public int kh_Typ { get; set; }
        public int kh_Rodzaj { get; set; }
        public string kh_Symbol { get; set; }
        public string adr_Nazwa { get; set; }
        public string adr_NazwaPelna { get; set; }
        public Nullable<int> IdFirmy { get; set; }
        public string Firma { get; set; }
        public string adr_Miejscowosc { get; set; }
        public string adr_NIP { get; set; }
        public string kh_REGON { get; set; }
        public string adr_Kod { get; set; }
        public string adr_Adres { get; set; }
        public string adr_Telefon { get; set; }
        public string kh_EMail { get; set; }
        public string kh_GaduGadu { get; set; }
        public string kh_Skype { get; set; }
        public string kh_WWW { get; set; }
        public string kh_Pracownik { get; set; }
        public string pk_Telefon { get; set; }
        public string pk_Email { get; set; }
        public string kh_Uwagi { get; set; }
        public Nullable<bool> kh_ZgodaDO { get; set; }
        public Nullable<bool> kh_ZgodaMark { get; set; }
        public Nullable<bool> kh_ZgodaEMail { get; set; }
        public Nullable<int> adr_IdPanstwo { get; set; }
        public Nullable<int> adr_IdWojewodztwo { get; set; }
        public Nullable<int> pk_IdKh { get; set; }
        public Nullable<int> kh_IdGrupa { get; set; }
        public Nullable<int> kh_IdRodzajKontaktu { get; set; }
        public Nullable<int> kh_IdPozyskany { get; set; }
        public Nullable<int> kh_IdBranza { get; set; }
        public Nullable<int> kh_IdRegion { get; set; }
        public Nullable<int> kh_IdDzial { get; set; }
        public string kh_Stanowisko { get; set; }
        public Nullable<int> kh_LiczbaPrac { get; set; }
        public int kh_Potencjalny { get; set; }
        public int kh_Jednorazowy { get; set; }
        public Nullable<int> kh_IdOpiekun { get; set; }
        public Nullable<int> kh_IdDodal { get; set; }
        public Nullable<int> kh_IdZmienil { get; set; }
        public Nullable<System.DateTime> kh_DataDodania { get; set; }
        public Nullable<System.DateTime> kh_DataZmiany { get; set; }
        public Nullable<System.DateTime> kh_DataOkolicznosciowa { get; set; }
        public Nullable<bool> pk_Podstaw { get; set; }
        public string kh_Pole1 { get; set; }
        public string kh_Pole2 { get; set; }
        public string kh_Pole3 { get; set; }
        public string kh_Pole4 { get; set; }
        public string kh_Pole5 { get; set; }
        public string kh_Pole6 { get; set; }
        public string kh_Pole7 { get; set; }
        public string kh_Pole8 { get; set; }
        public string adr_Faks { get; set; }
        public string kh_PESEL { get; set; }
    }
}
