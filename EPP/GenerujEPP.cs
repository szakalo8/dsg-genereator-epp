﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Text;
using InsERT;
using MetroFramework;
using System.Collections.Generic;
using System.Linq;


namespace EPP
{
    public partial class GenerujEPP : MetroFramework.Controls.MetroUserControl
    {
        /// <summary>
        /// Obiekt Subiekt GT
        /// </summary>
        InsERT.Subiekt _sgt;

        public enum Firma
        {
            Konekt,
            KonektBis,
            KonektTkaniny,
            Wektor,
            WektorBis,
            ElexTkaniny,
            ElexTkaninyBis,
            ElexBis,
            Elex
        }

        Firma firma;

        public GenerujEPP()
        {
            InitializeComponent();
        }

        /// <summary>
        ///     Ustanawia połączenie do Sfery
        /// </summary>
        private void SferaConn()
        {
            if (firma == Firma.Wektor)
            {
                var gt = new GT
                {
                    Produkt = ProduktEnum.gtaProduktSubiekt,
                    Autentykacja = AutentykacjaEnum.gtaAutentykacjaMieszana,
                    Serwer = Properties.Settings.Default.serverKM.Trim(),
                    Baza = Properties.Settings.Default.databaseKM.Trim(),
                    Uzytkownik = Properties.Settings.Default.userKM.Trim(),
                    UzytkownikHaslo = Properties.Settings.Default.passwordKM.Trim(),
                    Operator = Properties.Settings.Default.sferaUserKM.Trim(),
                    OperatorHaslo = Properties.Settings.Default.sferaPasswordKM
                };
                _sgt = (InsERT.Subiekt)gt.Uruchom((int)UruchomDopasujEnum.gtaUruchomDopasuj,
                    (int)UruchomEnum.gtaUruchomNowy | (int)UruchomEnum.gtaUruchomWTle);
            }
            else if (firma == Firma.Konekt)
            {
                var gt = new GT
                {
                    Produkt = ProduktEnum.gtaProduktSubiekt,
                    Autentykacja = AutentykacjaEnum.gtaAutentykacjaMieszana,
                    Serwer = Properties.Settings.Default.serverK.Trim(),
                    Baza = Properties.Settings.Default.databaseK.Trim(),
                    Uzytkownik = Properties.Settings.Default.userK.Trim(),
                    UzytkownikHaslo = Properties.Settings.Default.passwordK.Trim(),
                    Operator = Properties.Settings.Default.sferaUserK.Trim(),
                    OperatorHaslo = Properties.Settings.Default.sferaPasswordK
                };
                _sgt = (InsERT.Subiekt)gt.Uruchom((int)UruchomDopasujEnum.gtaUruchomDopasuj,
                    (int)UruchomEnum.gtaUruchomNowy | (int)UruchomEnum.gtaUruchomWTle);
            }
            else if (firma == Firma.KonektBis)
            {
                var gt = new GT
                {
                    Produkt = ProduktEnum.gtaProduktSubiekt,
                    Autentykacja = AutentykacjaEnum.gtaAutentykacjaMieszana,
                    Serwer = Properties.Settings.Default.serverKB.Trim(),
                    Baza = Properties.Settings.Default.databaseKB.Trim(),
                    Uzytkownik = Properties.Settings.Default.userKB.Trim(),
                    UzytkownikHaslo = Properties.Settings.Default.passwordKB.Trim(),
                    Operator = Properties.Settings.Default.sferaUserKB.Trim(),
                    OperatorHaslo = Properties.Settings.Default.sferaPasswordKB
                };
                _sgt = (InsERT.Subiekt)gt.Uruchom((int)UruchomDopasujEnum.gtaUruchomDopasuj,
                    (int)UruchomEnum.gtaUruchomNowy | (int)UruchomEnum.gtaUruchomWTle);
            }
            else if (firma == Firma.KonektTkaniny)
            {
                var gt = new GT
                {
                    Produkt = ProduktEnum.gtaProduktSubiekt,
                    Autentykacja = AutentykacjaEnum.gtaAutentykacjaMieszana,
                    Serwer = Properties.Settings.Default.serverKT.Trim(),
                    Baza = Properties.Settings.Default.databaseKT.Trim(),
                    Uzytkownik = Properties.Settings.Default.userKT.Trim(),
                    UzytkownikHaslo = Properties.Settings.Default.passwordKT.Trim(),
                    Operator = Properties.Settings.Default.sferaUserKT.Trim(),
                    OperatorHaslo = Properties.Settings.Default.sferaPasswordKT
                };
                _sgt = (InsERT.Subiekt)gt.Uruchom((int)UruchomDopasujEnum.gtaUruchomDopasuj,
                    (int)UruchomEnum.gtaUruchomNowy | (int)UruchomEnum.gtaUruchomWTle);
            }
            else if (firma == Firma.WektorBis)
            {
                var gt = new GT
                {
                    Produkt = ProduktEnum.gtaProduktSubiekt,
                    Autentykacja = AutentykacjaEnum.gtaAutentykacjaMieszana,
                    Serwer = Properties.Settings.Default.serverKM.Trim(),
                    Baza = Properties.Settings.Default.databaseKM.Trim(),
                    Uzytkownik = Properties.Settings.Default.userKM.Trim(),
                    UzytkownikHaslo = Properties.Settings.Default.passwordKM.Trim(),
                    Operator = Properties.Settings.Default.sferaUserKM.Trim(),
                    OperatorHaslo = Properties.Settings.Default.sferaPasswordKM
                };
                _sgt = (InsERT.Subiekt)gt.Uruchom((int)UruchomDopasujEnum.gtaUruchomDopasuj,
                    (int)UruchomEnum.gtaUruchomNowy | (int)UruchomEnum.gtaUruchomWTle);
            }
            else if (firma == Firma.ElexTkaniny)
            {
                var gt = new GT
                {
                    Produkt = ProduktEnum.gtaProduktSubiekt,
                    Autentykacja = AutentykacjaEnum.gtaAutentykacjaMieszana,
                    Serwer = Properties.Settings.Default.serverT.Trim(),
                    Baza = Properties.Settings.Default.databaseT.Trim(),
                    Uzytkownik = Properties.Settings.Default.userT.Trim(),
                    UzytkownikHaslo = Properties.Settings.Default.passwordT.Trim(),
                    Operator = Properties.Settings.Default.sferaUserT.Trim(),
                    OperatorHaslo = Properties.Settings.Default.sferaPasswordT
                };
                _sgt = (InsERT.Subiekt)gt.Uruchom((int)UruchomDopasujEnum.gtaUruchomDopasuj,
                    (int)UruchomEnum.gtaUruchomNowy | (int)UruchomEnum.gtaUruchomWTle);
            }
            else if (firma == Firma.ElexTkaninyBis)
            {
                var gt = new GT
                {
                    Produkt = ProduktEnum.gtaProduktSubiekt,
                    Autentykacja = AutentykacjaEnum.gtaAutentykacjaMieszana,
                    Serwer = Properties.Settings.Default.serverTB.Trim(),
                    Baza = Properties.Settings.Default.databaseTB.Trim(),
                    Uzytkownik = Properties.Settings.Default.userTB.Trim(),
                    UzytkownikHaslo = Properties.Settings.Default.passwordTB.Trim(),
                    Operator = Properties.Settings.Default.sferaUserTB.Trim(),
                    OperatorHaslo = Properties.Settings.Default.sferaPasswordTB
                };
                _sgt = (InsERT.Subiekt)gt.Uruchom((int)UruchomDopasujEnum.gtaUruchomDopasuj,
                    (int)UruchomEnum.gtaUruchomNowy | (int)UruchomEnum.gtaUruchomWTle);
            }
        }

        private void metroButtonGenerujEPP_Click(object sender, EventArgs e)
        {
            string kolekcjaID = "";
            metroProgressSpinner1.Spinning = true;
            bool? zakoncz = null;
            try
            {
                zakoncz = false;
                //string kolekcjaID = "";

                if (metroGridDokumentyWZ.RowCount != 0 && metroGridDokumentyWZ.SelectedRows.Count != 0)
                {
                    kolekcjaID = GetIdFromList();
                }
                else
                {
                    MetroMessageBox.Show(this, "Brak wybranych dokumentów");
                    return;
                }

                SferaConn();


                string nazwaPliku = DateTime.Now.ToString("yyyyMMddHHmmss") + ".epp";

                if (Properties.Settings.Default.katalogPliku == "")
                {
                    MetroMessageBox.Show(this, "Brak ścieżki zapisu pliku!");
                    return;
                }

                string filePath = Properties.Settings.Default.katalogPliku + "\\" + nazwaPliku;


                _sgt.MagazynId = ((sl_Magazyn)metroComboBoxMagazyny.SelectedItem).mag_Id;
                var oDokKol = _sgt.SuDokumentyManager.OtworzKolekcje($"dok_id IN ({kolekcjaID})", "");

                oDokKol.ZapiszDoPliku(filePath, false, "");

                oDokKol = null;

                PodmieńCeny(filePath);

                if (firma == Firma.Wektor)
                {
                    PodmieńKtr(filePath, firma);
                }

                if(firma == Firma.WektorBis)
                {
                    PodmieńKtr(filePath, firma);
                }

                MetroMessageBox.Show(this, "Wygenerowano plik EPP", "Sukces", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                metroTextBox_Logi.Text = "";
                metroTextBox_Logi.Text = ex.Message + Environment.NewLine + ex.Source + Environment.NewLine + ex.StackTrace + Environment.NewLine + kolekcjaID;
                metroProgressSpinner1.Spinning = false;
                MetroMessageBox.Show(this, "Błąd podczas generowanie pliku EPP", "Bład", MessageBoxButtons.OK,
                    MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
            finally
            {
                metroProgressSpinner1.Spinning = false;
                if (zakoncz == false)
                {
                    _sgt?.Zakoncz();
                }
            }

        }


        /// <summary>
        /// Podmienia kontrahenta w pliku epp
        /// </summary>
        /// <param name="filePath">ścieżka do pliku</param>
        /// <param name="firma">Nazwa firmy</param>
        private void PodmieńKtr(string filePath, Firma firma)
        {
            bool war1 = false; // Jeżeli w linni jest NAGLOWEK to true
            bool war2 = false; // Jezeli w lini po NAGLOWEK jest WZ
            string nrDok = "";

            string[] lines = System.IO.File.ReadAllLines(filePath, Encoding.Default);

            int count = lines.Length;

            for (int i = 0; i < count; i++)
            {

                if (war1 == false)
                {
                    if (lines[i].Trim() == "[NAGLOWEK]") //Sprawdza czy w lini jest NAGLOWEK
                    {
                        war1 = true;
                    }
                }
                else if (war1 == true)
                {
                    string nagLine = lines[i];
                    string typDok = nagLine.Substring(1, 2);
                    if (typDok == "WZ")
                    {
                        ///Tutaj trzeba juz czytać linie, znaleźć cene netto i ją zamienić
                        string pozLine = lines[i];
                        string newLineToInsert = GetNewLineWithWektorKtrToInsert(pozLine, firma);

                        lines[i] = newLineToInsert;

                        war1 = false;
                    }
                    else
                    {
                        war1 = false;
                        war2 = false;
                    }
                }

            }

            File.WriteAllLines(filePath, lines, Encoding.Default);
        }

        /// <summary>
        /// Podmienia kontrahenta w linni nagłówka
        /// </summary>
        /// <param name="pozLine">string linni</param>
        /// <param name="firma">nazwa firmy</param>
        /// <returns></returns>
        private string GetNewLineWithWektorKtrToInsert(string pozLine, Firma firma)
        {
            string newPozLine = "";

            string[] chars = new string[] { "," };

            // List<string> parametryPoz = pozLine.Split(new char[] { ',' }).ToList();

            List<string> parametryPoz = pozLine.Split(chars, StringSplitOptions.None).ToList();

            vwKlienci ktr = DA.GetKontrahent.GetKtrFromSubiekt(firma);

            parametryPoz[11] = $"\"{ktr.kh_Symbol}\"";        //symbol
         // parametryPoz[12] = $"\"{ktr.adr_Nazwa}\"\"";      //nazwa
            parametryPoz[12] = $"\"{ktr.adr_Nazwa}\"";      //nazwa
            parametryPoz[13] = $"\"{ktr.adr_NazwaPelna}\"";   //nazwa pelna
            parametryPoz[14] = $"\"{ktr.adr_Miejscowosc}\"";  //miasto
            parametryPoz[15] = $"\"{ktr.adr_Kod}\"";          //kod
            parametryPoz[16] = $"\"{ktr.adr_Adres}\"";        //ulica, adres
            parametryPoz[17] = $"\"{ktr.adr_NIP}\"";          //NIP

            foreach (var i in parametryPoz)
            {
                newPozLine += i + ",";
            }

            return newPozLine.Remove(newPozLine.Length - 1);
        }


        /// <summary>
        /// Podmiania ceny w pliku epp w pozycjach
        /// </summary>
        /// <param name="filePath">sciezka do pliku</param>
        private void PodmieńCeny(string filePath)
        {
            bool war1 = false; // Jeżeli w linni jest NAGLOWEK to true
            bool war2 = false; // Jezeli w lini po NAGLOWEK jest WZ
            bool war3 = false; // Jeżeli w którejs lini po NAGLOWEK jest ZAWARTOSC
            string nrDok = "";
            int idDok = 0;

            string[] lines = System.IO.File.ReadAllLines(filePath, Encoding.Default);

            int count = lines.Length;

                for (int i = 0; i < count; i++)
                {
                    if (war3 == false)
                    {
                        if (war2 == false)
                        {
                            if (war1 == false)
                            {
                                if (lines[i].Trim() == "[NAGLOWEK]") //Sprawdza czy w lini jest NAGLOWEK
                                {
                                    war1 = true;
                                }
                            }
                            else if (war1 == true)
                            {
                                string nagLine = lines[i];
                                string typDok = nagLine.Substring(1, 2);
                                if (typDok == "WZ")
                                {
                                    nrDok = GetNrDokFromLine(nagLine);
                                    idDok = DA.GetIdDok.GetIdDokFromSub(nrDok);
                                    war2 = true;
                                }
                                else
                                {
                                    war1 = false;
                                    war2 = false;
                                }
                            }
                        }
                        else if (war2 == true) //To się wykonuje jezeli w poprzedniej lini było WZ
                        {
                            if (lines[i].Trim() == "[ZAWARTOSC]")
                            {
                                war3 = true;
                            }
                        }
                    }
                    else if (war3 == true)
                    {
                        if (lines[i] == "")
                        {
                            war1 = false;
                            war2 = false;
                            war3 = false;
                            continue;
                        }
                        else
                        {
                            ///Tutaj trzeba juz czytać linie, znaleźć cene netto i ją zamienić
                            string pozLine = lines[i];
                            string newLineToInsert = GetNewLineToInsert(pozLine, nrDok, idDok);
                            lines[i] = newLineToInsert;

                        }
                    }

                }

            System.IO.File.WriteAllLines(filePath, lines, Encoding.Default);
        }


        /// <summary>
        /// Wyciąga numer dokumentu WZ z linii
        /// </summary>
        /// <param name="nagLine">linia nagłówka</param>
        /// <returns>numer dokumentu WZ</returns>
        private string GetNrDokFromLine(string nagLine)
        {
            string numerDok;

            List<string> result = nagLine.Split(new char[] { ',' }).ToList();

            numerDok = result[0].ToString().Trim('"') + " " + result[6].ToString().Trim('"');

            return numerDok;
        }

        /// <summary>
        /// Tworzy nową linię pozycji z poprawioną ceną netto
        /// </summary>
        /// <param name="pozLine">Lini pozycji do zmiany</param>
        /// <returns>Zmieniona linia pozycji</returns>
        private string GetNewLineToInsert(string pozLine, string nrDok, int idDok)
        {
            string newPozLine = "";

            List<string> parametryPoz = pozLine.Split(new char[] { ',' }).ToList();

            double finalnaCenaZakupu = DA.GetFinalnaCenaZakupu.GetFinalCenaZakupu(nrDok, parametryPoz[0]);


            string cenaNetto = parametryPoz[13];

            double cenaNettoPop = double.Parse(cenaNetto.Replace(".", ","));


            if (cenaNettoPop != 0 & finalnaCenaZakupu != 0)
            {
                //  GettingFormula.GetFormulaFromString("cenaNettoPop = (((cenaNettoPop * 1.03) - finalnaCenaZakupu) / 2) + finalnaCenaZakupu;");
                // cenaNettoPop = (((cenaNettoPop * 1.03) - finalnaCenaZakupu) / 2) + finalnaCenaZakupu;

               // double wsp = ((finalnaCenaZakupu * 0.03) + finalnaCenaZakupu);
               // cenaNettoPop = (((cenaNettoPop - wsp) / 2) + wsp);

                double wsp = (finalnaCenaZakupu);
                cenaNettoPop = (((cenaNettoPop - wsp) / 2) + wsp);

                // cenaNettoPop.ToString("F").Replace(",", ".");

                parametryPoz[13] = String.Format("{0:0.00}", cenaNettoPop).Replace(",", ".") + "00";
            }
            else
            {
                parametryPoz[13] = "0.0000";
            }

            //Podmiana symbolu towaru
            string id_poz = parametryPoz[0];

            string symbol_new = DA.GetNumerSeryjny.GetNumerSeryjnyFromPoz(int.Parse(id_poz), idDok);

            if(symbol_new != null)
            {
                string opis_new = parametryPoz[2].Replace("\"","") + " - " + parametryPoz[20].Split(';')[0].Replace("\"","") + " - " + symbol_new;

                parametryPoz[20] =  $"\"{opis_new}\"";

                parametryPoz[2] = $"\"{symbol_new}\"";
            }


            foreach (var i in parametryPoz)
            {
                newPozLine += i + ",";
            }

            return newPozLine.Remove(newPozLine.Length -1);
        }



        /// <summary>
        /// Zwraca listę ID do wygenerowani plików
        /// </summary>
        /// <returns>Lista id w postaci stringa oddzielonego przecinkami </returns>
        private string GetIdFromList()
        {
            string kolekcja = "";

            Int32 selectedRowCount = metroGridDokumentyWZ.Rows.GetRowCount(DataGridViewElementStates.Selected);
            if (selectedRowCount > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                for (int i = 0; i < selectedRowCount; i++)
                {
                    sb.Append(metroGridDokumentyWZ.SelectedRows[i].Cells[0].Value.ToString());
                    sb.Append(", ");
                }

                sb.Remove(sb.Length - 2, 2);

                kolekcja = sb.ToString();
            }

            return kolekcja;
        }

        private void metroButtonSzukajWZs_Click(object sender, EventArgs e)
        {
            metroProgressSpinner1.Spinning = true;

            try
            {
                metroCheckBoxZaznacz.Checked = false;

                int idMagazyn = ((sl_Magazyn)metroComboBoxMagazyny.SelectedItem).mag_Id;

                DateTime dataOd = Convert.ToDateTime(metroDateTimeOd.Value.ToShortDateString());
                DateTime dataDo = Convert.ToDateTime(metroDateTimeDo.Value.ToShortDateString());

                List<dok__Dokument> listWZFromBaseDatabase = new List<dok__Dokument>();
                List<dok__Dokument> listWZFromBaseCompany = new List<dok__Dokument>();

                List<int> kolekcja1;

                if (metroComboBoxFirmy.Text == "WEKTOR")
                {
                    firma = Firma.Wektor;
                    listWZFromBaseDatabase = DA.GetDocumentList.GetWZFromWEKTORSubiektToList(idMagazyn, dataOd, dataDo);
                    // DokumentVOBindingSource.DataSource = DA.GetDocumentList.GetWZFromWEKTORSubiektToList(idMagazyn, dataOd, dataDo);
                    DokumentVOBindingSource.DataSource = listWZFromBaseDatabase;
                }
                else if (metroComboBoxFirmy.Text == "KONEKT")
                {
                    firma = Firma.Konekt;
                    listWZFromBaseCompany = DA.GetDocumentList.GetWZFromKONEKTSubiektToList(dataOd, dataDo);
                    kolekcja1 = GetIdFromWZList(listWZFromBaseCompany);
                    listWZFromBaseDatabase = DA.GetDocumentList.GetWZFromSubiektToList(kolekcja1, idMagazyn, dataOd, dataDo);
                    DokumentVOBindingSource.DataSource = listWZFromBaseDatabase;
                }
                else if (metroComboBoxFirmy.Text == "KONEKT BIS")
                {
                    firma = Firma.KonektBis;
                    listWZFromBaseCompany = DA.GetDocumentList.GetWZFromKONEKTBISSubiektToList(dataOd, dataDo);
                    kolekcja1 = GetIdFromWZList(listWZFromBaseCompany);
                    listWZFromBaseDatabase = DA.GetDocumentList.GetWZFromSubiektToList(kolekcja1, idMagazyn, dataOd, dataDo);
                    DokumentVOBindingSource.DataSource = listWZFromBaseDatabase;
                }
                else if (metroComboBoxFirmy.Text == "KONEKT TKANINY")
                {
                    firma = Firma.KonektTkaniny;
                    listWZFromBaseCompany = DA.GetDocumentList.GetWZFromKOENKTTKANINYSubiektToList(dataOd, dataDo);
                    kolekcja1 = GetIdFromWZList(listWZFromBaseCompany);
                    listWZFromBaseDatabase = DA.GetDocumentList.GetWZFromSubiektToList(kolekcja1, idMagazyn, dataOd, dataDo);
                    DokumentVOBindingSource.DataSource = listWZFromBaseDatabase;
                }
                else if (metroComboBoxFirmy.Text == "WEKTOR BIS")
                {
                    firma = Firma.WektorBis;
                    listWZFromBaseDatabase = DA.GetDocumentList.GetWZFromWEKTORBISSubiektToList(idMagazyn, dataOd, dataDo);
                    // DokumentVOBindingSource.DataSource = DA.GetDocumentList.GetWZFromWEKTORSubiektToList(idMagazyn, dataOd, dataDo);
                    DokumentVOBindingSource.DataSource = listWZFromBaseDatabase;
                }
                else if (metroComboBoxFirmy.Text == "")
                {
                    MetroMessageBox.Show(this, "Wybierz firmę", "Uwaga", MessageBoxButtons.OK,MessageBoxIcon.Information);
                }

                decimal metry = 0;

                foreach (var s in listWZFromBaseDatabase)
                {
                    metry += s.ilosc_metrow;

                    //foreach (var a in s.dok_Pozycja)
                    //{
                   //   metry =+ a.ob_Ilosc;
                    //}
                }

                metroIloscDokLabel.Text = DokumentVOBindingSource.Count.ToString();
                metroiloscMetrLabel.Text = metry.ToString();

                metroProgressSpinner1.Spinning = false;
            }
            catch (Exception ex)
            {
                metroProgressSpinner1.Spinning = false;
                MetroMessageBox.Show(this, $"Bład {ex}");
            }
        }


        private List<int> GetIdFromWZList(List<dok__Dokument> listWZ)
        {
            var kolekcja = new List<int>();
            Int32 RowCount = listWZ.Count;

            if (RowCount > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                foreach(var i in listWZ)
                {
                    int id = 0;
                    bool czyInt = int.TryParse(i.dok_Uwagi, out id);
                    if (czyInt == true)
                    {
                        kolekcja.Add(id);
                    }
                    else
                    {
                        continue;
                    }
                }

            }

            return kolekcja;
        }

        private void GenerujEPP_Load(object sender, EventArgs e)
        {
            try
            {
                magazynVOBindingSource.DataSource = DA.GetWarehousesList.GetSnFromSubiektToList();
            }
            catch (Exception)
            {

                MetroMessageBox.Show(this, "Bład");
            }
        }

        private void metroCheckBoxZaznacz_CheckedChanged(object sender, EventArgs e)
        {
            if (metroCheckBoxZaznacz.Checked == true)
            {
                metroGridDokumentyWZ.SelectAll();
            }
            else if (metroCheckBoxZaznacz.Checked == false)
            {
                metroGridDokumentyWZ.ClearSelection();
            }
        }

    }
}
