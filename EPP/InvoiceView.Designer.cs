﻿namespace EPP
{
    partial class InvoiceView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.metroComboBoxWystawca = new MetroFramework.Controls.MetroComboBox();
            this.magazynVOBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.metroLabel_issuer = new MetroFramework.Controls.MetroLabel();
            this.metroLabel_recipient = new MetroFramework.Controls.MetroLabel();
            this.metroComboBoxOdbiorca = new MetroFramework.Controls.MetroComboBox();
            this.magazynVOBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.metroDateTime = new MetroFramework.Controls.MetroDateTime();
            this.metroLabel_date = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroButtonSzukajWZs = new MetroFramework.Controls.MetroButton();
            this.metroGridDokumentyWZ = new MetroFramework.Controls.MetroGrid();
            this.dok_DataWyst = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dok_NrPelny = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ilosc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CenaJednostkowa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WartoscNetto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WartoscBrutto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NazwaOdbiorcy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kodDostawyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nazwaTowaruDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tempPositionVOBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dokDokumentBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.metroButtonGenerujFV = new MetroFramework.Controls.MetroButton();
            ((System.ComponentModel.ISupportInitialize)(this.magazynVOBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.magazynVOBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.metroGridDokumentyWZ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tempPositionVOBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dokDokumentBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // metroComboBoxWystawca
            // 
            this.metroComboBoxWystawca.DataSource = this.magazynVOBindingSource;
            this.metroComboBoxWystawca.DisplayMember = "mag_Nazwa";
            this.metroComboBoxWystawca.FormattingEnabled = true;
            this.metroComboBoxWystawca.ItemHeight = 23;
            this.metroComboBoxWystawca.Location = new System.Drawing.Point(80, 43);
            this.metroComboBoxWystawca.Name = "metroComboBoxWystawca";
            this.metroComboBoxWystawca.Size = new System.Drawing.Size(160, 29);
            this.metroComboBoxWystawca.TabIndex = 3;
            this.metroComboBoxWystawca.UseSelectable = true;
            this.metroComboBoxWystawca.ValueMember = "mag_Id";
            // 
            // magazynVOBindingSource
            // 
            this.magazynVOBindingSource.DataSource = typeof(EPP.VO.MagazynVO);
            // 
            // metroLabel_issuer
            // 
            this.metroLabel_issuer.AutoSize = true;
            this.metroLabel_issuer.Location = new System.Drawing.Point(10, 42);
            this.metroLabel_issuer.Name = "metroLabel_issuer";
            this.metroLabel_issuer.Size = new System.Drawing.Size(66, 19);
            this.metroLabel_issuer.TabIndex = 4;
            this.metroLabel_issuer.Text = "Wystawca";
            // 
            // metroLabel_recipient
            // 
            this.metroLabel_recipient.AutoSize = true;
            this.metroLabel_recipient.Location = new System.Drawing.Point(249, 42);
            this.metroLabel_recipient.Name = "metroLabel_recipient";
            this.metroLabel_recipient.Size = new System.Drawing.Size(65, 19);
            this.metroLabel_recipient.TabIndex = 6;
            this.metroLabel_recipient.Text = "Odbiorca";
            // 
            // metroComboBoxOdbiorca
            // 
            this.metroComboBoxOdbiorca.DataSource = this.magazynVOBindingSource1;
            this.metroComboBoxOdbiorca.DisplayMember = "mag_Nazwa";
            this.metroComboBoxOdbiorca.FormattingEnabled = true;
            this.metroComboBoxOdbiorca.ItemHeight = 23;
            this.metroComboBoxOdbiorca.Location = new System.Drawing.Point(319, 43);
            this.metroComboBoxOdbiorca.Name = "metroComboBoxOdbiorca";
            this.metroComboBoxOdbiorca.Size = new System.Drawing.Size(160, 29);
            this.metroComboBoxOdbiorca.TabIndex = 5;
            this.metroComboBoxOdbiorca.UseSelectable = true;
            this.metroComboBoxOdbiorca.ValueMember = "mag_Id";
            // 
            // magazynVOBindingSource1
            // 
            this.magazynVOBindingSource1.DataSource = typeof(EPP.VO.MagazynVO);
            // 
            // metroDateTime
            // 
            this.metroDateTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.metroDateTime.Location = new System.Drawing.Point(529, 43);
            this.metroDateTime.MinimumSize = new System.Drawing.Size(0, 29);
            this.metroDateTime.Name = "metroDateTime";
            this.metroDateTime.Size = new System.Drawing.Size(160, 29);
            this.metroDateTime.TabIndex = 7;
            this.metroDateTime.ValueChanged += new System.EventHandler(this.metroDateTimeOd_ValueChanged);
            // 
            // metroLabel_date
            // 
            this.metroLabel_date.AutoSize = true;
            this.metroLabel_date.Location = new System.Drawing.Point(489, 42);
            this.metroLabel_date.Name = "metroLabel_date";
            this.metroLabel_date.Size = new System.Drawing.Size(36, 19);
            this.metroLabel_date.TabIndex = 8;
            this.metroLabel_date.Text = "Data";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel1.Location = new System.Drawing.Point(0, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(147, 25);
            this.metroLabel1.TabIndex = 9;
            this.metroLabel1.Text = "Generuj Fakturę";
            // 
            // metroButtonSzukajWZs
            // 
            this.metroButtonSzukajWZs.Location = new System.Drawing.Point(730, 43);
            this.metroButtonSzukajWZs.Name = "metroButtonSzukajWZs";
            this.metroButtonSzukajWZs.Size = new System.Drawing.Size(171, 29);
            this.metroButtonSzukajWZs.TabIndex = 10;
            this.metroButtonSzukajWZs.Text = "Szukaj";
            this.metroButtonSzukajWZs.UseSelectable = true;
            this.metroButtonSzukajWZs.Click += new System.EventHandler(this.metroButtonSzukajWZs_Click);
            // 
            // metroGridDokumentyWZ
            // 
            this.metroGridDokumentyWZ.AllowUserToAddRows = false;
            this.metroGridDokumentyWZ.AllowUserToDeleteRows = false;
            this.metroGridDokumentyWZ.AllowUserToResizeRows = false;
            this.metroGridDokumentyWZ.AutoGenerateColumns = false;
            this.metroGridDokumentyWZ.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGridDokumentyWZ.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.metroGridDokumentyWZ.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.metroGridDokumentyWZ.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGridDokumentyWZ.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.metroGridDokumentyWZ.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.metroGridDokumentyWZ.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dok_DataWyst,
            this.dok_NrPelny,
            this.Ilosc,
            this.CenaJednostkowa,
            this.WartoscNetto,
            this.WartoscBrutto,
            this.NazwaOdbiorcy,
            this.kodDostawyDataGridViewTextBoxColumn,
            this.nazwaTowaruDataGridViewTextBoxColumn});
            this.metroGridDokumentyWZ.DataSource = this.tempPositionVOBindingSource;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.metroGridDokumentyWZ.DefaultCellStyle = dataGridViewCellStyle5;
            this.metroGridDokumentyWZ.EnableHeadersVisualStyles = false;
            this.metroGridDokumentyWZ.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.metroGridDokumentyWZ.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGridDokumentyWZ.Location = new System.Drawing.Point(3, 91);
            this.metroGridDokumentyWZ.Name = "metroGridDokumentyWZ";
            this.metroGridDokumentyWZ.ReadOnly = true;
            this.metroGridDokumentyWZ.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGridDokumentyWZ.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.metroGridDokumentyWZ.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.metroGridDokumentyWZ.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.metroGridDokumentyWZ.Size = new System.Drawing.Size(921, 393);
            this.metroGridDokumentyWZ.TabIndex = 11;
            this.metroGridDokumentyWZ.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.metroGridDokumentyWZ_CellContentClick);
            // 
            // dok_DataWyst
            // 
            this.dok_DataWyst.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dok_DataWyst.DataPropertyName = "dok_DataWyst";
            this.dok_DataWyst.HeaderText = "Data Wystawienia";
            this.dok_DataWyst.Name = "dok_DataWyst";
            this.dok_DataWyst.ReadOnly = true;
            this.dok_DataWyst.Width = 113;
            // 
            // dok_NrPelny
            // 
            this.dok_NrPelny.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dok_NrPelny.DataPropertyName = "dok_NrPelny";
            this.dok_NrPelny.HeaderText = "Numer dokumentu";
            this.dok_NrPelny.Name = "dok_NrPelny";
            this.dok_NrPelny.ReadOnly = true;
            this.dok_NrPelny.Width = 117;
            // 
            // Ilosc
            // 
            this.Ilosc.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Ilosc.DataPropertyName = "Ilosc";
            this.Ilosc.HeaderText = "Ilość";
            this.Ilosc.Name = "Ilosc";
            this.Ilosc.ReadOnly = true;
            this.Ilosc.Width = 54;
            // 
            // CenaJednostkowa
            // 
            this.CenaJednostkowa.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.CenaJednostkowa.DataPropertyName = "CenaJednostkowa";
            this.CenaJednostkowa.HeaderText = "Cena";
            this.CenaJednostkowa.Name = "CenaJednostkowa";
            this.CenaJednostkowa.ReadOnly = true;
            this.CenaJednostkowa.Width = 57;
            // 
            // WartoscNetto
            // 
            this.WartoscNetto.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.WartoscNetto.DataPropertyName = "WartoscNetto";
            this.WartoscNetto.HeaderText = "Netto";
            this.WartoscNetto.Name = "WartoscNetto";
            this.WartoscNetto.ReadOnly = true;
            this.WartoscNetto.Width = 60;
            // 
            // WartoscBrutto
            // 
            this.WartoscBrutto.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.WartoscBrutto.DataPropertyName = "WartoscBrutto";
            this.WartoscBrutto.HeaderText = "Brutto";
            this.WartoscBrutto.Name = "WartoscBrutto";
            this.WartoscBrutto.ReadOnly = true;
            this.WartoscBrutto.Width = 63;
            // 
            // NazwaOdbiorcy
            // 
            this.NazwaOdbiorcy.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.NazwaOdbiorcy.DataPropertyName = "NazwaOdbiorcy";
            this.NazwaOdbiorcy.HeaderText = "Klient";
            this.NazwaOdbiorcy.Name = "NazwaOdbiorcy";
            this.NazwaOdbiorcy.ReadOnly = true;
            this.NazwaOdbiorcy.Width = 60;
            // 
            // kodDostawyDataGridViewTextBoxColumn
            // 
            this.kodDostawyDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.kodDostawyDataGridViewTextBoxColumn.DataPropertyName = "KodDostawy";
            this.kodDostawyDataGridViewTextBoxColumn.HeaderText = "Kod dostawy";
            this.kodDostawyDataGridViewTextBoxColumn.Name = "kodDostawyDataGridViewTextBoxColumn";
            this.kodDostawyDataGridViewTextBoxColumn.ReadOnly = true;
            this.kodDostawyDataGridViewTextBoxColumn.Width = 89;
            // 
            // nazwaTowaruDataGridViewTextBoxColumn
            // 
            this.nazwaTowaruDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.nazwaTowaruDataGridViewTextBoxColumn.DataPropertyName = "NazwaTowaru";
            this.nazwaTowaruDataGridViewTextBoxColumn.HeaderText = "Towar";
            this.nazwaTowaruDataGridViewTextBoxColumn.Name = "nazwaTowaruDataGridViewTextBoxColumn";
            this.nazwaTowaruDataGridViewTextBoxColumn.ReadOnly = true;
            this.nazwaTowaruDataGridViewTextBoxColumn.Width = 62;
            // 
            // tempPositionVOBindingSource
            // 
            this.tempPositionVOBindingSource.DataSource = typeof(EPP.VO.TempPositionVO);
            // 
            // dokDokumentBindingSource
            // 
            this.dokDokumentBindingSource.DataSource = typeof(EPP.dok__Dokument);
            // 
            // metroButtonGenerujFV
            // 
            this.metroButtonGenerujFV.Location = new System.Drawing.Point(753, 509);
            this.metroButtonGenerujFV.Name = "metroButtonGenerujFV";
            this.metroButtonGenerujFV.Size = new System.Drawing.Size(171, 29);
            this.metroButtonGenerujFV.TabIndex = 12;
            this.metroButtonGenerujFV.Text = "Generuj FV";
            this.metroButtonGenerujFV.UseSelectable = true;
            this.metroButtonGenerujFV.Click += new System.EventHandler(this.metroButtonGenerujFV_Click);
            // 
            // InvoiceView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.metroButtonGenerujFV);
            this.Controls.Add(this.metroGridDokumentyWZ);
            this.Controls.Add(this.metroButtonSzukajWZs);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.metroLabel_date);
            this.Controls.Add(this.metroDateTime);
            this.Controls.Add(this.metroLabel_recipient);
            this.Controls.Add(this.metroComboBoxOdbiorca);
            this.Controls.Add(this.metroLabel_issuer);
            this.Controls.Add(this.metroComboBoxWystawca);
            this.Name = "InvoiceView";
            this.Size = new System.Drawing.Size(927, 541);
            this.Load += new System.EventHandler(this.InvoiceView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.magazynVOBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.magazynVOBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.metroGridDokumentyWZ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tempPositionVOBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dokDokumentBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroComboBox metroComboBoxWystawca;
        private MetroFramework.Controls.MetroLabel metroLabel_issuer;
        private MetroFramework.Controls.MetroLabel metroLabel_recipient;
        private MetroFramework.Controls.MetroComboBox metroComboBoxOdbiorca;
        private MetroFramework.Controls.MetroDateTime metroDateTime;
        private MetroFramework.Controls.MetroLabel metroLabel_date;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroButton metroButtonSzukajWZs;
        private MetroFramework.Controls.MetroGrid metroGridDokumentyWZ;
        private System.Windows.Forms.BindingSource magazynVOBindingSource;
        private System.Windows.Forms.BindingSource magazynVOBindingSource1;
        private System.Windows.Forms.BindingSource dokDokumentBindingSource;
        private System.Windows.Forms.BindingSource tempPositionVOBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn dok_DataWyst;
        private System.Windows.Forms.DataGridViewTextBoxColumn dok_NrPelny;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ilosc;
        private System.Windows.Forms.DataGridViewTextBoxColumn CenaJednostkowa;
        private System.Windows.Forms.DataGridViewTextBoxColumn WartoscNetto;
        private System.Windows.Forms.DataGridViewTextBoxColumn WartoscBrutto;
        private System.Windows.Forms.DataGridViewTextBoxColumn NazwaOdbiorcy;
        private System.Windows.Forms.DataGridViewTextBoxColumn kodDostawyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nazwaTowaruDataGridViewTextBoxColumn;
        private MetroFramework.Controls.MetroButton metroButtonGenerujFV;
    }
}