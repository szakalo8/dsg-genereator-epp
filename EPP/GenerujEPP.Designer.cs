﻿namespace EPP
{
    partial class GenerujEPP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroComboBoxMagazyny = new MetroFramework.Controls.MetroComboBox();
            this.magazynVOBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroDateTimeOd = new MetroFramework.Controls.MetroDateTime();
            this.metroDateTimeDo = new MetroFramework.Controls.MetroDateTime();
            this.metroButtonSzukajWZs = new MetroFramework.Controls.MetroButton();
            this.metroTabControlGenerujEPP = new MetroFramework.Controls.MetroTabControl();
            this.metroTabPage1 = new MetroFramework.Controls.MetroTabPage();
            this.metroiloscMetrLabel = new MetroFramework.Controls.MetroLabel();
            this.metroLabel = new MetroFramework.Controls.MetroLabel();
            this.metroIloscDokLabel = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroProgressSpinner1 = new MetroFramework.Controls.MetroProgressSpinner();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroComboBoxFirmy = new MetroFramework.Controls.MetroComboBox();
            this.metroCheckBoxZaznacz = new MetroFramework.Controls.MetroCheckBox();
            this.metroButtonGenerujEPP = new MetroFramework.Controls.MetroButton();
            this.metroGridDokumentyWZ = new MetroFramework.Controls.MetroGrid();
            this.dokIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dok_NrPelny = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dok_DataWyst = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ilosc_metrow = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DokumentVOBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.metroTabPage2 = new MetroFramework.Controls.MetroTabPage();
            this.metroTextBox_Logi = new MetroFramework.Controls.MetroTextBox();
            this.dokDokumentBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.magazynVOBindingSource)).BeginInit();
            this.metroTabControlGenerujEPP.SuspendLayout();
            this.metroTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGridDokumentyWZ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DokumentVOBindingSource)).BeginInit();
            this.metroTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dokDokumentBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel1.Location = new System.Drawing.Point(0, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(116, 25);
            this.metroLabel1.TabIndex = 1;
            this.metroLabel1.Text = "Generuj EPP";
            // 
            // metroComboBoxMagazyny
            // 
            this.metroComboBoxMagazyny.DataSource = this.magazynVOBindingSource;
            this.metroComboBoxMagazyny.DisplayMember = "mag_Nazwa";
            this.metroComboBoxMagazyny.FormattingEnabled = true;
            this.metroComboBoxMagazyny.ItemHeight = 23;
            this.metroComboBoxMagazyny.Location = new System.Drawing.Point(74, 4);
            this.metroComboBoxMagazyny.Name = "metroComboBoxMagazyny";
            this.metroComboBoxMagazyny.Size = new System.Drawing.Size(157, 29);
            this.metroComboBoxMagazyny.TabIndex = 2;
            this.metroComboBoxMagazyny.UseSelectable = true;
            // 
            // magazynVOBindingSource
            // 
            this.magazynVOBindingSource.DataSource = typeof(EPP.VO.MagazynVO);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(3, 14);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(65, 19);
            this.metroLabel2.TabIndex = 3;
            this.metroLabel2.Text = "Magazyn:";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(246, 14);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(66, 19);
            this.metroLabel3.TabIndex = 4;
            this.metroLabel3.Text = "DATA OD:";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(524, 14);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(32, 19);
            this.metroLabel4.TabIndex = 5;
            this.metroLabel4.Text = "DO:";
            // 
            // metroDateTimeOd
            // 
            this.metroDateTimeOd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.metroDateTimeOd.Location = new System.Drawing.Point(318, 4);
            this.metroDateTimeOd.MinimumSize = new System.Drawing.Size(0, 29);
            this.metroDateTimeOd.Name = "metroDateTimeOd";
            this.metroDateTimeOd.Size = new System.Drawing.Size(200, 29);
            this.metroDateTimeOd.TabIndex = 6;
            // 
            // metroDateTimeDo
            // 
            this.metroDateTimeDo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.metroDateTimeDo.Location = new System.Drawing.Point(562, 4);
            this.metroDateTimeDo.MinimumSize = new System.Drawing.Size(0, 29);
            this.metroDateTimeDo.Name = "metroDateTimeDo";
            this.metroDateTimeDo.Size = new System.Drawing.Size(200, 29);
            this.metroDateTimeDo.TabIndex = 7;
            // 
            // metroButtonSzukajWZs
            // 
            this.metroButtonSzukajWZs.Location = new System.Drawing.Point(768, 4);
            this.metroButtonSzukajWZs.Name = "metroButtonSzukajWZs";
            this.metroButtonSzukajWZs.Size = new System.Drawing.Size(171, 29);
            this.metroButtonSzukajWZs.TabIndex = 8;
            this.metroButtonSzukajWZs.Text = "Szukaj";
            this.metroButtonSzukajWZs.UseSelectable = true;
            this.metroButtonSzukajWZs.Click += new System.EventHandler(this.metroButtonSzukajWZs_Click);
            // 
            // metroTabControlGenerujEPP
            // 
            this.metroTabControlGenerujEPP.Controls.Add(this.metroTabPage1);
            this.metroTabControlGenerujEPP.Controls.Add(this.metroTabPage2);
            this.metroTabControlGenerujEPP.Location = new System.Drawing.Point(4, 29);
            this.metroTabControlGenerujEPP.Name = "metroTabControlGenerujEPP";
            this.metroTabControlGenerujEPP.SelectedIndex = 0;
            this.metroTabControlGenerujEPP.Size = new System.Drawing.Size(947, 552);
            this.metroTabControlGenerujEPP.TabIndex = 9;
            this.metroTabControlGenerujEPP.UseSelectable = true;
            // 
            // metroTabPage1
            // 
            this.metroTabPage1.Controls.Add(this.metroiloscMetrLabel);
            this.metroTabPage1.Controls.Add(this.metroLabel);
            this.metroTabPage1.Controls.Add(this.metroIloscDokLabel);
            this.metroTabPage1.Controls.Add(this.metroLabel6);
            this.metroTabPage1.Controls.Add(this.metroProgressSpinner1);
            this.metroTabPage1.Controls.Add(this.metroLabel5);
            this.metroTabPage1.Controls.Add(this.metroComboBoxFirmy);
            this.metroTabPage1.Controls.Add(this.metroCheckBoxZaznacz);
            this.metroTabPage1.Controls.Add(this.metroButtonGenerujEPP);
            this.metroTabPage1.Controls.Add(this.metroGridDokumentyWZ);
            this.metroTabPage1.Controls.Add(this.metroButtonSzukajWZs);
            this.metroTabPage1.Controls.Add(this.metroLabel2);
            this.metroTabPage1.Controls.Add(this.metroComboBoxMagazyny);
            this.metroTabPage1.Controls.Add(this.metroDateTimeDo);
            this.metroTabPage1.Controls.Add(this.metroDateTimeOd);
            this.metroTabPage1.Controls.Add(this.metroLabel3);
            this.metroTabPage1.Controls.Add(this.metroLabel4);
            this.metroTabPage1.HorizontalScrollbarBarColor = true;
            this.metroTabPage1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.HorizontalScrollbarSize = 10;
            this.metroTabPage1.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage1.Name = "metroTabPage1";
            this.metroTabPage1.Size = new System.Drawing.Size(939, 510);
            this.metroTabPage1.TabIndex = 0;
            this.metroTabPage1.Text = "Generuj EPP";
            this.metroTabPage1.VerticalScrollbarBarColor = true;
            this.metroTabPage1.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.VerticalScrollbarSize = 10;
            // 
            // metroiloscMetrLabel
            // 
            this.metroiloscMetrLabel.AutoSize = true;
            this.metroiloscMetrLabel.Location = new System.Drawing.Point(562, 459);
            this.metroiloscMetrLabel.Name = "metroiloscMetrLabel";
            this.metroiloscMetrLabel.Size = new System.Drawing.Size(16, 19);
            this.metroiloscMetrLabel.TabIndex = 18;
            this.metroiloscMetrLabel.Text = "0";
            // 
            // metroLabel
            // 
            this.metroLabel.AutoSize = true;
            this.metroLabel.Location = new System.Drawing.Point(470, 459);
            this.metroLabel.Name = "metroLabel";
            this.metroLabel.Size = new System.Drawing.Size(86, 19);
            this.metroLabel.TabIndex = 17;
            this.metroLabel.Text = "Ilość metrów:";
            // 
            // metroIloscDokLabel
            // 
            this.metroIloscDokLabel.AutoSize = true;
            this.metroIloscDokLabel.Location = new System.Drawing.Point(387, 459);
            this.metroIloscDokLabel.Name = "metroIloscDokLabel";
            this.metroIloscDokLabel.Size = new System.Drawing.Size(16, 19);
            this.metroIloscDokLabel.TabIndex = 16;
            this.metroIloscDokLabel.Text = "0";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(318, 459);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(63, 19);
            this.metroLabel6.TabIndex = 15;
            this.metroLabel6.Text = "Ilość dok:";
            // 
            // metroProgressSpinner1
            // 
            this.metroProgressSpinner1.Location = new System.Drawing.Point(121, 457);
            this.metroProgressSpinner1.Maximum = 100;
            this.metroProgressSpinner1.Name = "metroProgressSpinner1";
            this.metroProgressSpinner1.Size = new System.Drawing.Size(86, 50);
            this.metroProgressSpinner1.Spinning = false;
            this.metroProgressSpinner1.TabIndex = 14;
            this.metroProgressSpinner1.UseSelectable = true;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(3, 49);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(46, 19);
            this.metroLabel5.TabIndex = 13;
            this.metroLabel5.Text = "Firma:";
            // 
            // metroComboBoxFirmy
            // 
            this.metroComboBoxFirmy.FormattingEnabled = true;
            this.metroComboBoxFirmy.ItemHeight = 23;
            this.metroComboBoxFirmy.Items.AddRange(new object[] {
            "KONEKT",
            "KONEKT BIS",
            "KONEKT TKANINY",
            "WEKTOR",
            "WEKTOR BIS"});
            this.metroComboBoxFirmy.Location = new System.Drawing.Point(74, 39);
            this.metroComboBoxFirmy.Name = "metroComboBoxFirmy";
            this.metroComboBoxFirmy.Size = new System.Drawing.Size(157, 29);
            this.metroComboBoxFirmy.TabIndex = 12;
            this.metroComboBoxFirmy.UseSelectable = true;
            // 
            // metroCheckBoxZaznacz
            // 
            this.metroCheckBoxZaznacz.AutoSize = true;
            this.metroCheckBoxZaznacz.Location = new System.Drawing.Point(0, 459);
            this.metroCheckBoxZaznacz.Name = "metroCheckBoxZaznacz";
            this.metroCheckBoxZaznacz.Size = new System.Drawing.Size(115, 15);
            this.metroCheckBoxZaznacz.TabIndex = 11;
            this.metroCheckBoxZaznacz.Text = "Zaznacz wszystko";
            this.metroCheckBoxZaznacz.UseSelectable = true;
            this.metroCheckBoxZaznacz.CheckedChanged += new System.EventHandler(this.metroCheckBoxZaznacz_CheckedChanged);
            // 
            // metroButtonGenerujEPP
            // 
            this.metroButtonGenerujEPP.Location = new System.Drawing.Point(768, 478);
            this.metroButtonGenerujEPP.Name = "metroButtonGenerujEPP";
            this.metroButtonGenerujEPP.Size = new System.Drawing.Size(171, 29);
            this.metroButtonGenerujEPP.TabIndex = 10;
            this.metroButtonGenerujEPP.Text = "Generuj EPP";
            this.metroButtonGenerujEPP.UseSelectable = true;
            this.metroButtonGenerujEPP.Click += new System.EventHandler(this.metroButtonGenerujEPP_Click);
            // 
            // metroGridDokumentyWZ
            // 
            this.metroGridDokumentyWZ.AllowUserToAddRows = false;
            this.metroGridDokumentyWZ.AllowUserToDeleteRows = false;
            this.metroGridDokumentyWZ.AllowUserToResizeRows = false;
            this.metroGridDokumentyWZ.AutoGenerateColumns = false;
            this.metroGridDokumentyWZ.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGridDokumentyWZ.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.metroGridDokumentyWZ.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.metroGridDokumentyWZ.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGridDokumentyWZ.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.metroGridDokumentyWZ.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.metroGridDokumentyWZ.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dokIdDataGridViewTextBoxColumn,
            this.dok_NrPelny,
            this.dok_DataWyst,
            this.ilosc_metrow});
            this.metroGridDokumentyWZ.DataSource = this.DokumentVOBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.metroGridDokumentyWZ.DefaultCellStyle = dataGridViewCellStyle2;
            this.metroGridDokumentyWZ.EnableHeadersVisualStyles = false;
            this.metroGridDokumentyWZ.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.metroGridDokumentyWZ.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGridDokumentyWZ.Location = new System.Drawing.Point(3, 74);
            this.metroGridDokumentyWZ.Name = "metroGridDokumentyWZ";
            this.metroGridDokumentyWZ.ReadOnly = true;
            this.metroGridDokumentyWZ.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGridDokumentyWZ.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.metroGridDokumentyWZ.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.metroGridDokumentyWZ.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.metroGridDokumentyWZ.Size = new System.Drawing.Size(936, 379);
            this.metroGridDokumentyWZ.TabIndex = 9;
            // 
            // dokIdDataGridViewTextBoxColumn
            // 
            this.dokIdDataGridViewTextBoxColumn.DataPropertyName = "dok_Id";
            this.dokIdDataGridViewTextBoxColumn.HeaderText = "ID";
            this.dokIdDataGridViewTextBoxColumn.Name = "dokIdDataGridViewTextBoxColumn";
            this.dokIdDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dok_NrPelny
            // 
            this.dok_NrPelny.DataPropertyName = "dok_NrPelny";
            this.dok_NrPelny.HeaderText = "Numer pełny";
            this.dok_NrPelny.Name = "dok_NrPelny";
            this.dok_NrPelny.ReadOnly = true;
            this.dok_NrPelny.Width = 200;
            // 
            // dok_DataWyst
            // 
            this.dok_DataWyst.DataPropertyName = "dok_DataWyst";
            this.dok_DataWyst.HeaderText = "Data Wystawienia";
            this.dok_DataWyst.Name = "dok_DataWyst";
            this.dok_DataWyst.ReadOnly = true;
            this.dok_DataWyst.Width = 200;
            // 
            // ilosc_metrow
            // 
            this.ilosc_metrow.DataPropertyName = "ilosc_metrow";
            this.ilosc_metrow.HeaderText = "Ilosc metrów";
            this.ilosc_metrow.Name = "ilosc_metrow";
            this.ilosc_metrow.ReadOnly = true;
            // 
            // DokumentVOBindingSource
            // 
            this.DokumentVOBindingSource.DataSource = typeof(EPP.dok__Dokument);
            // 
            // metroTabPage2
            // 
            this.metroTabPage2.Controls.Add(this.metroTextBox_Logi);
            this.metroTabPage2.HorizontalScrollbarBarColor = true;
            this.metroTabPage2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage2.HorizontalScrollbarSize = 10;
            this.metroTabPage2.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage2.Name = "metroTabPage2";
            this.metroTabPage2.Size = new System.Drawing.Size(939, 510);
            this.metroTabPage2.TabIndex = 1;
            this.metroTabPage2.Text = "Log";
            this.metroTabPage2.VerticalScrollbarBarColor = true;
            this.metroTabPage2.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage2.VerticalScrollbarSize = 10;
            // 
            // metroTextBox_Logi
            // 
            // 
            // 
            // 
            this.metroTextBox_Logi.CustomButton.Image = null;
            this.metroTextBox_Logi.CustomButton.Location = new System.Drawing.Point(431, 2);
            this.metroTextBox_Logi.CustomButton.Name = "";
            this.metroTextBox_Logi.CustomButton.Size = new System.Drawing.Size(499, 499);
            this.metroTextBox_Logi.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_Logi.CustomButton.TabIndex = 1;
            this.metroTextBox_Logi.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_Logi.CustomButton.UseSelectable = true;
            this.metroTextBox_Logi.CustomButton.Visible = false;
            this.metroTextBox_Logi.Lines = new string[] {
        "logi"};
            this.metroTextBox_Logi.Location = new System.Drawing.Point(3, 3);
            this.metroTextBox_Logi.MaxLength = 32767;
            this.metroTextBox_Logi.Multiline = true;
            this.metroTextBox_Logi.Name = "metroTextBox_Logi";
            this.metroTextBox_Logi.PasswordChar = '\0';
            this.metroTextBox_Logi.ReadOnly = true;
            this.metroTextBox_Logi.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_Logi.SelectedText = "";
            this.metroTextBox_Logi.SelectionLength = 0;
            this.metroTextBox_Logi.SelectionStart = 0;
            this.metroTextBox_Logi.ShortcutsEnabled = true;
            this.metroTextBox_Logi.Size = new System.Drawing.Size(933, 504);
            this.metroTextBox_Logi.TabIndex = 2;
            this.metroTextBox_Logi.Text = "logi";
            this.metroTextBox_Logi.UseSelectable = true;
            this.metroTextBox_Logi.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_Logi.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // dokDokumentBindingSource
            // 
            this.dokDokumentBindingSource.DataSource = typeof(EPP.dok__Dokument);
            // 
            // GenerujEPP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.metroTabControlGenerujEPP);
            this.Controls.Add(this.metroLabel1);
            this.Name = "GenerujEPP";
            this.Size = new System.Drawing.Size(954, 584);
            this.Load += new System.EventHandler(this.GenerujEPP_Load);
            ((System.ComponentModel.ISupportInitialize)(this.magazynVOBindingSource)).EndInit();
            this.metroTabControlGenerujEPP.ResumeLayout(false);
            this.metroTabPage1.ResumeLayout(false);
            this.metroTabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGridDokumentyWZ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DokumentVOBindingSource)).EndInit();
            this.metroTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dokDokumentBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroComboBox metroComboBoxMagazyny;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroDateTime metroDateTimeOd;
        private MetroFramework.Controls.MetroDateTime metroDateTimeDo;
        private MetroFramework.Controls.MetroButton metroButtonSzukajWZs;
        private MetroFramework.Controls.MetroTabControl metroTabControlGenerujEPP;
        private MetroFramework.Controls.MetroTabPage metroTabPage1;
        private MetroFramework.Controls.MetroTabPage metroTabPage2;
        private MetroFramework.Controls.MetroButton metroButtonGenerujEPP;
        private MetroFramework.Controls.MetroGrid metroGridDokumentyWZ;
        private System.Windows.Forms.BindingSource magazynVOBindingSource;
        private System.Windows.Forms.BindingSource DokumentVOBindingSource;
        private System.Windows.Forms.BindingSource dokDokumentBindingSource;
        private MetroFramework.Controls.MetroCheckBox metroCheckBoxZaznacz;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroComboBox metroComboBoxFirmy;
        private MetroFramework.Controls.MetroProgressSpinner metroProgressSpinner1;
        private MetroFramework.Controls.MetroLabel metroIloscDokLabel;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroiloscMetrLabel;
        private MetroFramework.Controls.MetroLabel metroLabel;
        private System.Windows.Forms.DataGridViewTextBoxColumn dokIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dok_NrPelny;
        private System.Windows.Forms.DataGridViewTextBoxColumn dok_DataWyst;
        private System.Windows.Forms.DataGridViewTextBoxColumn ilosc_metrow;
        private MetroFramework.Controls.MetroTextBox metroTextBox_Logi;
    }
}
